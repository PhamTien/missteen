﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucNewsEvent.ascx.cs" Inherits="MyWeb.views.news.ucNewsEvent" %>
<%if (showAdv)
  {%>
<div class="box-news-event">
    <div class="header"><%= MyWeb.Global.GetLangKey("header-news-event")%></div>
    <ul class="body-news">
        <asp:Literal ID="ltrNew" runat="server"></asp:Literal>
    </ul>
</div>
<%} %>