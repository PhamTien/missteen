﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucMenuLeft.ascx.cs" Inherits="MyWeb.views.control.ucMenuLeft" %>
<div class="box-menu-left">
    <div class="header"><%= MyWeb.Global.GetLangKey("head-menu-left")%></div>
    <asp:Literal ID="ltrMenuLeft" runat="server"></asp:Literal>
</div>
