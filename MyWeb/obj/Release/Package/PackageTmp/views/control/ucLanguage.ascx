﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucLanguage.ascx.cs" Inherits="MyWeb.views.control.ucLanguage" %>
<ul class="ul-language">
    <li class="viet-nam">
        <asp:LinkButton ID="btnVietnam" runat="server" OnClick="btnVietnam_Click" CausesValidation="false"><img src="../../theme/admin_cms/img/vi.png" alt="Tiếng việt" /><span>Tiếng việt</span></asp:LinkButton>
    </li>
    <li class="eng-lish">
        <asp:LinkButton ID="btnEnglish" runat="server" OnClick="btnEnglish_Click" CausesValidation="false"><img src="../../theme/admin_cms/img/en.png" alt="English"/><span>English</span></asp:LinkButton>
    </li>
</ul>