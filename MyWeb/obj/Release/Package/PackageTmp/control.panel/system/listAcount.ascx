﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="listAcount.ascx.cs" Inherits="MyWeb.control.panel.system.listAcount" %>

<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="/control.panel/">Trang chủ</a></li>
        <li class="active">Thành viên quản trị</li>
    </ol>
    <!-- end breadcrumb -->
    <asp:Panel ID="pnlListForder" runat="server" Visible="true">
        <h1 class="page-header">Thành viên quản trị</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse" data-sortable-id="table-basic-1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">Danh sách thành viên quản trị</h4>
                    </div>

                    <div class="alert alert-info fade in" id="pnlErr" runat="server" visible="false">
                        <asp:Literal ID="ltrErr" runat="server"></asp:Literal>
                        <button class="close" data-dismiss="alert" type="button">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-5">
                                <asp:LinkButton ID="btnAddNew" runat="server" class="btn btn-success btn-sm" OnClick="btnAddNew_Click"><i class="fa fa-plus"></i><span>Thêm mới</span></asp:LinkButton>
                                <asp:LinkButton ID="btnDeleteAll" runat="server" class="btn btn-danger btn-sm" OnClick="btnDeleteAll_Click"><i class="fa fa-trash-o"></i>Xóa</asp:LinkButton>
                            </div>

                            <div class="col-sm-7">
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered dataTable no-footer dtr-inline">
                                        <thead>
                                            <tr>
                                                <th width="10">
                                                    <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="False"></asp:CheckBox>
                                                </th>
                                                <th>Tên đăng nhập</th>
                                                <th width="180">Tên thành viên</th>
                                                <th width="180">Ban quản trị</th>
                                                <th width="180">Loại tài khoản</th>
                                                <th width="50">Sắp xếp</th>
                                                <th width="20"></th>
                                                <th width="140">Công cụ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="rptFolderList" runat="server" OnItemCommand="rptFolderList_ItemCommand">
                                                <ItemTemplate>
                                                    <tr class="even gradeC">
                                                        <td>
                                                            <asp:CheckBox ID="chkBox" CssClass="chkBoxSelect" runat="server"></asp:CheckBox>
                                                            <asp:HiddenField ID="hidCatID" Value='<%#DataBinder.Eval(Container.DataItem, "useId")%>' runat="server" />
                                                            <asp:Label ID="lblID" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"useId")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <%#DataBinder.Eval(Container.DataItem,"useUid")%>
                                                        </td>
                                                        <td>
                                                            <%#DataBinder.Eval(Container.DataItem, "useName")%>
                                                        </td>
                                                        <td>
                                                            <%#QuyenAdmin(DataBinder.Eval(Container.DataItem, "useAdmin").ToString())%>
                                                        </td>
                                                        <td>
                                                            <%#RoleAdmin(DataBinder.Eval(Container.DataItem, "useRole").ToString())%>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtNumberOrder" runat="server" class="form-control input-sm" onkeyup="valid(this,'quotes')" onblur="valid(this,'quotes')" Text='<%#DataBinder.Eval(Container.DataItem, "useOrd").ToString()%>' OnTextChanged="txtNumberOrder_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="btnActive" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"useId")%>' CommandName="Active" class="btn btn-primary btn-xs" ToolTip="Kích hoạt"><%#ShowActive(DataBinder.Eval(Container.DataItem, "useActive").ToString())%></asp:LinkButton>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton class="btn btn-primary btn-xs" ID="btnLogin" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"useId")%>' CommandName="AutoLogin" ToolTip='<%# "Đăng nhập như: " + DataBinder.Eval(Container.DataItem,"useName")%>'><i class="fa fa-sign-out"></i></asp:LinkButton>
                                                            <asp:LinkButton class="btn btn-success btn-xs" ID="btnEdit" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"useId")%>' CommandName="Edit" ToolTip="Sửa"><i class="fa fa-pencil-square-o"></i>Sửa</asp:LinkButton>
                                                            <asp:LinkButton class="btn btn-danger btn-xs" ID="btnDel" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"useId")%>' CommandName="Del" ToolTip="Xóa" OnClientClick="javascript:return confirm('Bạn có muốn xóa?');"><i class="fa fa-trash-o"></i>Xóa</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>

                        <div class="row dataTables_wrapper">
                            <div class="col-sm-5">
                                <div id="data-table_info" class="dataTables_info" role="status" aria-live="polite">
                                    <asp:Literal ID="ltrStatistic" runat="server"></asp:Literal>
                                </div>
                            </div>

                            <div class="col-sm-7">
                                <div id="data-table_paginate" class="dataTables_paginate paging_simple_numbers">
                                    <ul class="pagination">
                                        <li id="data-table_previous" class="paginate_button previous disabled">
                                            <asp:LinkButton ID="btnPrevious" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Trước</asp:LinkButton>
                                        </li>
                                        <asp:Repeater ID="rptNumberPage" runat="server" OnItemCommand="rptNumberPage_ItemCommand" OnItemDataBound="rptNumberPage_ItemDataBound">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltrLiPage" runat="server"></asp:Literal>
                                                <asp:LinkButton ID="btn" runat="server" CommandArgument='<%# Eval("PageIndex") %>' CommandName="page" Text='<%# Eval("PageText") %> '></asp:LinkButton></li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <li id="data-table_next" class="paginate_button next">
                                            <asp:LinkButton ID="btnNext" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Sau</asp:LinkButton>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>



                </div>
            </div>

        </div>
    </asp:Panel>

    <asp:Panel ID="pnlAddForder" runat="server" Visible="false">
        <h1 class="page-header">Thêm/sửa thành viên quản trị</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse panel-with-tabs" data-sortable-id="ui-unlimited-tabs-1">
                    <div class="panel-heading p-0">
                        <div class="panel-heading-btn m-r-10 m-t-10">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        </div>
                        <div class="tab-overflow">
                            <ul class="nav nav-tabs nav-tabs-inverse">
                                <li class="prev-button"><a href="javascript:;" data-click="prev-tab" class="text-inverse"><i class="fa fa-arrow-left"></i></a></li>
                                <li class="active"><a href="#info-tab" data-toggle="tab">Thông tin thành viên</a></li>
                                <li class=""><a href="#contain-tab" data-toggle="tab">Phân quyền</a></li>
                                <li class="next-button"><a href="javascript:;" data-click="next-tab" class="text-inverse"><i class="fa fa-arrow-right"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="tab-content panel-body panel-form">
                        <asp:Panel ID="pnlErr2" runat="server" Visible="false">
                            <div class="alert alert-danger fade in" style="border-radius: 0px;">
                                <button class="close" data-dismiss="alert" type="button">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <asp:Literal ID="ltrErr2" runat="server"></asp:Literal>
                            </div>
                        </asp:Panel>
                        <div class="tab-pane fade active in" id="info-tab">
                            <div class="form-horizontal form-bordered">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Acount thành viên:</label>
                                    <div class="col-md-7">
                                        <asp:HiddenField ID="hidID" runat="server" />
                                        <asp:TextBox ID="txtUserID" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Mật khẩu thành viên:</label>
                                    <div class="col-md-7">
                                        <asp:TextBox ID="txtPassID" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Tên thành viên:</label>
                                    <div class="col-md-7">
                                        <asp:TextBox ID="txtTen" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Email:</label>
                                    <div class="col-md-7">
                                        <asp:TextBox ID="txtEmail" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Thứ tự:</label>
                                    <div class="col-md-7">
                                        <asp:TextBox ID="txtThuTu" runat="server" class="form-control" onkeyup="valid(this,'quotes')" onblur="valid(this,'quotes')">1</asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Kích hoạt:</label>
                                    <div class="col-md-7">
                                        <asp:CheckBox ID="chkKichhoat" runat="server" Checked="True" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade in" id="contain-tab">
                            <div class="form-horizontal form-bordered">


                                <div class="form-group">
                                    <label class="control-label col-md-2">Ban quản trị:</label>
                                    <div class="col-md-5">
                                        <asp:CheckBox ID="chkQuantri" runat="server" />
                                    </div>
                                    <div class="col-md-5">
                                        <span class="vntsc-help">
                                            <i class="ion-help-circled fa-1x"></i>&nbsp; Nếu không là quản trị thì chỉ quản trị đươc dữ liệu của mình
                                        </span>
                                    </div>
                                </div>

                                <asp:UpdatePanel ID="udpModule" runat="server">
                                    <ContentTemplate>

                                        <div class="form-group">
                                            <label class="control-label col-md-2">Loại user:</label>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlquantri" runat="server" AutoPostBack="True" class="form-control" OnSelectedIndexChanged="ddlquantri_SelectedIndexChanged">
                                                    <asp:ListItem Value="1">Tổng biên tập</asp:ListItem>
                                                    <asp:ListItem Value="2">Quản trị viên</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <asp:Panel ID="pnlSetPer" runat="server" Visible="false">
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Phân quyền:</label>

                                                <div class="col-md-7">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Tên module</th>
                                                                <th>Kích hoạt</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <asp:Repeater ID="rptModule" runat="server" OnItemDataBound="rptModule_ItemDataBound" OnItemCommand="rptModule_ItemCommand">
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblorder" runat="server"></asp:Label>
                                                                            <asp:HiddenField ID="hidModuleID" runat="server" Value='<%#Eval("treecode")%>' />
                                                                        </td>
                                                                        <td>
                                                                            <%#BindName(DataBinder.Eval(Container.DataItem, "treecode").ToString(),DataBinder.Eval(Container.DataItem, "name").ToString())%>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblActive" runat="server" Visible="false" Text="0"></asp:Label>
                                                                            <asp:LinkButton ID="btnActive" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"treecode")%>' CommandName="ActiveUnActive">
                                                                                <asp:Literal ID="ltrIcon" runat="server"></asp:Literal>
                                                                            </asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </tbody>

                                                    </table>
                                                </div>
                                            </div>
                                        </asp:Panel>

                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </div>
                        </div>

                        <div class="form-horizontal form-bordered">
                            <div class="form-group" style="border-top: 1px solid #eee;">
                                <label class="control-label col-md-2"></label>
                                <div class="col-md-7">
                                    <asp:LinkButton ID="btnUpdate" runat="server" class="btn btn-primary" OnClick="btnUpdate_Click">Cập nhật</asp:LinkButton>
                                    <asp:LinkButton ID="btnReset" runat="server" class="btn btn-danger" OnClick="btnReset_Click">Hủy</asp:LinkButton>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>

</div>
<script type="text/javascript">
    function checkValidation(id) {
    }
</script>
