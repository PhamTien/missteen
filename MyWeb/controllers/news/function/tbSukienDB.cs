﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class tbSukienDB
{
    #region[tbSukien_Add]
    public static bool tbSukien_Add(tbSukienDATA _tbSukienDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbSukien_Add", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@skName", _tbSukienDATA.skName));
                dbCmd.Parameters.Add(new SqlParameter("@skLevel", _tbSukienDATA.skLevel));
                dbCmd.Parameters.Add(new SqlParameter("@skOrd", _tbSukienDATA.skOrd));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbSukien_Update]
    public static bool tbSukien_Update(tbSukienDATA _tbSukienDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbSukien_Update", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@ID", _tbSukienDATA.ID));
                dbCmd.Parameters.Add(new SqlParameter("@skName", _tbSukienDATA.skName));
                dbCmd.Parameters.Add(new SqlParameter("@skOrd", _tbSukienDATA.skOrd));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbSukien_Delete]
    public static bool tbSukien_Delete(string ID)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbSukien_Delete", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbCmd.Parameters.Add(new SqlParameter("@ID", ID));
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbSukien_GetByAll]
    public static List<tbSukienDATA> tbSukien_GetByAll()
    {
        List<tbSukienDATA> list = new List<tbSukienDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbSukien_GetByAll", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbSukienDATA objtbSukienDATA = new tbSukienDATA(
                        reader["ID"].ToString(),
                        reader["skName"].ToString(),
                        reader["skLevel"].ToString(),
                        reader["skOrd"].ToString());
                        list.Add(objtbSukienDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbSukien_GetByID]
    public static List<tbSukienDATA> tbSukien_GetByID(string ID)
    {
        List<tbSukienDATA> list = new List<tbSukienDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbSukien_GetByID", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@ID", ID));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbSukienDATA objtbSukienDATA = new tbSukienDATA(
                        reader["ID"].ToString(),
                        reader["skName"].ToString(),
                        reader["skLevel"].ToString(),
                        reader["skOrd"].ToString());
                        list.Add(objtbSukienDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
}