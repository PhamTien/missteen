﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class tbPageDATA
{
    #region[Declare variables]
    private string _pagId;
    private string _pagName;
    private string _pagImage;
    private string _pagDetail;
    private string _paglevel;
    private string _pagTitle;
    private string _pagDescription;
    private string _pagKeyword;
    private string _pagType;
    private string _pagLink;
    private string _pagTarget;
    private string _pagPosition;
    private string _pagOrd;
    private string _pagActive;
    private string _catId;
    private string _grnId;
    private string _pagLang;
    private string _pagTagName;
    private string _check1;
    private string _check2;
    private string _check3;
    private string _check4;
    private string _check5;    
    #endregion
    #region[Function]
    public tbPageDATA() { }
    public tbPageDATA(string pagId_, string pagName_, string pagImage_, string pagDetail_, string paglevel_, string pagTitle_, string pagDescription_, string pagKeyword_, string pagType_, string pagLink_, string pagTarget_, string pagPosition_, string pagOrd_, string pagActive_, string catId_, string grnId_, string pagLang_, string pagTagName_, string check1_, string check2_, string check3_, string check4_, string check5_)
    {
        _pagId = pagId_;
        _pagName = pagName_;
        _pagImage = pagImage_;
        _pagDetail = pagDetail_;
        _paglevel = paglevel_;
        _pagTitle = pagTitle_;
        _pagDescription = pagDescription_;
        _pagKeyword = pagKeyword_;
        _pagType = pagType_;
        _pagLink = pagLink_;
        _pagTarget = pagTarget_;
        _pagPosition = pagPosition_;
        _pagOrd = pagOrd_;
        _pagActive = pagActive_;
        _catId = catId_;
        _grnId = grnId_;
        _pagLang = pagLang_;
        _pagTagName = pagTagName_;
        _check1 = check1_;
        _check2 = check2_;
        _check3 = check3_;
        _check4 = check4_;
        _check5 = check5_;        
    }
    #endregion
    #region[Assigned value]
    public string pagId { get { return _pagId; } set { _pagId = value; } }
    public string pagName { get { return _pagName; } set { _pagName = value; } }
    public string pagImage { get { return _pagImage; } set { _pagImage = value; } }
    public string pagDetail { get { return _pagDetail; } set { _pagDetail = value; } }
    public string paglevel { get { return _paglevel; } set { _paglevel = value; } }
    public string pagTitle { get { return _pagTitle; } set { _pagTitle = value; } }
    public string pagDescription { get { return _pagDescription; } set { _pagDescription = value; } }
    public string pagKeyword { get { return _pagKeyword; } set { _pagKeyword = value; } }
    public string pagType { get { return _pagType; } set { _pagType = value; } }
    public string pagLink { get { return _pagLink; } set { _pagLink = value; } }
    public string pagTarget { get { return _pagTarget; } set { _pagTarget = value; } }
    public string pagPosition { get { return _pagPosition; } set { _pagPosition = value; } }
    public string pagOrd { get { return _pagOrd; } set { _pagOrd = value; } }
    public string pagActive { get { return _pagActive; } set { _pagActive = value; } }
    public string catId { get { return _catId; } set { _catId = value; } }
    public string grnId { get { return _grnId; } set { _grnId = value; } }
    public string pagLang { get { return _pagLang; } set { _pagLang = value; } }
    public string pagTagName { get { return _pagTagName; } set { _pagTagName = value; } }
    public string check1 { get { return _check1; } set { _check1 = value; } }
    public string check2 { get { return _check2; } set { _check2 = value; } }
    public string check3 { get { return _check3; } set { _check3 = value; } }
    public string check4 { get { return _check4; } set { _check4 = value; } }
    public string check5 { get { return _check5; } set { _check5 = value; } }    
    #endregion
}