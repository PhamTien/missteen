﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class tbShopingcartDB
{
    #region[Create Temporary table]
    public static DataTable Cart_CreateCartDetail()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("proid");
        dt.Columns.Add("cusid");
        dt.Columns.Add("proname");
        dt.Columns.Add("price");
        dt.Columns.Add("number");
        dt.Columns.Add("money");
        dt.Columns.Add("size");
        dt.Columns.Add("stt");
        dt.Columns.Add("img");
        return dt;
    }
    #endregion
    #region[Add Temporary table]
    public static void Cart_AddProduct(ref DataTable cartdetail, string proid, string cusid, string proname, string price, string number, string size, string img)
    {
        bool has = false;
        for (int i = 0; i < cartdetail.Rows.Count; i++)
        {
            if (cartdetail.Rows[i]["proid"].ToString().Equals(proid))
            {
                has = true;
                string t = cartdetail.Rows[i]["price"].ToString();
                cartdetail.Rows[i]["number"] = int.Parse(cartdetail.Rows[i]["number"].ToString().Replace(",", "")) + int.Parse(number.ToString().Replace(",", ""));
                cartdetail.Rows[i]["price"] = int.Parse(cartdetail.Rows[i]["price"].ToString().Replace(",", ""));
                cartdetail.Rows[i]["money"] = Convert.ToDouble(cartdetail.Rows[i]["money"].ToString().Replace(",", "")) + int.Parse(price.ToString().Replace(",", ""));
                cartdetail.Rows[i]["img"] = img;
            }
        }
        if (has == false)
        {
            if (number == null || number == "")
                number = "1";
            DataRow dr = cartdetail.NewRow();
            dr["proid"] = proid;
            dr["cusid"] = cusid;
            dr["proname"] = proname;
            dr["price"] = price;
            dr["number"] = number;

            dr["money"] = Convert.ToInt32(number) * Convert.ToDouble(price);
            dr["size"] = size;
            dr["img"] = img;
            cartdetail.Rows.Add(dr);
        }
    }
    #endregion
    #region[Update amount, money]
    public static void Cart_UpdateNumber(ref DataTable cartdetail, string proid, string inumber)
    {
        if (cartdetail.Rows.Count > 0)
        {
            for (int i = 0; i < cartdetail.Rows.Count; i++)
            {
                if (cartdetail.Rows[i]["proid"].ToString().Equals(proid))
                {
                    cartdetail.Rows[i]["number"] = inumber;
                    cartdetail.Rows[i]["money"] = Convert.ToDouble(inumber) * Convert.ToDouble(cartdetail.Rows[i]["price"].ToString());
                    break;
                }
            }
        }
    }
    #endregion
    #region[Delete Temporary table]
    public static void Cart_DeleteProduct(ref DataTable cartdetail, string proid)
    {
        if (cartdetail.Rows.Count > 0)
        {
            for (int i = 0; i < cartdetail.Rows.Count; i++)
            {
                if (cartdetail.Rows[i]["proid"].ToString().Equals(proid))
                {
                    cartdetail.Rows.RemoveAt(i);
                    break;
                }
            }
        }
    }
    #endregion
    #region[tbShopingcart_Add]
    public static bool tbShopingcart_Add(tbShopingcartDATA _tbShopingcartDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbShopingcart_Add", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@proid", _tbShopingcartDATA.proid));
                dbCmd.Parameters.Add(new SqlParameter("@cus_id", _tbShopingcartDATA.cus_id));
                dbCmd.Parameters.Add(new SqlParameter("@proname", _tbShopingcartDATA.proname));
                dbCmd.Parameters.Add(new SqlParameter("@price", _tbShopingcartDATA.price));
                dbCmd.Parameters.Add(new SqlParameter("@number", _tbShopingcartDATA.number));
                dbCmd.Parameters.Add(new SqlParameter("@money", _tbShopingcartDATA.money));
                dbCmd.Parameters.Add(new SqlParameter("@createdate", _tbShopingcartDATA.createdate));
                dbCmd.Parameters.Add(new SqlParameter("@lang", _tbShopingcartDATA.lang));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbShopingcart_GetBycus_id]
    public static List<tbShopingcartDATA> tbShopingcart_GetBycus_id(string scus_id)
    {
        List<tbShopingcartDATA> list = new List<tbShopingcartDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbShopingcart_GetBycus_id", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbCmd.Parameters.Add(new SqlParameter("@cus_id", scus_id));
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbShopingcartDATA objtbShopingcartDATA = new tbShopingcartDATA(
                        reader["proid"].ToString(),
                        reader["cus_id"].ToString(),
                        reader["proname"].ToString(),
                        reader["price"].ToString(),
                        reader["number"].ToString(),
                        reader["money"].ToString(),
                        reader["createdate"].ToString(),
                        reader["lang"].ToString());
                        list.Add(objtbShopingcartDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
}