﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class tbUserDATA
{
    #region[Declare variables]
    private string _useId;
    private string _useUid;
    private string _usePid;
    private string _useName;
    private string _useAdmin;
    private string _useLevel;
    private string _useOrd;
    private string _useActive;
    private string _useMail;
    private string _useAuthorities;
    private string _useRole;
    #endregion
    #region[Function]
    public tbUserDATA() { }
    public tbUserDATA(string useId_, string useUid_, string usePid_, string useName_, string useAdmin_,
        string useLevel_, string useOrd_, string useActive_, string useMail_, string useAuthorities_, string useRole_)
    {
        _useId = useId_;
        _useUid = useUid_;
        _usePid = usePid_;
        _useName = useName_;
        _useAdmin = useAdmin_;
        _useLevel = useLevel_;
        _useOrd = useOrd_;
        _useActive = useActive_;
        _useMail = useMail_;
        _useAuthorities = useAuthorities_;
        _useRole = useRole_;
    }
    #endregion
    #region[Assigned value]
    public string useId { get { return _useId; } set { _useId = value; } }
    public string useUid { get { return _useUid; } set { _useUid = value; } }
    public string usePid { get { return _usePid; } set { _usePid = value; } }
    public string useName { get { return _useName; } set { _useName = value; } }
    public string useAdmin { get { return _useAdmin; } set { _useAdmin = value; } }
    public string useLevel { get { return _useLevel; } set { _useLevel = value; } }
    public string useOrd { get { return _useOrd; } set { _useOrd = value; } }
    public string useActive { get { return _useActive; } set { _useActive = value; } }
    public string useMail { get { return _useMail; } set { _useMail = value; } }
    public string useAuthorities { get { return _useAuthorities; } set { _useAuthorities = value; } }
    public string useRole { get { return _useRole; } set { _useRole = value; } }
    #endregion
}