﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb
{
    public partial class view_product : System.Web.UI.Page
    {
        public string strID = "";
        string lang = "vi";
        public bool showjs = false;
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (Request["id"] != null) { strID = Request["id"].ToString(); }
            if (!IsPostBack)
            {
                try
                {
                    BindData();
                }
                catch{}
            }
        }
        private void BindData()
        {
            List<tbProductDATA> list = tbProductDB.tbProduct_GetByID(strID);
            if (list.Count > 0)
            {
                hidID.Value = list[0].proId.ToString();

                rptpro.DataSource = list;
                rptpro.DataBind();
            }
        }

        public string BindImageSlide(string strProImage, string strProTitle, string strType)
        {
            string str = "";
            tbProduct dbPro = db.tbProducts.FirstOrDefault(s => s.proId == int.Parse(strID));
            string alt = dbPro.proAlt != "" && dbPro.proAlt != null ? dbPro.proAlt : dbPro.proName;
            string[] arrImage = new string[10];
            if (strProImage != null && strProImage.Length > 0)
                arrImage = strProImage.Split(Convert.ToChar(","));
            arrImage = arrImage.Where(s => s != null && s.Length > 0).ToArray();
            if (arrImage.Count() > 0)
            {
                if (arrImage.Count() > 1)
                {
                    if (strType == "1")
                    {
                        str += "<div id=\"sync1\" class=\"owl-carousel\">";
                    }
                    else
                    {
                        str += "<div id=\"sync2\" class=\"owl-carousel\">";
                    }
                    

                    showjs = true;
                    for (int i = 0; i < arrImage.Count(); i++)
                    {
                        if (strType == "1")
                        {
                            str += "<div class=\"item\"><img class=\"cloudzoom\" src=\"" + arrImage[i] + "\" data-cloudzoom=\"zoomSizeMode:'image',autoInside: 100\" /></div>";
                        }
                        else
                        {
                            str += "<div class=\"item\"><img src=\"" + arrImage[i] + "\" /></div>";
                        }

                    }
                    str += "</div>";
                }
                else
                {
                    if (strType == "1")
                    {
                        str += "<div class=\"view-index\"><img src=\"" + arrImage[0] + "\" /></div>";
                    }
                }
            }
            return str;
        }

        public string BindSalePrice(string Price1, string Price2, string priceType)
        {
            string str = "";
            try
            {
                string price = !String.IsNullOrEmpty(Price1) && int.Parse(Price1) > 0 ? Price1 : "0";
                price = !String.IsNullOrEmpty(Price2) && int.Parse(price) > int.Parse(Price2) && Price2 != "0" ? Price2 : price;
                if (price != "0")
                    str += "<span class=\"color-text-second\">" + string.Format("{0:N0} " + priceType, double.Parse(price)) + "</span>";
                else
                    str += "<span class=\"color-text-second\">" + MyWeb.Global.GetLangKey("price_view") + "</span>";
            }
            catch
            {
                str += "<span class=\"color-text-second\">" + MyWeb.Global.GetLangKey("price_view") + "</span>";
            }
            return str;
        }

        protected string Status(string stt)
        {
            return stt == "0" ? MyWeb.Global.GetLangKey("view_con_hang") : MyWeb.Global.GetLangKey("view_het_hang");
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            int pid = int.Parse(hidID.Value);
            tbProduct obj = db.tbProducts.SingleOrDefault(c => c.proId == pid && c.proLang == lang);
            string rqurl = "/" + obj.proTagName + ".html";
            string tinhtrang = obj.proStatus.ToString();

            if (tinhtrang.Contains("0"))
            {
                DataTable cartdt = new DataTable();
                if (Session["prcart"] != null)
                {
                    cartdt = (DataTable)Session["prcart"];
                }
                else
                {
                    Session["prcart"] = tbShopingcartDB.Cart_CreateCartDetail();
                    cartdt = (DataTable)Session["prcart"];
                }
                string cost = "0";

                if (obj != null)
                {
                    if (obj.proPrice != "")
                    {
                        cost = obj.proPrice;
                    }
                    tbShopingcartDB.Cart_AddProduct(ref cartdt, obj.proId.ToString(), "0", obj.proName, cost, idsoluong3.Value, "1", obj.proImage.Split(Convert.ToChar(","))[0]);
                }
                Session["prcart"] = cartdt;

                Response.Write("<script>alert('Thêm vào giỏ hàng thành công!');</script>");
                Response.Write("<script>window.location='" + rqurl + "'</script>");
            }
            else
            {
                Response.Write("<script>alert('Sản phẩm này đã hết bạn vui lòng đặt mua sản phẩm khác!');</script>");
                Response.Write("<script>window.location='" + rqurl + "'</script>");
            }
        }

        protected void btnAddNow_Click(object sender, EventArgs e)
        {
            int pid = int.Parse(hidID.Value);
            tbProduct obj = db.tbProducts.SingleOrDefault(c => c.proId == pid && c.proLang == lang);
            string rqurl = "/" + obj.proTagName + ".html";
            string tinhtrang = obj.proStatus.ToString();

            if (tinhtrang.Contains("0"))
            {
                DataTable cartdt = new DataTable();
                if (Session["prcart"] != null)
                {
                    cartdt = (DataTable)Session["prcart"];
                }
                else
                {
                    Session["prcart"] = tbShopingcartDB.Cart_CreateCartDetail();
                    cartdt = (DataTable)Session["prcart"];
                }
                string cost = "0";

                if (obj != null)
                {
                    if (obj.proPrice != "")
                    {
                        cost = obj.proPrice;
                    }
                    tbShopingcartDB.Cart_AddProduct(ref cartdt, obj.proId.ToString(), "0", obj.proName, cost, idsoluong3.Value, "1", obj.proImage.Split(Convert.ToChar(","))[0]);
                }
                Session["prcart"] = cartdt;
                Response.Redirect("/gio-hang.html");
            }
            else
            {
                Response.Write("<script>alert('Sản phẩm này đã hết bạn vui lòng đặt mua sản phẩm khác!');</script>");
                Response.Write("<script>window.location='" + rqurl + "'</script>");
            }
        }
    }
}