﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace MyWeb
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class webService : System.Web.Services.WebService
    {
        private string lang = "";
        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [System.Web.Services.WebMethod(EnableSession = true)]
        public List<entProductSearch> getProductList()
        {
            dataAccessDataContext db = new dataAccessDataContext();
            lang = MyWeb.Global.GetLang();
            if (GlobalClass.pageProducts == false)
            {
                var _ds = db.tbNews.Where(s => s.newActive == 0 && s.newLang == lang).OrderByDescending(s => s.newCreateDate).ToList();
                List<entProductSearch> arrProductSearch = new List<entProductSearch>();

                if (_ds.Count() > 0)
                {
                    for (int i = 0; i < _ds.Count(); i++)
                    {
                        entProductSearch item = new entProductSearch();
                        item.id = int.Parse(_ds[i].newid.ToString());
                        item.strName = _ds[i].newName;

                        if (_ds[i].newImage != null && _ds[i].newImage.Length > 0)
                        {
                            item.strImg = _ds[i].newImage.Split(',')[0];
                        }
                        arrProductSearch.Add(item);

                    }
                }

                return arrProductSearch;
            }
            else
            {
                var _ds = db.tbProducts.Where(s => s.proActive == 1 && s.proLang == lang).OrderByDescending(s => s.proId).ToList();
                List<entProductSearch> arrProductSearch = new List<entProductSearch>();
                if (_ds.Count() > 0)
                {
                    for (int i = 0; i < _ds.Count(); i++)
                    {
                        entProductSearch item = new entProductSearch();
                        item.id = int.Parse(_ds[i].proId.ToString());
                        item.strName = _ds[i].proName;

                        if (_ds[i].proImage != null && _ds[i].proImage.Length > 0)
                        {
                            item.strImg = _ds[i].proImage.Split(',')[0];
                        }
                        arrProductSearch.Add(item);
                    }
                }
                return arrProductSearch;
            }

        }
        public class entProductSearch
        {
            private int _id;
            public int id
            {
                get { return _id; }
                set { _id = value; }
            }

            private string _strName;
            public string strName
            {
                get { return _strName; }
                set { _strName = value; }
            }

            private string _strImg;
            public string strImg
            {
                get { return _strImg; }
                set { _strImg = value; }
            }
        }
    }
}
