﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb
{
    public partial class add_cart_product : System.Web.UI.Page
    {
        public string strID = "";
        string lang = "vi";
        public string strPrice = "0";
        DataTable cartdt = new DataTable();
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lang = MyWeb.Global.GetLang();
                if (Request["id"] != null) { strID = Request["id"].ToString(); }
                if (Session["prcart"] != null) { cartdt = (DataTable)Session["prcart"]; } else { Session["prcart"] = tbShopingcartDB.Cart_CreateCartDetail(); cartdt = (DataTable)Session["prcart"]; }

                if (GlobalClass.chkLogin == "1")
                {
                    if (Session["uidr"] != null)
                    {
                        ltrThanhToan.Text = "<a class=\"btn-add-card-op order-now\" href=\"/thanh-toan.html\" title=\"" + MyWeb.Global.GetLangKey("button-order") + "\"><span>" + MyWeb.Global.GetLangKey("button-order") + "</span></a>";
                    }
                    else
                    {
                        ltrThanhToan.Text = "<a class=\"btn-add-card-op order-now\" href=\"javascript:{}\" onclick=\"clickOrder()\" title=\"" + MyWeb.Global.GetLangKey("button-order") + "\"><span>" + MyWeb.Global.GetLangKey("button-order") + "</span></a>";
                    }
                }
                else
                {
                    ltrThanhToan.Text = "<a class=\"btn-add-card-op order-now\" href=\"/thanh-toan.html\" title=\"" + MyWeb.Global.GetLangKey("button-order") + "\"><span>" + MyWeb.Global.GetLangKey("button-order") + "</span></a>";
                }

                if (!IsPostBack)
                {
                    try
                    {
                        AddiTemCart();
                        BindData();
                    }
                    catch { }
                }
            }
            catch{}
            
        }

        void AddiTemCart()
        {
            if (Request["id"] != null)
            {
                List<tbProductDATA> objList = tbProductDB.tbProduct_GetByID(strID);
                if (objList.Count > 0)
                {
                    string strStatus = objList[0].proStatus.ToString();
                    if (strStatus.Contains("0"))
                    {
                        if (objList[0].proPrice != "")
                        {
                            strPrice = objList[0].proPrice;
                        }
                        tbShopingcartDB.Cart_AddProduct(ref cartdt, objList[0].proId.ToString(), "0", objList[0].proName, strPrice, "1", "1", objList[0].proImage.Split(Convert.ToChar(","))[0]);
                        objList.Clear();
                        objList = null;
                    }
                    else
                    {
                        ltrErr.Text = "Sản phẩm này đã hết bạn vui lòng đặt mua sản phẩm khác!";
                    }
                }
            }
        }

        void BindData()
        {
            if (cartdt.Rows.Count > 0)
            {
                rptDanhsach.DataSource = cartdt;
                rptDanhsach.DataBind();
                Double Tongtien = 0;
                for (int i = 0; i < cartdt.Rows.Count; i++)
                {
                    Tongtien = Tongtien + Convert.ToDouble(cartdt.Rows[i]["money"].ToString());
                }
                lblTongtien.Text = String.Format("{0:N0}", Tongtien) + " " + GlobalClass.CurrencySymbol;
                Session["prcart"] = cartdt;
            }
            else
            {
                rptDanhsach.DataSource = null;
                rptDanhsach.DataBind();
                lblTongtien.Text = "";
            }
            for (int i = 0; i < cartdt.Rows.Count; i++)
            {
                Label stt = (Label)rptDanhsach.Items[i].FindControl("lblstt");
                stt.Text = (i + 1).ToString();
            }
        }

        protected void rptDanhsach_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            DataTable dt = new DataTable();
            string strName = e.CommandName.ToString();
            strID = e.CommandArgument.ToString();
            string surl = Request.Url.ToString();
            int index = e.Item.ItemIndex;
            switch (strName)
            {
                case "Update":
                    TextBox txtSL = (TextBox)rptDanhsach.Items[index].FindControl("txtSL");
                    if (txtSL.Text != "" && txtSL.Text != "0")
                    {
                        List<tbProductDATA> list = tbProductDB.tbProduct_GetByID(strID);
                        if (list.Count > 0)
                        {
                            tbShopingcartDB.Cart_UpdateNumber(ref cartdt, list[0].proId.ToString(), txtSL.Text);
                        }
                        BindData();
                    }
                    break;
                case "Delete":
                    dt = cartdt;
                    List<tbProductDATA> list2 = tbProductDB.tbProduct_GetByID(strID);
                    tbShopingcartDB.Cart_DeleteProduct(ref dt, list2[0].proId);
                    cartdt = dt;
                    Session["prcart"] = cartdt;
                    BindData();
                    break;
            }
        }
    }
}