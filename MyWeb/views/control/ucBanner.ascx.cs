﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.control
{
    public partial class ucBanner : System.Web.UI.UserControl
    {
        string lang = "vi";
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            //if (!IsPostBack)
            //{
                Viewlist();
            //}
        }
        void Viewlist()
        {
            string str = "";
            List<tbAdvertiseDATA> list = tbAdvertiseDB.tbAdvertise_GetByPosition_Vietime("1");
            list = list.Where(s => s.advLang.Trim().ToLower() == lang).ToList();
            if (list.Count > 0)
            {
                if (list.Count > 0)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        string path = "";
                        path = list[i].advImage;
                        if (path.IndexOf(".swf") > 0)
                        {
                            str += "<script language='javascript'>playfile('" + path + "', '" + list[i].advWidth + "', '" + list[i].advHeight + "', true, '', '', 'link=" + list[i].advLink + "');</script>";
                        }
                        else
                        {
                            str += "<a class=\"link-logo\" href=\"" + list[0].advLink + "\" title=\"" + list[0].advName + "\" target=\"" + list[0].advTarget + "\"><img class=\"img-logo\" src=\"" + path + "\" width=\"" + list[0].advWidth + "\" height=\"" + list[0].advHeight + "\" alt=\"" + list[0].advName + "\"/></a>";
                        }
                    }
                }
            }
            ltrbanner.Text = str;
            list.Clear();
            list = null;
        }
    }
}