﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucLinkwebsite.ascx.cs" Inherits="MyWeb.views.control.ucLinkwebsite" %>

<%if (showAdv)
  {%>
<div class="box-linkweb">
    <div class="header"><%= MyWeb.Global.GetLangKey("link_website")%></div>
    <div class="body-adv">
        <asp:Literal ID="ltrLinkWeb" runat="server"></asp:Literal>
    </div>
</div>

<script type="text/javascript">
    function changeLink(url) {
        var win = window.open(url, '_blank');
        win.focus();
    }
</script>
<%} %>
