﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.control
{
    public partial class ucLogin : System.Web.UI.UserControl
    {
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            Session.Remove("um");
            Session.Remove("uidr");
            Session["um"] = null;
            Session["uidr"] = null;
            Response.Redirect("/");
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string user = txtUser.Text;
            string pass = vmmsclass.Encodingvmms.Encode(txtPass.Text);
            List<tbMember> lst = db.tbMembers.Where(s => s.uID == user && s.uPas == pass).ToList();
            if (lst.Count > 0)
            {
                Session["uidr"] = lst[0].ID;
                Session["um"] = lst[0].uID;
                Session.Timeout = 60;
                Response.Redirect("/");
            }
            else
            {
                string rqurl = HttpContext.Current.Request.Url.AbsoluteUri;
                Response.Write("<script>alert('" + MyWeb.Global.GetLangKey("user_login_error") + "')</script>");
                Response.Write("<script>window.location='" + rqurl + "'</script>");
            }
        }
    }
}