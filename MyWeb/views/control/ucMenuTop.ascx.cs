﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.control
{
    public partial class ucMenuTop : System.Web.UI.UserControl
    {
        string lang = "vi";
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                BindData();
            }
        }
        private void BindData()
        {
            string strMenu = "";
            List<tbPageDATA> objPage = tbPageDB.tbPage_GetBypagPosition("4", lang).Where(s => s.pagActive == "1" && s.paglevel.Length == 5).OrderBy(s => s.pagOrd).ToList();
            foreach (tbPageDATA obj in objPage)
            {
                string strLink = obj.pagType == "3" ? obj.pagLink : obj.pagLink;
                strMenu += "<li><a href=\"" + strLink + "\" target=\"" + obj.pagTarget + "\">";
                if (obj.pagImage.Contains("<i"))
                {
                    strMenu += obj.pagImage + obj.pagName;
                }
                else if (obj.pagImage=="")
                {
                    strMenu += obj.pagName;
                }
                else
                {
                    strMenu += "<img class=\"icon-top\" src=\"" + obj.pagImage + "\" alt=\"" + obj.pagName + "\">" + obj.pagName;
                }
                strMenu += "</a></li>";
            }
            objPage.Clear();
            objPage = null;
            ltrMenuTop.Text = strMenu;
        }
    }
}