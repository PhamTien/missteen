﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucBreadcrumb.ascx.cs" Inherits="MyWeb.views.control.ucBreadcrumb" %>
<div class="box-breadcumb">
    <ul class="breadcum">
        <asp:Literal ID="ltrBreadcumb" runat="server"></asp:Literal>
    </ul>
</div>
