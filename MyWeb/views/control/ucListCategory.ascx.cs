﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.control
{
    public partial class ucListCategory : System.Web.UI.UserControl
    {
        string lang = "vi";
        string strCurLevel = "";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                strCurLevel = getCurrentLevel();
                BindData();
            }
        }

        private void BindData()
        {
            string _strMenu = "";
            List<tbPageDATA> _objList = tbPageDB.tbPage_GetByAll(lang);
            _objList = _objList.Where(s => s.pagType == pageType.GP && s.pagActive == "1" && s.check2 == "1").ToList();
            List<tbPageDATA> _objListSub = _objList.Where(s => s.paglevel.Length == 5).ToList();
            foreach (tbPageDATA obj in _objListSub)
            {
                if (MyWeb.Common.StringClass.Check(obj.pagLink) && obj.pagLink != "#")
                    _strMenu += "<div class=\"header\"><a href=\"" + obj.pagTagName + ".html\" target=\"" + obj.pagTarget + "\">" + obj.pagName + "</a></div>";
                else
                    _strMenu += "<div class=\"header\"><a href=\"" + obj.pagTagName + ".html\" target=\"" + obj.pagTarget + "\">" + obj.pagName + "</a></div>";

                List<tbPageDATA> list = _objList.Where(s => s.paglevel.StartsWith(obj.paglevel) && s.paglevel.Length > obj.paglevel.Length).ToList();
                int ilevel = 0, iblevel = 0, k = 0, n = 1, ilength = 5, istartlevel = 1;
                string _strLevel = "";
                if (list.Count > 0)
                {
                    for (int j = 0; j < list.Count; j++)
                    {
                        string _strLiClass = "";
                        _strLevel = list[j].paglevel;
                        ilevel = (_strLevel.Length / ilength) - istartlevel;
                        if (ilevel > iblevel)
                        {
                            _strMenu += ilevel == 1 ? string.Format("<ul id=\"{0}\" class=\"{1}\">", "nav-menu-left", "nav-menuleft") : "<ul>";
                        }
                        if (ilevel < iblevel)
                        {
                            for (k = 1; k <= (iblevel - ilevel); k++)
                            {
                                _strMenu += "</ul>";
                                if (iblevel > 1) { _strMenu += "</li>"; }
                            }
                            iblevel = ilevel;
                        }
                        string strName = list[j].pagName;
                        if ((strCurLevel.Length > 0 && strCurLevel.Length >= list[j].paglevel.Length && list[j].paglevel == strCurLevel.Substring(0, list[j].paglevel.Length)) || (list[j].pagLink.Length > 1 && (Request.Url.AbsolutePath.Contains(list[j].pagLink) || Request.RawUrl.Contains(list[j].pagLink))))
                        {
                            _strLiClass = "active";
                        }
                        string lastClass = j == list.Count - 1 ? "last" : "";
                        string iconClass = tbPageDB.tbPage_ExitstByLevel(_strLevel).Count > 0 ? ilevel == 1 ? "itop" : "icon" : "";
                        if (iconClass.Length > 0)
                            _strLiClass += " " + iconClass;
                        if (lastClass.Length > 0)
                            _strLiClass += " " + lastClass;
                        if (_strLiClass.Length > 0)
                            _strLiClass = string.Format(" class=\"{0}\"", _strLiClass.Trim());

                        string Link = list[j].pagTagName + ".html";
                        _strMenu += "<li" + _strLiClass + "><a href=\"" + Link + "\" target=\"" + list[j].pagTarget + "\">" + list[j].pagName + "</a>";
                        if (tbPageDB.tbPage_ExitstByLevel(_strLevel).Count == 0)
                        {
                            _strMenu += "</li>";
                        }
                        iblevel = ilevel;
                        if (n == list.Count)
                        {
                            for (int v = 0; v < iblevel; v++)
                            {
                                _strMenu += "</ul>";
                                if (iblevel > 1) { _strMenu += "</ul></li>"; }
                            }
                        }
                        n++;
                    }
                    _strMenu += "</ul>";
                }
                list.Clear();
                list = null;
            }
            _objList.Clear();
            _objList = null;
            ltrCategory.Text = _strMenu;
        }
        private string getCurrentLevel()
        {
            string culevel = "";
            string curLink = "";
            tbPage curPage = null;
            var curURL = Request.RawUrl;
            curLink = Request["hp"] != null ? Request["hp"] : curURL.Substring(curURL.LastIndexOf("/") + 1);
            curLink = Request["hp"] != null ? curLink + ".html" : curLink;
            if (Request["hp"] != null && Request["hp"].ToString() != "")
            {
                string rqq = Request["hp"].ToString();
                curPage = db.tbPages.FirstOrDefault(s => s.pagTagName == rqq && s.pagType == 100);
            }
            var homePage = db.tbPages.FirstOrDefault(s => s.pagLink == "/" && s.pagTagName.Contains("trang-chu"));
            if (homePage != null)
                culevel = homePage.paglevel;
            //viet them
            try
            {
                string pagtag = Request["hp"] != null ? Request["hp"] : curURL.Substring(curURL.LastIndexOf("/") + 1);
                var pagtytin = db.tbPages.Where(u => u.pagTagName == pagtag).FirstOrDefault();
                if (pagtytin.pagType == 800)
                {
                    var tbnew = db.tbNews.FirstOrDefault(u => u.newTagName == pagtag);
                    var data = db.tbPages.Where(u => u.pagId == tbnew.grnID).FirstOrDefault();
                    var xx = db.tbPages.Where(u => u.paglevel == data.paglevel.Substring(0, 5)).FirstOrDefault();
                    string chuoi = xx.pagTagName + ".html";
                    var kq = db.tbPages.Where(u => u.pagLink == chuoi).FirstOrDefault();
                    return kq.paglevel.Substring(0, 5);
                }
            }
            catch {

            }
            if (curPage != null)
            {
                return curPage.paglevel;
            }
            else
            {
                return culevel;
            }

        }
    }
}