﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.control
{
    public partial class ucMenuMain : System.Web.UI.UserControl
    {
        string lang = "vi";
        string strCurLevel = "";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                strCurLevel = getCurrentLevel();
                BindMenuMain();
                ltrMarquee.Text = GlobalClass.conContact;
            }
        }

        private void BindMenuMain()
        {
            string strActive = "";
            string strRequest = Request.QueryString["e"];
            string strUrl = Request.Url.AbsolutePath.ToString().ToLower();
            int intHome = 0;
            switch (strRequest)
            {
                case "":
                    strActive = "/";
                    intHome += 1;
                    break;
                case "lh":
                    strActive = "/lien-he";
                    intHome += 1;
                    break;
                case "pro_latest":
                    strActive = "/san-pham-moi";
                    intHome += 1;
                    break;
                case "pro":
                    strActive = "/san-pham";
                    intHome += 1;
                    break;
            }
            string _strMenu = "";
            List<tbPageDATA> _objList = tbPageDB.tbPage_GetBypagPosition("1", lang);

            if (_objList.Count > 0)
            {
                int ilevel = 0, iblevel = 0, ilength = 5, istartlevel = 0, k = 0, n = 1;
                string _strLevel = "";
                for (int j = 0; j < _objList.Count; j++)
                {
                    string _strLiClass = "";
                    _strLevel = _objList[j].paglevel;
                    ilevel = (_strLevel.Length / ilength) - istartlevel;
                    if (ilevel > iblevel)
                    {
                        _strMenu += ilevel == 1 ? string.Format("<ul id=\"{0}\" class=\"nav-menu\">", "nav-main-menu") : "<ul class=\"sub-menu\">";
                    }
                    if (ilevel < iblevel)
                    {
                        for (k = 1; k <= (iblevel - ilevel); k++)
                        {
                            _strMenu += "</ul>";
                            if (iblevel > 1) { _strMenu += "</li>"; }
                        }
                        iblevel = ilevel;
                    }
                    string _strName = _objList[j].pagName;

                    if (intHome > 0)
                    {
                        if (_objList[j].pagLink == strActive)
                        {
                            _strLiClass = "active";
                        }
                    }
                    else
                    {
                        if ((strCurLevel.Length > 0 && strCurLevel.Length >= _objList[j].paglevel.Length && _objList[j].paglevel == strCurLevel.Substring(0, _objList[j].paglevel.Length)) || (_objList[j].pagLink.Length > 1 && (Request.Url.AbsolutePath.Contains(_objList[j].pagLink) || Request.RawUrl.Contains(_objList[j].pagLink))))
                        {
                            _strLiClass = "active";
                        }
                    }

                    string lastClass = j == _objList.Count - 1 ? "last" : "";
                    string iconClass = tbPageDB.tbPage_ExitstByLevel(_strLevel).Count > 0 ? ilevel == 1 ? "itop" : "icon" : "";
                    if (iconClass.Length > 0)
                        _strLiClass += " " + iconClass;
                    if (lastClass.Length > 0)
                        _strLiClass += " " + lastClass;
                    if (_strLiClass.Length > 0)
                        _strLiClass = string.Format(" class=\"{0}\"", _strLiClass.Trim());

                    string _strUrlOut = "";
                    string _strLink = "";

                    if (_objList[j].pagType == "3")
                    {
                        _strUrlOut = MyWeb.Common.StringClass.GetContent(_objList[j].pagLink, 7);
                        if (!(_strUrlOut == "http://..."))
                        {
                            _strUrlOut = "http://";
                        }
                        else
                        {
                            _strUrlOut = "";
                        }
                    }
                    _strLink = _objList[j].pagType == "3" ? _objList[j].pagLink : _objList[j].pagLink;

                    _strMenu += "<li" + _strLiClass + "><span class=\"span-menu\"></span><a href=\"" + _strLink + "\" target=\"" + _objList[j].pagTarget + "\">" + _objList[j].pagName + "</a>";

                    if (tbPageDB.tbPage_ExitstByLevel(_strLevel).Count == 0)
                    {
                        _strMenu += "</li>";
                    }

                    iblevel = ilevel;
                    if (n == _objList.Count)
                    {
                        k = 0;
                        for (k = iblevel - 1; k == 1; k--)
                        {
                            _strMenu += "</ul>";
                            if (iblevel > 1) { _strMenu += "</ul></li>"; }
                        }
                    }
                    n++;
                }
                _strMenu += "</ul>";
            }
            _objList.Clear();
            _objList = null;
            ltrMainMenu.Text = _strMenu;
        }

        private string getCurrentLevel()
        {
            string culevel = "";
            string curLink = "";
            var curURL = Request.RawUrl;
            curLink = Request["hp"] != null ? Request["hp"] : curURL.Substring(curURL.LastIndexOf("/") + 1);
            curLink = Request["hp"] != null ? curLink + ".html" : curLink;
            var curPage = db.tbPages.FirstOrDefault(s => s.pagLink == curLink && s.pagType < 100);
            var homePage = db.tbPages.FirstOrDefault(s => s.pagLink == "/" && s.pagTagName.Contains("trang-chu"));
            if (homePage != null)
                culevel = homePage.paglevel;
            try
            {
                string pagtag = Request["hp"] != null ? Request["hp"] : curURL.Substring(curURL.LastIndexOf("/") + 1);
                var pagtytin = db.tbPages.Where(u => u.pagTagName == pagtag).FirstOrDefault();
                if (pagtytin.pagType == 800)
                {
                    var tbnew = db.tbNews.FirstOrDefault(u => u.newTagName == pagtag);
                    var data = db.tbPages.Where(u => u.pagId == tbnew.grnID).FirstOrDefault();
                    var xx = db.tbPages.Where(u => u.paglevel == data.paglevel.Substring(0, 5)).FirstOrDefault();
                    string chuoi = xx.pagTagName + ".html";
                    var kq = db.tbPages.Where(u => u.pagLink == chuoi).FirstOrDefault();
                    return kq.paglevel.Substring(0, 5);
                }
                else if (pagtytin.pagType == 900)
                {
                    var objPro = db.tbProducts.FirstOrDefault(s => s.proTagName == pagtag);
                    var _objPage = db.tbPages.Where(u => u.pagId == int.Parse(objPro.catId)).FirstOrDefault();
                    var xx = db.tbPages.Where(u => u.paglevel == _objPage.paglevel.Substring(0, 5)).FirstOrDefault();
                    string chuoi = xx.pagTagName + ".html";
                    var kq = db.tbPages.Where(u => u.pagLink == chuoi).FirstOrDefault();
                    if (kq != null)
                    {
                        return kq.paglevel.Substring(0, 5);
                    }
                    else
                    {
                        var objPage = db.tbPages.FirstOrDefault(s => s.pagType == int.Parse(pageType.MP) && s.paglevel.Length == 5);
                        return objPage.paglevel;
                    }


                }
            }
            catch
            {

            }
            if (curPage != null)
            {
                return curPage.paglevel;
            }
            else
            {
                return culevel;
            }
        }
    }
}