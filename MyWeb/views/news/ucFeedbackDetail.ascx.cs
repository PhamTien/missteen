﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.news
{
    public partial class ucFeedbackDetail : System.Web.UI.UserControl
    {
        string lang = "vi";
        string strNewTag = "";
        public string strLink = "";
        public string strUrl = "";
        int iEmptyIndex = 0;
        string currentPage;
        public string strUrlFace = "";
        public string wf = "770";
        public string nf = "5";
        public string cf = "light";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            strUrl = HttpContext.Current.Request.RawUrl;
            if (Request["hp"] != null) { strNewTag = Request["hp"].ToString(); }
            iEmptyIndex = strNewTag.IndexOf("?");
            if (iEmptyIndex != -1)
            {
                strNewTag = strNewTag.Substring(0, iEmptyIndex);
            }
            currentPage = Request.QueryString["page"] != null ? Request.QueryString["page"] : "1";
            try { Convert.ToInt16(currentPage); }
            catch { currentPage = "1"; }
            if (GlobalClass.widthfkComment != null) { wf = GlobalClass.widthfkComment; }
            if (GlobalClass.numfkComment != null) { nf = GlobalClass.numfkComment; }
            if (GlobalClass.colorfkComment != null) { cf = GlobalClass.colorfkComment; }

            if (!IsPostBack)
            {
                BindData();
            }
        }
        private void BindData()
        {
            List<tbContact> objNews = db.tbContacts.Where(s => s.conwebsite == strNewTag && s.conLang == lang).ToList();
            List<tbContactDATA> _objNewsOrther = tbContactDB.tbContact_Paging("1", GlobalClass.viewNews5, "*", "conActive = 1", "conDate desc,  conId  desc");
            strUrlFace = Request.Url.ToString();
            if (objNews.Count > 0)
            {
                ltrNewName.Text = objNews[0].conName;
                rptNewsDetails.DataSource = objNews;
                rptNewsDetails.DataBind();

                if (objNews[0].conName != null && objNews[0].conName != "")
                {
                    string strt = "<b>Tag</b>: ";
                    string tag = objNews[0].conName;
                    tag = tag.Trim();
                    string[] arrTag = tag.Split(Convert.ToChar(";"));
                    if (arrTag.Count() > 1)
                    {
                        for (int i = 0; i < arrTag.Count(); i++)
                        {
                            string t = arrTag[i].Replace(" ", "-");
                            strt += "<a rel='tagn' href='/tagn_" + t + ".html'>" + t + "</a>";
                        }
                    }
                    else
                    {
                        tag = tag.Replace(",", "");
                        strt += "<a rel='tagn' href='/tagn_" + tag + ".html'>" + tag + "</a>";
                    }
                    ltrTag.Text = "";
                }
                else
                {
                    ltrTag.Text = "";
                }
                if (_objNewsOrther.Count > 0)
                {
                    rptNewsOrther.DataSource = _objNewsOrther;
                    rptNewsOrther.DataBind();
                    pnNewsOrther.Visible = true;
                }
            }
        }
    }
}