﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.news
{
    public partial class ucFeedback : System.Web.UI.UserControl
    {
        string idcha = "";
        string grnid = "";
        string date = "";
        string lang = "vi";
        string p = "";
        string mem = "";
        string strWhere = "";
        int currentPage = 1;
        int recordCount = 0;
        int pageSize = 10;
        private bool fullContent = true;
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                currentPage = Request.RawUrl.Contains("page") ? int.Parse(common.GetParameterFromUrl(Request.RawUrl, "page")) : 1;
            }
            catch { }
            try { pageSize = int.Parse(GlobalClass.viewNews1); }
            catch { }
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {

                BindData();

            }
        }
        private string LoadWhere()
        {
            strWhere = "conActive = '0' and  conLang = '" + lang + "' ";
            List<tbContactDATA> list = tbContactDB.tbContact_GetByID(idcha);
            if (list.Count > 0)
            {
                Session["Lang"] = list[0].conLang.Trim().ToLower();
                if (GlobalClass.showSEO.Equals("true"))
                {
                    Page.Title = list[0].conName + " | " + GlobalClass.conTitle;
                }
                else
                {
                    Page.Title = list[0].conName;
                }
            }
            strWhere = "conActive = 1";
            recordCount = int.Parse(tbContactDB.tbContacts_Count("Select Count(*) from  tbContact  Where " + strWhere));
            return strWhere;
        }
        private void BindData()
        {
            LoadWhere();
            List<tbContactDATA> list = new List<tbContactDATA>();
            strWhere = "conActive = 1";
            list = tbContactDB.tbContact_Paging((currentPage).ToString(), pageSize.ToString(), "*", strWhere, "conDate desc,conId desc");

            string _str = "";
            if (list.Count > 0)
            {
                _str += "<div class=\"box-body clearfix\">";
                for (int i = 0; i < list.Count; i++)
                {
                    tbContact dbNew = db.tbContacts.FirstOrDefault(s => s.conId == Int32.Parse(list[i].conId));
                    string alt = dbNew.conCompany;
                    string link = "/" + list[i].conwebsite + ".html";
                    string image = list[i].confile != null && list[i].confile.Trim() != "" ? string.Format("<a href=\"{0}\" title=\"{1}\"><img src=\"{2}\" alt=\"{3}\" title=\"{1}\"></a>", link, list[i].conName, list[i].confile, alt) : "";
                    _str += String.Format("<div class=\"cat-news clearfix\">{2}<h3><a href=\"{0}\" title=\"{1}\">{1}</a> </h3><div class=\"clearfix\">{3}</div><div class=\"news-more\" ><a href=\"{0}\" rel=\"nofollow\">" + MyWeb.Global.GetLangKey("news_more") + "</a></div></div>", link, list[i].conName, image, fullContent ? list[i].conDetail.ToString() : MyWeb.Common.StringClass.GetContent(list[i].conDetail.ToString(), 185));
                }
                _str += "</div>";
                ltrlist.Text = _str;
            }
            ltrPaging.Text = common.PopulatePager(recordCount, currentPage, pageSize);
        }
        void BindDataAll()
        {
            string curUrl = Request.RawUrl;
            string[] mang = curUrl.Split('=');
            string page = "1";
            if (mang.Length == 2)
            {
                if (mang[1] != null)
                {
                    page = mang[1];
                    currentPage = Convert.ToInt32(page) - 1;
                }
            }
            else
            {
                currentPage = 0;
            }
            tbPage gn = db.tbPages.Where(s => s.pagTagName == grnid.Replace(".html", "") && s.pagType == 200).FirstOrDefault();
            var listpage = db.tbPages.Where(u => u.pagActive == 1 && u.paglevel.Substring(0, gn.paglevel.Length) == gn.paglevel).Select(u => u.pagId).ToArray();
            string _str = "";
            IEnumerable<tbContact> news;
            var newall = db.tbContacts.Where(u => u.conActive == 0 && u.conLang == lang && listpage.Contains(u.conId)).OrderByDescending(u => u.conId).ToList();
            news = newall.Skip(currentPage * pageSize).Take(pageSize).ToList();
            recordCount = newall.Count;
            if (news.Count() > 0)
            {
                _str += "<div class=\"box-body homepage clearfix\">";
                string col_tem1 = "", col_tem2 = "";
                if (GlobalClass.viewNews10 == "3")
                {
                    col_tem1 = " twocol1";
                    col_tem2 = " twocol2";
                }
                int i = 0;
                if (GlobalClass.viewNews10 != "1")
                {
                    i = 0;
                    foreach (tbContact n in news)
                    {
                        string alt = n.conCompany;
                        string link = "/" + n.conName + ".html";

                        string image = n.confile != null && n.confile.Trim() != "" ? string.Format("<a href=\"{0}\" title=\"{1}\"><img src=\"{2}\" alt=\"{3}\" title=\"{1}\"></a>", link, n.conName, n.confile, alt) : "";
                        if (i == 0)
                            _str += String.Format("<div class=\"cat-news" + col_tem1 + " clearfix\">{2}<h3><a href=\"{0}\" title=\"{1}\">{1}</a> </h3><div class=\"clearfix\">{3}</div><div class=\"news-more\"><a href=\"{0}\">" + MyWeb.Global.GetLangKey("news_more") + "</a></div></div>", link, n.conName, image, fullContent ? n.conDetail.ToString() : MyWeb.Common.StringClass.GetContent(n.conDetail.ToString(), 185));
                        else
                        {
                            if (i == 1)
                                _str += "<div class=\"cat-morenews" + col_tem2 + " clearfix\"><ul>";
                            _str += String.Format("<li><a href=\"" + link + "\" title=\"" + n.conName + "\">" + n.conName + "</a> </li>");
                            if (i == news.Count() - 1)
                                _str += "</ul></div>";
                        }
                        i++;
                    }
                }
                else
                {
                    foreach (tbContact n in news)
                    {
                        string alt = n.conName;
                        string link = "/" + n.conName + ".html";
                        string image = n.confile != null && n.confile.Trim() != "" ? string.Format("<a href=\"{0}\" title=\"{1}\"><img src=\"{2}\" alt=\"{3}\" title=\"{1}\"></a>", link, n.conName, n.confile, alt) : "";
                        _str += String.Format("<div class=\"cat-news clearfix\">{2}<h4><a href=\"{0}\" title=\"{1}\">{1}</a> </h4><div class=\"clearfix\">{3}</div><div class=\"news-more\"><a href=\"{0}\">" + MyWeb.Global.GetLangKey("news_more") + "</a></div></div>", link, n.conName, image, fullContent ? n.conDetail.ToString() : MyWeb.Common.StringClass.GetContent(n.conDetail.ToString(), 185));
                        i++;
                    }
                }
                _str += "<div class=\"clear\"></div>";
                _str += "</div>";
            }
            ltrPaging.Text = common.PopulatePager(recordCount, (currentPage + 1), pageSize);
            ltrlist.Text = _str;
        }
    }
}