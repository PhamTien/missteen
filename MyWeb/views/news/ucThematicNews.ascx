﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucThematicNews.ascx.cs" Inherits="MyWeb.views.news.ucThematicNews" %>

<%if (showAdv)
  {%>
<div class="box-news-thematic">
    <div class="header"><%= MyWeb.Global.GetLangKey("header-thematic-news")%></div>
    <ul class="body-news">
        <asp:Literal ID="ltrThematic" runat="server"></asp:Literal>
    </ul>
</div>

<%} %>