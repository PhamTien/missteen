﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.library
{
    public partial class ucListAllVideo : System.Web.UI.UserControl
    {
        string lang = "vi";
        dataAccessDataContext db = new dataAccessDataContext();        
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                BindVideo();
            }
        }
        protected void BindVideo()
        {
            var list = db.Libraries.Where(s => s.File != "" && s.Active == 1 && s.Lang == lang && s.Priority == 1).OrderByDescending(s => s.Id).ToList();

            if (list.Count() > 0)
            {
                string strKq = "";
                for (int i = 0; i < list.Count(); i++)
                {
                    strKq += "<div class='videoItem'>";
                    var urlVideo = list[i].File;
                    urlVideo = urlVideo.Replace("&feature=youtu.be", "");
                    urlVideo = urlVideo.Replace("watch?v=", "embed/");                    
                    strKq += "<div class='video-frame'><iframe width='292' height='280' src='" + urlVideo + "' frameborder='0' allowfullscreen></iframe></div>";
                    strKq += "<div class='video-name'>" + list[i].Name + "</div>";
                    strKq += "</div>";
                }
                ltrAllVideo.Text = strKq;
            }            
        }
    }
}