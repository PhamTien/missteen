﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.library
{
    public partial class ucListVideo : System.Web.UI.UserControl
    {
        string lang = "vi";
        dataAccessDataContext db = new dataAccessDataContext();
        DataSet ds = new DataSet();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                BindVideo();
            } 
        }
        protected void BindVideo()
        {
            var list = db.Libraries.Where(s => s.File != "").OrderByDescending(s => s.Id).ToList();

            if (list.Count() > 0)
            {
                string kq = "";
                kq += "<ul id='list_player'>";
                for (int i = 0; i < list.Count(); i++)
                {
                    var urlVideo = list[i].File;
                    urlVideo = urlVideo.Replace("&feature=youtu.be", "");
                    urlVideo = urlVideo.Replace("watch?v=", "embed/");
                    kq += "<li><a href='javascript:void(0)' class='videoItems' title='" + list[i].Name + "' name='" + urlVideo + "'>" + list[i].Name + "</a></li>";
                }
                kq += "</ul>";
                ltrListVideo.Text = kq;

                var firstVideo = list[0].File;
                firstVideo = firstVideo.Replace("&feature=youtu.be", "");
                firstVideo = firstVideo.Replace("watch?v=", "embed/");
                ltrIframeVideo.Text = "<iframe width='292' height='280' src='" + firstVideo + "' allowfullscreen></iframe>";
            }
            else
            {
                var list2 = db.Libraries.Where(s => s.File != "").OrderByDescending(s => s.Id).Take(15).ToList();
                if (list2.Count() > 0)
                {
                    string kq = "";
                    kq += "<ul id='list_player'>";
                    for (int i = 0; i < list2.Count(); i++)
                    {
                        var urlVideo = list2[i].File;
                        urlVideo = urlVideo.Replace("&feature=youtu.be", "");
                        urlVideo = urlVideo.Replace("watch?v=", "embed/");
                        kq += "<li><a href='javascript:void(0)' class='videoItems' title='" + list2[i].Name + "' name='" + urlVideo + "'>" + list2[i].Name + "</a></li>";
                    }
                    kq += "</ul>";
                    ltrListVideo.Text = kq;

                    var firstVideo = list2[0].File;
                    firstVideo = firstVideo.Replace("&feature=youtu.be", "");
                    firstVideo = firstVideo.Replace("watch?v=", "embed/");
                    ltrIframeVideo.Text = "<iframe width='292' height='280' src='" + firstVideo + "' allowfullscreen></iframe>";
                }
                
            }
        }
    }
}