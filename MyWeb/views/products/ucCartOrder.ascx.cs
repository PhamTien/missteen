﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.products
{
    public partial class ucCartOrder : System.Web.UI.UserControl
    {
        string lang = "vi";
        DataTable cartdt = new DataTable();
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            ltrHead.Text = "Thông tin đặt hàng";
            if (Session["uidr"] != null)
            {
                tbMember mem = db.tbMembers.Where(s => s.ID == int.Parse(Session["uidr"].ToString())).FirstOrDefault();
                txtname.Text = mem.Fax;
                txtEmail.Text = mem.Email;
                txtAddress.Text = mem.Add;
                txtPhone.Text = mem.Tell;
            }
            if (Session["prcart"] != null) { cartdt = (DataTable)Session["prcart"]; } else { Session["prcart"] = tbShopingcartDB.Cart_CreateCartDetail(); cartdt = (DataTable)Session["prcart"]; }
            lang = MyWeb.Global.GetLang();
            if (cartdt.Rows.Count > 0) { btnSend.Visible = true; } else { btnSend.Visible = false; }
            if (!IsPostBack)
            {
                BindPay();
            }
        }

        void BindPay()
        {
            ddlthanhtoan.Items.Clear();
            ddlthanhtoan.Items.Add(new ListItem(MyWeb.Global.GetLangKey("payment_method_1"), "1"));
            if (GlobalClass.ShowpaymentBaoKim == true && GlobalClass.baokim1.Trim() == "1") { ddlthanhtoan.Items.Add(new ListItem(MyWeb.Global.GetLangKey("payment_method_2"), "2")); }
            if (GlobalClass.ShowpaymentNganLuong == true && GlobalClass.nganluong1.Trim() == "1") { ddlthanhtoan.Items.Add(new ListItem(MyWeb.Global.GetLangKey("payment_method_3"), "3")); }
            ddlthanhtoan.Items.Add(new ListItem(MyWeb.Global.GetLangKey("payment_method_4"), "4"));
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            Session["prcart"] = cartdt;
            decimal Tongtien = 0;
            string Chuoisp = "<b>Sản phẩm đặt mua:</b><br />";
            for (int i = 0; i < cartdt.Rows.Count; i++)
            {
                decimal Tien = Convert.ToDecimal(cartdt.Rows[i]["money"].ToString());
                Tongtien = Tongtien + Tien;
                Chuoisp += " - " + cartdt.Rows[i]["proname"].ToString() + " với số lượng: " + cartdt.Rows[i]["number"].ToString() + "; số tiền: " + string.Format("{0:n0}", Tien) + "<br />";
            }
            tbCustomersDATA ObjtbCustomersDATA = new tbCustomersDATA();
            ObjtbCustomersDATA.Acount = "";
            if (Session["uidr"] != null)
            {
                ObjtbCustomersDATA.Acount = Session["uidr"].ToString();
            }
            ObjtbCustomersDATA.Pas = "";
            ObjtbCustomersDATA.name = txtname.Text;
            ObjtbCustomersDATA.mail = txtEmail.Text;
            ObjtbCustomersDATA.Address = txtAddress.Text;
            ObjtbCustomersDATA.phone = txtPhone.Text;
            ObjtbCustomersDATA.detail = txtContent.Text;
            ObjtbCustomersDATA.totalmoney = Tongtien.ToString();
            ObjtbCustomersDATA.lang = lang;
            ObjtbCustomersDATA.Tell = txtPhoneCodinh.Text;
            ObjtbCustomersDATA.Payment = ShowPay(ddlthanhtoan.SelectedValue);
            tbCustomersDB.tbCustomers_Add(ObjtbCustomersDATA);
            List<tbCustomersDATA> listmax = tbCustomersDB.tbCustomers_GetByMaxID();
            for (int i = 0; i < cartdt.Rows.Count; i++)
            {
                tbShopingcartDATA ObjtbShopingcartDATA = new tbShopingcartDATA();
                ObjtbShopingcartDATA.proid = cartdt.Rows[i]["proid"].ToString();
                ObjtbShopingcartDATA.cus_id = listmax[0].id;
                ObjtbShopingcartDATA.proname = cartdt.Rows[i]["proname"].ToString();
                ObjtbShopingcartDATA.price = cartdt.Rows[i]["price"].ToString();
                ObjtbShopingcartDATA.number = cartdt.Rows[i]["number"].ToString();
                ObjtbShopingcartDATA.money = cartdt.Rows[i]["money"].ToString();
                ObjtbShopingcartDATA.createdate = DateTime.Now.Month.ToString() + "/" + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Year.ToString();
                ObjtbShopingcartDATA.lang = lang;
                tbShopingcartDB.tbShopingcart_Add(ObjtbShopingcartDATA);
            }
            string returnurl = "http://" + Request.Url.Authority;
            tbCustomer customer = db.tbCustomers.OrderByDescending(s => s.id).FirstOrDefault();
            if (ddlthanhtoan.SelectedValue == "1" || ddlthanhtoan.SelectedValue == "4") { sendmail(Chuoisp, Tongtien); }
            else if (ddlthanhtoan.SelectedValue == "3")
            {
                String return_url = returnurl;// Địa chỉ trả về 
                String transaction_info = txtContent.Text;//Thông tin giao dịch
                String order_code = showmadonhang(customer.id.ToString()) + customer.id.ToString();//"Hãy lập trình mã hóa đơn của bạn vào đây";//Mã giỏ hàng 
                String receiver = GlobalClass.nganluong2;//Tài khoản nhận tiền 
                String price = Tongtien.ToString();//Lấy giá của giỏ hàng
                Nganluong nl = new Nganluong();
                String url;
                url = nl.buildCheckoutUrl(return_url, receiver, transaction_info, order_code, price.Trim());
                sendmail(Chuoisp, Tongtien);
                Response.Redirect(url);//Gán địa chỉ url cho nút thanh toán

            }
            else if (ddlthanhtoan.SelectedValue == "2")
            {
                String merchant_id = GlobalClass.baokim3;
                String order_id = showmadonhang(customer.id.ToString()) + customer.id.ToString();//string.Format("{0:ddMMyyyHHss}", DateTime.Now);
                String business = GlobalClass.baokim2;
                String total_amount = Tongtien.ToString();
                String shipping_fee = "";
                String tax_fee = "";
                String order_description = txtContent.Text;
                String url_success = returnurl;
                String url_cancel = "";
                String url_detail = "";
                Baokim bk = new Baokim();
                String url;
                url = bk.createRequestUrl(order_id, business, total_amount.Trim(), shipping_fee, tax_fee, order_description, url_success, url_cancel, url_detail);
                sendmail(Chuoisp, Tongtien);
                Response.Redirect(url);
            }
            txtAddress.Text = "";
            txtContent.Text = "";
            txtEmail.Text = "";
            txtname.Text = "";
            txtPhone.Text = "";
            ddlthanhtoan.SelectedValue = "1";
            txtPhoneCodinh.Text = "";
            Session["prcart"] = null;
            cartdt.Rows.Clear();
            Response.Write("<script language=javascript>alert('Thông tin đặt hàng đã thành công, chúng tôi sẽ liên hệ quý khách trong thời gian sớm nhất !');</script>");
            Response.AddHeader("refresh", "1;url=/");
        }

        public static String ShowPay(string strName)
        {
            if (strName == "1") return MyWeb.Global.GetLangKey("payment_method_1");
            else if (strName == "2") return MyWeb.Global.GetLangKey("payment_method_2");
            else if (strName == "3") return MyWeb.Global.GetLangKey("payment_method_3");
            else if (strName == "4") return MyWeb.Global.GetLangKey("payment_method_4");
            else
                return "";
        }

        public static String showmadonhang(string id)
        {
            string Chuoi = "";
            for (int i = 0; i < 5 - id.Length; i++)
            {
                Chuoi = Chuoi + "0";
            }
            return Chuoi;
        }

        protected void sendmail(string Chuoisp, decimal Tongtien)
        {
            string Noidung = "";
            Noidung = txtname.Text != "" ? "<b>Họ tên: </b>" + txtname.Text + "<br />" : "";
            Noidung += txtAddress.Text != "" ? "<b>Địa chỉ: </b>" + txtAddress.Text + "<br />" : "";
            Noidung += txtPhone.Text != "" ? "<b>Điện thoại: </b>" + txtPhone.Text + "<br />" : "";
            Noidung += txtEmail.Text != "" ? "<b>Email: </b>" + txtEmail.Text + "<br />" : "";
            Noidung += ShowPay(ddlthanhtoan.SelectedValue) != "" ? "<b>Hình thức thanh toán: </b>" + ShowPay(ddlthanhtoan.SelectedValue) + "<br />" : "";
            Noidung += "<br />" + Chuoisp;
            Noidung += "<b>Tổng số tiền:</b> " + string.Format("{0:n0}", Tongtien) + "VNĐ";
            try
            {
                common.SendMail(GlobalClass.conMail_Info, "", "", "", "Thông tin đặt hàng trên website " + Request.Url.Host + " từ: " + txtname.Text, Noidung);

                common.SendMail(txtEmail.Text.Trim(), "", "", "", "Thông tin đặt hàng trên website " + Request.Url.Host, Noidung);
            }
            catch { }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            Session["prcart"] = cartdt;
            Resetcontrol();
        }
        void Resetcontrol()
        {
            Session["prcart"] = cartdt;
            txtname.Text = "";
            txtEmail.Text = "";
            txtPhone.Text = "";
            txtAddress.Text = "";
            txtContent.Text = "";
            txtPhoneCodinh.Text = "";
            ddlthanhtoan.SelectedValue = "1";
        }
    }
}