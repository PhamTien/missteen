﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.products
{
    public partial class ucProDetail : System.Web.UI.UserControl
    {
        HttpCookie myCookie = new HttpCookie("ViewedProducts");
        HttpCookie _ViewedProducts = null;
        public string strUrl = "";
        string strProID = "";
        int iEmptyIndex = 0;
        string strDetailPage;
        string lang = "vi";
        public string strUr = "";
        public string wf = "770";
        public string nf = "5";
        public string cf = "light";
        string strCatID = "";
        int currentPage = 1;
        int recordCount = 0;
        int pageSize = 10;
        public string strUrlFace = "";
        private string strNumberView = "";
        string viewBy = GlobalClass.viewProducts9;
        string sortBy = GlobalClass.viewProducts10;
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            strUrl = HttpContext.Current.Request.UrlReferrer != null ? HttpContext.Current.Request.UrlReferrer.AbsolutePath : "";
            if (Request["hp"] != null)
            {
                strProID = Request["hp"].ToString();
            }
            iEmptyIndex = strProID.IndexOf("?");
            if (iEmptyIndex != -1)
            {
                strProID = strProID.Substring(0, iEmptyIndex);
            }
            strUrlFace = Request.Url.ToString();
            strDetailPage = Request.QueryString["dpage"] != null ? Request.QueryString["dpage"] : "1";
            try { Convert.ToInt16(strDetailPage); }
            catch { strDetailPage = "1"; }
            lang = MyWeb.Global.GetLang();
            if (GlobalClass.widthfkComment != null) { wf = GlobalClass.widthfkComment; }
            if (GlobalClass.numfkComment != null) { nf = GlobalClass.numfkComment; }
            if (GlobalClass.colorfkComment != null) { cf = GlobalClass.colorfkComment; }
            strUr = Request.Url.ToString();

            if (!IsPostBack)
            {
                try
                {
                    BindData();
                    BindTabs();
                    BindDataOther();
                    BindRelated();
                }
                catch { }
            }
        }
        private void BindData()
        {
            List<tbProductDATA> list = tbProductDB.tbProduct_GetByTagName(strProID, lang);
            strCatID = list[0].catId;
            List<tbProduct> dbPro = db.tbProducts.Where(s => s.proTagName == strProID).ToList();
           
            if (list.Count > 0)
            {
                hidID.Value = dbPro[0].proId.ToString();

                rptpro.DataSource = dbPro;
                rptpro.DataBind();
                GetListComment(dbPro[0].proId);
            }
            try
            {
                _ViewedProducts = Request.Cookies["ViewedProducts"];
                if (_ViewedProducts.Value == null)
                {
                    myCookie.Value = list[0].proId.ToString();
                    myCookie.Expires = DateTime.Now.AddDays(1d);
                    Response.Cookies.Add(myCookie);
                }
                else
                {
                    if (_ViewedProducts.Value.ToString().Contains(list[0].proId.ToString()))
                    {
                    }
                    else
                    {
                        myCookie.Value = list[0].proId.ToString() + "," + _ViewedProducts.Value;
                        myCookie.Expires = DateTime.Now.AddDays(1d);
                        Response.Cookies.Add(myCookie);
                    }

                }
            }
            catch
            {
                myCookie.Value = list[0].proId.ToString();
                myCookie.Expires = DateTime.Now.AddDays(1d);
                Response.Cookies.Add(myCookie);
            }
            list = null;
        }

        public string GetProManu(string manuId)
        {
            if (!string.IsNullOrEmpty(manuId))
            {
                var manu = db.tbPages.FirstOrDefault(x => x.pagId == int.Parse(manuId));
                if (manu != null)
                    return manu.pagName;
                else
                    return string.Empty;
            }
            else
                return string.Empty;
        }

        public string GetProStatus(string stt)
        {
            if (stt == "0")
                return "Còn hàng";
            else
                return "Hết hàng";
        }

        public string GetProDate(string prodate)
        {
            DateTime proDate = Convert.ToDateTime(prodate);
            return common.ConvertDate(proDate);
        }

        public string GetHotline()
        {
            var config = db.tbConfigs.FirstOrDefault(x => x.conLang == lang);
            if (config != null)
                return config.conTel;
            else
                return "";
        }

        public string BindImageSlide(string strProImage, string strProTitle, string strType)
        {
            string str = "";

            tbProduct dbPro = db.tbProducts.FirstOrDefault(s => s.proTagName == strProID);
            string alt = dbPro.proAlt != "" && dbPro.proAlt != null ? dbPro.proAlt : dbPro.proName;

            string[] arrImage = new string[10];
            if (strProImage != null && strProImage.Length > 0)
                arrImage = strProImage.Split(Convert.ToChar(","));
            arrImage = arrImage.Where(s => s != null && s.Length > 0).ToArray();
            if (arrImage.Count() > 0)
            {
                if (arrImage.Count() > 1)
                {
                    for (int i = 0; i < arrImage.Count(); i++)
                    {
                        if (strType == "1")
                        {
                            str += "<div class=\"item\"><img class=\"cloudzoom\" src=\"" + arrImage[i] + "\" data-cloudzoom=\"zoomSizeMode:'image',autoInside: 100\" /></div>";
                        }
                        else
                        {
                            str += "<div class=\"item\"><img src=\"" + arrImage[i] + "\" /></div>";
                        }

                    }
                }
                else
                {
                    if (strType == "1")
                    {
                        str += "<div class=\"item\"><img class=\"cloudzoom\" src=\"" + arrImage[0] + "\" data-cloudzoom='zoomSizeMode:'image',autoInside: 100' /></div>";
                    }
                }
            }
            return str;
        }

        public string BindSalePrice(string Price1, string Price2, string priceType)
        {
            string str = "";
            try
            {
                string price = !String.IsNullOrEmpty(Price1) && int.Parse(Price1) > 0 ? Price1 : "0";
                price = !String.IsNullOrEmpty(Price2) && int.Parse(price) > int.Parse(Price2) && Price2 != "0" ? Price2 : price;
                if (price != "0")
                    str += "<span class=\"color-text-second\">" + string.Format("{0:N0} " + priceType, double.Parse(price)) + "</span>";
                else
                    str += "<span class=\"color-text-second\">" + MyWeb.Global.GetLangKey("price_view") + "</span>";
            }
            catch
            {
                str += "<span class=\"color-text-second\">" + MyWeb.Global.GetLangKey("price_view") + "</span>";
            }
            return str;
        }

        protected string Status(string stt)
        {
            return stt == "0" ? MyWeb.Global.GetLangKey("view_con_hang") : MyWeb.Global.GetLangKey("view_het_hang");
        }

        protected void btnAddNow_Click(object sender, EventArgs e)
        {
            int pid = int.Parse(hidID.Value);
            tbProduct obj = db.tbProducts.SingleOrDefault(c => c.proId == pid && c.proLang == lang);
            string rqurl = "/" + obj.proTagName + ".html";
            string tinhtrang = obj.proStatus.ToString();

            if (tinhtrang.Contains("0"))
            {
                DataTable cartdt = new DataTable();
                if (Session["prcart"] != null)
                {
                    cartdt = (DataTable)Session["prcart"];
                }
                else
                {
                    Session["prcart"] = tbShopingcartDB.Cart_CreateCartDetail();
                    cartdt = (DataTable)Session["prcart"];
                }
                string cost = "0";

                if (obj != null)
                {
                    if (obj.proPrice != "")
                    {
                        cost = obj.proPrice;
                    }
                    tbShopingcartDB.Cart_AddProduct(ref cartdt, obj.proId.ToString(), "0", obj.proName, cost, idsoluong3.Value, "1", obj.proImage.Split(Convert.ToChar(","))[0]);
                }
                Session["prcart"] = cartdt;
                Response.Redirect("/gio-hang.html");
            }
            else
            {
                Response.Write("<script>alert('Sản phẩm này đã hết bạn vui lòng đặt mua sản phẩm khác!');</script>");
                Response.Write("<script>window.location='" + rqurl + "'</script>");
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            int pid = int.Parse(hidID.Value);
            tbProduct obj = db.tbProducts.SingleOrDefault(c => c.proId == pid && c.proLang == lang);
            string rqurl = "/" + obj.proTagName + ".html";
            string tinhtrang = obj.proStatus.ToString();

            if (tinhtrang.Contains("0"))
            {
                DataTable cartdt = new DataTable();
                if (Session["prcart"] != null)
                {
                    cartdt = (DataTable)Session["prcart"];
                }
                else
                {
                    Session["prcart"] = tbShopingcartDB.Cart_CreateCartDetail();
                    cartdt = (DataTable)Session["prcart"];
                }
                string cost = "0";

                if (obj != null)
                {
                    if (obj.proPrice != "")
                    {
                        cost = obj.proPrice;
                    }
                    tbShopingcartDB.Cart_AddProduct(ref cartdt, obj.proId.ToString(), "0", obj.proName, cost, idsoluong3.Value, "1", obj.proImage.Split(Convert.ToChar(","))[0]);
                }
                Session["prcart"] = cartdt;

                Response.Write("<script>alert('Thêm vào giỏ hàng thành công!');</script>");
                Response.Write("<script>window.location='" + rqurl + "'</script>");
            }
            else
            {
                Response.Write("<script>alert('Sản phẩm này đã hết bạn vui lòng đặt mua sản phẩm khác!');</script>");
                Response.Write("<script>window.location='" + rqurl + "'</script>");
            }
        }

        void BindTabs()
        {
            string _strTabsName = "";
            string _strTabs = "";
            List<tbProductDATA> list = tbProductDB.tbProduct_GetByTagName(strProID, lang);
            List<tbProTag> lst = db.tbProTags.Where(s => s.proId == int.Parse(list[0].proId)).OrderBy(s => s.Ord).ToList();
            if (lst.Count > 0)
            {
                _strTabsName += "<ul class=\"tabs-pro\">";
                for (int i = 0; i < lst.Count; i++)
                {
                    if (i == 0)
                    {
                        _strTabsName += "<li class=\"active\" rel=\"tab" + i + "\">" + lst[i].TagName + "</li>";
                    }
                    else
                    {
                        _strTabsName += "<li rel=\"tab" + i + "\">" + lst[i].TagName + "</li>";
                    }
                    _strTabs += "<div id=\"tab" + i + "\" class=\"tab_content\">" + lst[i].TagValue + "</div>";

                }
                _strTabsName += "</ul>";
            }
            ltrTabs.Text = _strTabsName + _strTabs;
        }

        void BindDataOther()
        {
            try
            {
                if (GlobalClass.viewProducts4 != "") { strNumberView = GlobalClass.viewProducts4; }
                viewBy = !String.IsNullOrEmpty(Request.QueryString["view"]) ? Request.QueryString["view"].ToString() : viewBy;

                IEnumerable<tbProduct> product = db.tbProducts.Where(s => s.catId == strCatID && s.proLang.Trim() == lang && s.proActive == 1 && s.proTagName != strProID);
                if (sortBy == "name")
                    product = product.OrderBy(s => s.proName).ThenByDescending(s => s.proId).ToList();
                else if (sortBy == "namedesc")
                    product = product.OrderByDescending(s => s.proName).ThenByDescending(s => s.proId).ToList();
                else if (sortBy == "price")
                    product = product.OrderBy(s => s.proPrice).ThenByDescending(s => s.proId).ToList();
                else if (sortBy == "pricedesc")
                    product = product.OrderByDescending(s => s.proPrice).ThenByDescending(s => s.proId).ToList();
                else
                    product = product.OrderByDescending(s => s.proDate).ThenByDescending(s => s.proId).ToList();
                recordCount = product.Count();
                product = product.Skip(0).Take(int.Parse(strNumberView));
                ltrProList.Text = "<ul class=\"product-list\">" + common.LoadProductList(product, viewBy) + "</ul>";
                //ltrPaging.Text = common.PopulatePager(recordCount, currentPage, pageSize);
                //ltrPaging.Visible = GlobalClass.viewProducts5.Contains("1") ? true : false;
            }
            catch { }
        }

        protected void GetListComment(long? id)
        {
            // Load list comment
            var listComment = db.tbComments.Where(s => s.comActive == 1 && s.comNewId == id).OrderByDescending(s => s.comId).ToList();
            int sizeComment = 10;
            if (listComment.Count() > 0)
            {
                if (listComment.Count > sizeComment)
                    ltrPagingComment.Text = getCommentPaging(currentPage, sizeComment, listComment.Count);
                listComment = listComment.Skip((currentPage - 1) * sizeComment).Take(sizeComment).ToList();
                string strComment = "";
                strComment += "<div id='listComment'>";
                for (int c = 0; c < listComment.Count(); c++)
                {
                    strComment += "<div class='commentNewItem'>";
                    strComment += "<span class='itemName'>" + listComment[c].comName + "</span>";
                    strComment += "<span class='itemEmail'>" + listComment[c].comEmail + "</span>";
                    strComment += "<span class='itemContent'>" + listComment[c].comComment + "</span>";
                    strComment += "<span class='itemCreatedDate'>" + listComment[c].comDate + "</span>";
                    strComment += "</div>";
                }
                strComment += "</div>";
                ltrListComment.Text = strComment;
            }
        }

        protected string getCommentPaging(int cur, int size, int total)
        {
            string strResult = "";
            var numberOfPage = total % size == 0 ? total / size : (total / size) + 1;
            strResult += "<ul class='paging'>";
            var curUrl = Request.RawUrl;
            curUrl = curUrl.IndexOf("?") > 0 ? curUrl.Substring(0, curUrl.IndexOf("?")) : curUrl;
            for (int i = 1; i <= numberOfPage; i++)
            {
                strResult += i == cur ?
                    "<li class='active'>" + i + "</li>" :
                    "<li class='item'><a href='" + curUrl + "?page=" + i + "' title='Trang " + i + "'>" + i + "</a></li>";
            }
            strResult += "</ul>";
            return strResult;
        }

        protected void lbtSendComment_Click(object sender, EventArgs e)
        {
            try
            {
                tbComment obj = new tbComment();
                obj.comName = txtCommentName.Text;
                obj.comEmail = txtCommentEmail.Text;
                obj.comComment = txtCommentContent.Text;
                obj.comDate = DateTime.Now;
                obj.comNewId = Convert.ToInt64(hidID.Value.ToString());
                obj.comActive = 0;
                db.tbComments.InsertOnSubmit(obj);
                db.SubmitChanges();
                lbAlter.Visible = true;
                lbAlter.Text = "Gửi bình luận thành công !";
                txtCommentName.Text = "";
                txtCommentEmail.Text = "";
                txtCommentContent.Text = "";
            }
            catch (Exception)
            {
                lbAlter.Visible = true;
                lbAlter.Text = "Có lỗi xảy ra, vui lòng thử lại sau !";
            }
        }

        void BindRelated()
        {
            try
            {
                tbProduct dbPro = db.tbProducts.FirstOrDefault(s => s.proTagName == strProID);
                List<tbProduct> product = new List<tbProduct>();
                string[] mang = dbPro.proSpLienQuan.Split(',');
                if (mang.Length > 0)
                {
                    for (int i = 0; i < mang.Length; i++)
                    {
                        var data = db.tbProducts.First(u => u.proId == Convert.ToInt32(mang[i]));
                        product.Add(data);
                    }
                }
                ltrListpro2.Text = "<ul class=\"product-list\">" + common.LoadProductList(product, viewBy) + "</ul>";
            }
            catch { }
        }
    }
}