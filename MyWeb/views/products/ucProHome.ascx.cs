﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.products
{
    public partial class ucProHome : System.Web.UI.UserControl
    {
        string lang = "vi";
        public string strNumberView = "", strClassName = "";
        int currentPage = 1;
        string viewtype = GlobalClass.viewProducts5;
        string viewBy = GlobalClass.viewProducts9;
        string sortBy = GlobalClass.viewProducts10;
        int recordCount = 0;
        int pageSize = 16;
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            try
            {
                currentPage = Request.QueryString["page"] != null ? int.Parse(Request.QueryString["page"].ToString()) : 1;
            }
            catch { }
            if (!IsPostBack)
            {
                strClassName = Session["home_page"] != null ? "box-pro-home" : "box-pro-sub";
                strNumberView = GlobalClass.viewProducts2;
                if (viewtype == "1")
                {
                    BindData2();
                }
                else
                {
                    BindData();
                }

            }
        }
        private void BindData()
        {
            viewBy = !String.IsNullOrEmpty(Request.QueryString["view"]) ? Request.QueryString["view"].ToString() : viewBy;
            string strCatID = "";

            if (Request["hp"] != null && Request["hp"].ToString() != "")
                strCatID = Request["hp"].ToString().Split('?')[0];

            if (strCatID != "")
            {
                tbPage objSelected = db.tbPages.FirstOrDefault(s => s.pagTagName == strCatID && s.pagType == 100);
                if (objSelected != null)
                {
                    int intLeng = objSelected.paglevel.Length;
                    List<tbPageDATA> objCategory = tbPageDB.tbPage_GetByAll(lang);
                    objCategory = objCategory.Where(s => s.pagType == pageType.GP && s.pagActive == "1" && s.pagLang.Trim() == lang && s.paglevel.StartsWith(objSelected.paglevel)).ToList();
                    string _str = "";
                    if (objCategory.Count() > 0)
                    {
                        foreach (tbPageDATA obj in objCategory)
                        {
                            string catLevel = obj.paglevel;
                            var _product = db.tbProducts.Where(s => s.proActive == 1 && s.catId == obj.pagId && s.proLang.Trim() == lang).OrderBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();

                            if (sortBy == "name")
                                _product = _product.OrderBy(s => s.proName).ThenBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
                            else if (sortBy == "namedesc")
                                _product = _product.OrderByDescending(s => s.proName).ThenBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
                            else if (sortBy == "price")
                                _product = _product.OrderBy(s => double.Parse(s.proPrice)).ThenBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
                            else if (sortBy == "pricedesc")
                                _product = _product.OrderByDescending(s => double.Parse(s.proPrice)).ThenBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
                            else
                                _product = _product.OrderByDescending(s => s.proDate).ThenByDescending(s => s.proId).ToList();

                            recordCount = _product.Count();
                            _product = _product.Skip(0).Take(int.Parse(strNumberView)).ToList();

                            _str += "<div class='box-pro-sub'>";
                            var listAdv = db.tbAdvertise_categories.Where(s => s.CatID == int.Parse(obj.pagId)).ToList();
                            if (listAdv.Count() > 0)
                            {
                                var arrId = listAdv.Select(s => s.advertisID).ToArray();
                                var dsAdv = db.tbAdvertises.Where(s => arrId.Contains(s.advId) && s.advLang == lang && s.advActive == 1).OrderBy(s => s.advOrd).ToList();
                                _str += "<div class='advByGroup' id='advCat'>";
                                for (int i = 0; i < dsAdv.Count(); i++)
                                {
                                    _str += "<div class='advItem'>";
                                    _str += "<a class='advItemLink' href='" + dsAdv[i].advLink + "' title='" + dsAdv[i].advName + "' target='" + dsAdv[i].advTarget + "'><img class='advItemImg' src='" + dsAdv[i].advImage + "' alt='" + dsAdv[i].advName + "' /></a>";
                                    _str += "</div>";
                                }
                                _str += "</div>";
                            }

                            if (_product.Count() > 0)
                            {
                                _str += "<div class=\"cate-header sub-top\">";
                                _str += "<div class=\"txt-name-sub\"><a href=\"/" + MyWeb.Common.StringClass.NameToTag(obj.pagTagName) + ".html\" >" + obj.pagName + "</a> <span>| " + String.Format(MyWeb.Global.GetLangKey("findproduct"), recordCount) + "</span></div>";
                                _str += "</div>";

                                _str += "<ul class=\"product-list\">";
                                _str += common.LoadProductList(_product, viewBy, obj.pagId.ToString());
                                _str += "</ul>";
                                
                            }                            
                            _str += "</div>";
                        }
                    }
                    ltrpro.Text = _str;
                }
            }
            else
            {
                List<tbPageDATA> lstCategory = tbPageDB.tbPage_GetByAll(lang);
                if (Session["home_page"] != null)
                {
                    lstCategory = lstCategory.Where(s => s.pagType == pageType.GP && s.check1 == "1" && s.pagActive == "1" && s.pagLang.Trim() == lang).ToList();
                }
                else
                {
                    lstCategory = lstCategory.Where(s => s.pagType == pageType.GP && s.pagActive == "1" && s.pagLang.Trim() == lang && s.paglevel.Length == 5).ToList();
                }

                string _str = "";
                if (lstCategory.Count() > 0)
                {
                    foreach (tbPageDATA obj in lstCategory)
                    {
                        string catLevel = obj.paglevel;
                        var lstCatChild = db.tbPages.Where(s => s.pagActive == 1 && s.pagLang == lang && s.pagType == int.Parse(pageType.GP) && s.paglevel.StartsWith(obj.paglevel)).Select(s => s.pagId).ToArray();
                        var lstPro = db.tbPages.Where(s => s.paglevel.Length >= obj.paglevel.Length && s.paglevel.Substring(0, obj.paglevel.Length) == obj.paglevel).ToList();
                        var product = db.tbProducts.Where(s => s.proActive == 1 && s.proLang.Trim() == lang && lstCatChild.Contains(Convert.ToInt32(s.catId))).ToList();
                        if (sortBy == "name")
                            product = product.OrderBy(s => s.proName).ThenBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
                        else if (sortBy == "namedesc")
                            product = product.OrderByDescending(s => s.proName).ThenBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
                        else if (sortBy == "price")
                            product = product.OrderBy(s => double.Parse(s.proPrice)).ThenBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
                        else if (sortBy == "pricedesc")
                            product = product.OrderByDescending(s => double.Parse(s.proPrice)).ThenBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
                        else
                            product = product.OrderByDescending(s => s.proDate).ThenByDescending(s => s.proId).ToList();
                        recordCount = product.Count();
                        product = product.Skip(0).Take(int.Parse(strNumberView)).ToList();
                        if (product.Count() > 0)
                        {
                            var listAdv = db.tbAdvertise_categories.Where(s => s.CatID == int.Parse(obj.pagId)).ToList();
                            if (listAdv.Count() > 0)
                            {
                                var arrId = listAdv.Select(s => s.advertisID).ToArray();
                                var dsAdv = db.tbAdvertises.Where(s => arrId.Contains(s.advId) && s.advLang == lang && s.advActive == 1).OrderBy(s => s.advOrd).ToList();
                                _str += "<div class='advByGroup' id='advCat'>";
                                for (int i = 0; i < dsAdv.Count(); i++)
                                {
                                    _str += "<div class='advItem'>";
                                    _str += "<a class='advItemLink' href='" + dsAdv[i].advLink + "' title='" + dsAdv[i].advName + "' target='" + dsAdv[i].advTarget + "'><img class='advItemImg' src='" + dsAdv[i].advImage + "' alt='" + dsAdv[i].advName + "' /></a>";
                                    _str += "</div>";
                                }
                                _str += "</div>";
                            }
                            _str += "<div class=\"cate-header sub-top\">";
                            if (Session["home_page"] != null)
                            {
                                _str += "<div class=\"txt-name-sub\"><a href=\"/" + MyWeb.Common.StringClass.NameToTag(obj.pagTagName) + ".html\" >" + obj.pagName + "</a> <span>| " + String.Format(MyWeb.Global.GetLangKey("findproduct"), recordCount) + "</span></div>";
                            }
                            else
                            {
                                _str += "<div class=\"txt-name-sub2\"><a href=\"/" + MyWeb.Common.StringClass.NameToTag(obj.pagTagName) + ".html\" >" + obj.pagName + "</a></div>";
                            }

                            _str += "</div>";

                            _str += "<ul class=\"product-list\">";
                            _str += common.LoadProductList(product, viewBy, obj.pagId.ToString());
                            _str += "</ul>";                            
                        }
                        
                    }
                }
                ltrpro.Text = _str;
            }
        }
        private void BindData2()
        {

            viewBy = !String.IsNullOrEmpty(Request.QueryString["view"]) ? Request.QueryString["view"].ToString() : viewBy;
            string catId = "";

            if (Request["hp"] != null && Request["hp"].ToString() != "")
                catId = Request["hp"].ToString().Split('?')[0];
            if (catId != "")
            {
                tbPage selectcat = db.tbPages.FirstOrDefault(s => s.pagTagName == catId && s.pagType == 100);
                if (selectcat != null)
                {
                    int leng = selectcat.paglevel.Length;
                    List<tbPageDATA> category = tbPageDB.tbPage_GetByAll(lang);
                    if (Session["home_page"] != null)
                    {
                        category = category.Where(s => s.pagType == pageType.GP && s.check1 == "1" && s.pagActive == "1" && s.pagLang.Trim() == lang && s.paglevel.StartsWith(selectcat.paglevel)).ToList();
                    }
                    else
                    {
                        category = category.Where(s => s.pagType == pageType.GP && s.pagActive == "1" && s.pagLang.Trim() == lang && s.paglevel.Length == 5 && s.paglevel.StartsWith(selectcat.paglevel)).ToList();
                    } 
                    string _str = "";
                    if (category.Count() > 0)
                    {
                        foreach (tbPageDATA cat in category)
                        {
                            string catLevel = cat.paglevel;

                            var lstPro = db.tbPages.Where(s => s.paglevel.Length >= cat.paglevel.Length && s.paglevel.Substring(0, cat.paglevel.Length) == cat.paglevel).ToList();
                            string strChild = "";
                            for (int i = 0; i < lstPro.Count; i++)
                            {
                                if (i != (lstPro.Count - 1)) strChild += lstPro[i].pagId + ",";
                                else strChild += lstPro[i].pagId;
                            }
                            IEnumerable<tbProduct> product = db.tbProducts.Where(s => s.proActive == 1 && s.proLang.Trim() == lang);
                            product = product.Where(s => s.catId == cat.pagId).OrderBy(s => s.proOrd).ThenByDescending(s => s.proId);

                            if (sortBy == "name")
                                product = product.OrderBy(s => s.proName).ThenBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
                            else if (sortBy == "namedesc")
                                product = product.OrderByDescending(s => s.proName).ThenBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
                            else if (sortBy == "price")
                                product = product.OrderBy(s => double.Parse(s.proPrice)).ThenBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
                            else if (sortBy == "pricedesc")
                                product = product.OrderByDescending(s => double.Parse(s.proPrice)).ThenBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
                            else
                                product = product.OrderByDescending(s => s.proDate).ThenByDescending(s => s.proId).ToList();
                            recordCount = product.Count();
                            product = product.Skip(0).Take(int.Parse(strNumberView));

                            var listAdv = db.tbAdvertise_categories.Where(s => s.CatID == int.Parse(cat.pagId)).ToList();
                            if (listAdv.Count() > 0)
                            {
                                var arrId = listAdv.Select(s => s.advertisID).ToArray();
                                var dsAdv = db.tbAdvertises.Where(s => arrId.Contains(s.advId) && s.advLang == lang && s.advActive == 1).OrderBy(s => s.advOrd).ToList();
                                dsAdv = dsAdv.OrderByDescending(x => x.advId).ToList();
                                _str += "<div class='advByGroup' id='advCat'>";
                                //for (int i = 0; i < dsAdv.Count(); i++)
                                //{
                                //Lay anh mới nhất của nhóm sản phẩm được cập nhật
                                    _str += "<div class='advItem'>";
                                    _str += "<a class='advItemLink' href='/san-pham.html' title='" + dsAdv[0].advName + "' target='" + dsAdv[0].advTarget + "'><img class='advItemImg' src='" + dsAdv[0].advImage + "' alt='" + dsAdv[0].advName + "' /></a>";
                                    _str += "</div>";
                                //}
                                _str += "</div>";
                            } 
                            if (product.Count() > 0)
                            {
                                _str += "<div class=\"cate-header sub-top\">";
                                _str += "<div class=\"txt-name-sub\"><a href=\"/" + MyWeb.Common.StringClass.NameToTag(cat.pagTagName) + ".html\" >" + cat.pagName + "</a> <span>| " + String.Format(MyWeb.Global.GetLangKey("findproduct"), recordCount) + "</span></div>";
                                _str += "</div>";

                                _str += "<ul class=\"product-list\">";
                                _str += common.LoadProductList(product, viewBy, cat.pagId.ToString());
                                _str += "</ul>";                                                             
                            }                            
                        }
                    }
                    ltrpro.Text = _str;
                }
            }
            else
            {
                List<tbPageDATA> lstCategory = tbPageDB.tbPage_GetByAll(lang);
                if (Session["home_page"] != null)
                {
                    lstCategory = lstCategory.Where(s => s.pagType == pageType.GP && s.check1 == "1" && s.pagActive == "1" && s.pagLang.Trim() == lang).ToList();
                }
                else
                {
                    lstCategory = lstCategory.Where(s => s.pagType == pageType.GP && s.pagActive == "1" && s.pagLang.Trim() == lang && s.paglevel.Length == 5).ToList();
                }
                string _str = "";
                if (lstCategory.Count() > 0)
                {
                    foreach (tbPageDATA cat in lstCategory)
                    {
                        IEnumerable<tbProduct> product = db.tbProducts.Where(s => s.proActive == 1 && s.proLang.Trim() == lang);
                        product = product.Where(s => s.catId == cat.pagId).OrderBy(s => s.proOrd).ThenByDescending(s => s.proId);

                        if (sortBy == "name")
                            product = product.OrderBy(s => s.proName).ThenBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
                        else if (sortBy == "namedesc")
                            product = product.OrderByDescending(s => s.proName).ThenBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
                        else if (sortBy == "price")
                            product = product.OrderBy(s => double.Parse(s.proPrice)).ThenBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
                        else if (sortBy == "pricedesc")
                            product = product.OrderByDescending(s => double.Parse(s.proPrice)).ThenBy(s => s.proOrd).ThenByDescending(s => s.proId).ToList();
                        else
                            product = product.OrderByDescending(s => s.proDate).ThenByDescending(s => s.proId).ToList();
                        recordCount = product.Count();
                        product = product.Skip(0).Take(int.Parse(strNumberView));

                        var listAdv = db.tbAdvertise_categories.Where(s => s.CatID == int.Parse(cat.pagId)).ToList();
                        if (listAdv.Count() > 0)
                        {
                            var arrId = listAdv.Select(s => s.advertisID).ToArray();
                            var dsAdv = db.tbAdvertises.Where(s => arrId.Contains(s.advId) && s.advLang == lang && s.advActive == 1).OrderBy(s => s.advOrd).ToList();
                            _str += "<div class='advByGroup' id='advCat'>";
                            for (int i = 0; i < dsAdv.Count(); i++)
                            {
                                _str += "<div class='advItem'>";
                                _str += "<a class='advItemLink' href='" + dsAdv[i].advLink + "' title='" + dsAdv[i].advName + "' target='" + dsAdv[i].advTarget + "'><img class='advItemImg' src='" + dsAdv[i].advImage + "' alt='" + dsAdv[i].advName + "' /></a>";
                                _str += "</div>";
                            }
                            _str += "</div>";
                        } 
                        if (product.Count() > 0)
                        {
                            _str += "<div class=\"cate-header sub-top\">";
                            _str += "<div class=\"txt-name-sub\"><a href=\"/" + MyWeb.Common.StringClass.NameToTag(cat.pagTagName) + ".html\" >" + cat.pagName + "</a>";
                            var catChild = db.tbPages.Where(s => s.pagType == int.Parse(pageType.GP) && s.pagActive == 1 && s.pagLang.Trim() == lang && s.paglevel.StartsWith(cat.paglevel) && s.paglevel.Length == (cat.paglevel.Length * 2)).ToList();
                            if (catChild != null && catChild.Count > 0)
                            {
                                int i = 0;
                                foreach (var item in catChild)
                                {
                                    if (i == 0)
                                        _str += "<span> <a href=\"/" + MyWeb.Common.StringClass.NameToTag(item.pagTagName) + ".html\">" + item.pagName + "</a></span>";
                                    else
                                        _str += "<span> | <a href=\"/" + MyWeb.Common.StringClass.NameToTag(item.pagTagName) + ".html\">" + item.pagName + "</a></span>";
                                    i++;
                                }
                            }
                            _str += "</div>";
                            _str += "</div>";

                            _str += "<ul class=\"product-list\">";
                            _str += common.LoadProductList(product, viewBy, cat.pagId.ToString());
                            _str += "</ul>";                                                    
                        }
                        
                    }
                }
                ltrpro.Text = _str;
            }
        }
    }
}