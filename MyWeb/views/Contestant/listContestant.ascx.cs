﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.Contestant
{
    public partial class listContestant : System.Web.UI.UserControl  
    {
        public string strUrlFace = "";
        public string wf = "770";
        public string nf = "5";
        public string cf = "light";
        int currentPage;
        dataAccessDataContext db = new dataAccessDataContext();
        protected string lang = "vi";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (GlobalClass.widthfkComment != null) { wf = GlobalClass.widthfkComment; }
            if (GlobalClass.numfkComment != null) { nf = GlobalClass.numfkComment; }
            if (GlobalClass.colorfkComment != null) { cf = GlobalClass.colorfkComment; }
            lang = MyWeb.Global.GetLang();
            currentPage = Request.QueryString["page"] != null ? int.Parse(Request.QueryString["page"]) : 1;
            if (!IsPostBack)
            {
                //LoadData();
            }
        }

    }
}