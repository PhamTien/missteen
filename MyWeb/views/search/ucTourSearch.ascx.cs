﻿using MyWeb.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.search
{
    public partial class ucTourSearch : System.Web.UI.UserControl
    {
        string lang = "vi";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                LoadData();
            }
        }
        protected void LoadData()
        {
            string placeFrom = !String.IsNullOrEmpty(Request.QueryString["from"]) ? Request.QueryString["from"].ToString() : "";
            string placeTo = !String.IsNullOrEmpty(Request.QueryString["to"]) ? Request.QueryString["to"].ToString() : "";
            string time = !String.IsNullOrEmpty(Request.QueryString["time"]) ? Request.QueryString["time"].ToString() : "";
            string inout = !String.IsNullOrEmpty(Request.QueryString["inout"]) ? Request.QueryString["inout"].ToString() : "";
            int plFrom = 0;
            int plTo = 0;
            int inOut = 0;
            List<int?> lstPlaceIn = new List<int?>();
            List<int> lstPlaceOut = new List<int>();
            List<int> lstCat = new List<int>();
            if (!string.IsNullOrEmpty(placeFrom))
                plFrom = Convert.ToInt32(placeFrom);
            if (!string.IsNullOrEmpty(placeTo))
                plTo = Convert.ToInt32(placeTo);
            if (!string.IsNullOrEmpty(inout))
                inOut = Convert.ToInt32(inout);

            if (inOut != 0)
                lstCat.Add(inOut);
            if (plFrom != 0)
                lstPlaceIn.Add(plFrom);
            if (plTo != 0)
                lstPlaceOut.Add(plTo);

            time = time.Trim();

            var listCat = new List<tbPage>();
            var listCatReal = new List<tbPage>();
            var listPlace = new List<tbPage>();
            var lstTourIdDb = new List<int>();
            int currentPage = Request.RawUrl.Contains("page") ? int.Parse(common.GetParameterFromUrl(Request.RawUrl, "page")) : 1;
            string strResult = "";
            int pageSize = 10;

            listCat =
                    db.tbPages.Where(
                        s => s.pagActive == 1 && s.pagLang == lang && s.pagType == int.Parse(pageType.TourPlace) && lstCat.Contains(s.pagId))
                        .ToList();
            if (listCat != null && listCat.Count > 0)
            {
                foreach (var iCat in listCat)
                {
                        var lstCatReal = db.tbPages.Where(
                                s =>
                                s.pagLang == lang && s.pagActive == 1 
                                && s.pagType == int.Parse(pageType.TourPlace) 
                                && s.paglevel.StartsWith(iCat.paglevel)).ToList();
                        listCatReal.AddRange(lstCatReal);
                }
            }

            listPlace =
                    db.tbPages.Where(
                        s => s.pagActive == 1 && s.pagLang == lang && s.pagType == int.Parse(pageType.TourPlace))
                        .ToList();
            if (lstPlaceOut != null && lstPlaceOut.Count > 0)
                listPlace = listPlace.Where(x => lstPlaceOut.Contains(x.pagId)).ToList();
            var arrPlaceId = listPlace.Select(x => x.pagId).ToList();

            List<int?> lstPlaceOutIdDb = new List<int?>();
            if (arrPlaceId != null && arrPlaceId.Count > 0)
            {
                foreach (var item in arrPlaceId)
                {
                    lstPlaceOutIdDb.Add(item);
                }
            }
            
            List<int?> lstTourId= new List<int?>();
            if (lstPlaceOutIdDb != null && lstPlaceOutIdDb.Count > 0)
                lstTourId = db.TourPlaces.Where(x => lstPlaceOutIdDb.Contains(x.IdPlace)).Select(x => x.IdTour).ToList();

            if (lstTourId != null && lstTourId.Count > 0)
            {
                foreach (var item in lstTourId)
                {
                    lstTourIdDb.Add(Convert.ToInt32(item));
                }
            }

            var arrId = listCatReal.Select(s => s.pagId).ToArray();
            
            var listTour =
                db.Tours.Where(s => s.Lang == lang && s.Active == 1 && arrId.Contains(s.IdPlace.Value)).ToList();

            if (lstPlaceIn != null && lstPlaceIn.Count > 0)
                listTour = listTour.Where(x => lstPlaceIn.Contains(x.IdPlace)).ToList();

            if (lstTourIdDb != null && lstTourIdDb.Count > 0)
                listTour = listTour.Where(x => lstTourIdDb.Contains(x.Id)).ToList();

            if (!string.IsNullOrEmpty(time) && !time.Equals("0"))
                listTour = listTour.Where(x => x.NumberOfDay.Contains(time)).ToList();

            if (listTour.Any())
            {
                int count = listTour.Count;

                listTour = listTour.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

                foreach (var item in listTour)
                {
                    strResult += "<div class='tourItems'>";
                    strResult += "  <div class='tourItems_info1'>";
                    strResult += "      <a class='tourItems_img' href='/" + item.Tagname + ".html' title='" + item.Name +
                                 "'><img src='" + item.Image1 + "' alt='" + item.Name + "' /></a>";
                    strResult += "      <a class='tourItems_link' href='/" + item.Tagname + ".html' title='" + item.Name + "'>" + item.Name + "</a>";
                    strResult += "      <span class='tourItems_priceOrifin'><strong>Giá: </strong>" + String.Format("{0:0,0}", item.PriceOrigin) +
                                 " " + item.PriceUnit + "</span>";
                    strResult += "      <span class='tourItems_price'><strong>Khuyến mại: </strong>" + String.Format("{0:0,0}", item.Price) + " " +
                                 item.PriceUnit + "</span>";
                    strResult += "      <span class='tourItems_topic'><strong>Chủ đề: </strong>" + GetName(item.IdTourTopic.Value) + "</span>";
                    strResult += "      <span class='tourItems_cat'><strong>Nhóm tour: </strong>" + GetName(item.IdTourCategory.Value) + "</span>";
                    strResult += "      <span class='tourItems_des'>" + MyWeb.Common.StringClass.GetContent(item.Description, 150) + "</span>";
                    strResult += "  </div>";
                    strResult += "  <div class='tourItems_info2'>";
                    strResult += "      <div class='tourItems_placeStart'><span>Điểm đi: </span>" + GetName(item.IdPlace.Value) + "</div>";
                    strResult += "      <div class='tourItems_placeEnd'><span>Điểm đến: </span>" + GetListPlaceName(item.Id) + "</div>";
                    strResult += "  </div>";
                    strResult += "  <div class='tourItems_info3'>";
                    strResult += "      <span class='tourItems_start'><strong>Thời gian khởi hành: </strong>" + item.Start +
                                 "</span>";
                    strResult += "      <span class='tourItems_time'><strong>Số ngày: </strong>" + item.NumberOfDay + "</span>";
                    strResult += "      <span class='tourItems_tran'><strong>Phương tiện: </strong>" + item.Transportation +
                                 "</span>";
                    strResult += "  </div>";
                    strResult += "</div>";
                }

                ltrContent.Text = strResult;
                ltrPaging.Text = common.PopulatePager(count, currentPage, pageSize);
            }
        }

        protected string GetName(int plId)
        {
            var item = db.tbPages.FirstOrDefault(s => s.pagId == plId);

            return item != null ? item.pagName : "";
        }

        protected string GetListPlaceName(int tourId)
        {
            var listPl = db.TourPlaces.Where(s => s.IdTour == tourId).Select(s => s.IdPlace).ToList();
            var listPage = db.tbPages.Where(s => listPl.Contains(s.pagId)).ToList();
            string strResult = "";

            if (listPage.Any())
            {

                foreach (var item in listPage)
                {
                    strResult += strResult != "" ? "</br><span class='itemTours_placeItem'>" + item.pagName + "</span>" : "<span class='itemTours_placeItem'>" + item.pagName + "</span>";
                }
            }

            return strResult;
        }
    }
}