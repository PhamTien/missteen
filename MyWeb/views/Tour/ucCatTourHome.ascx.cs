﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyWeb.control.panel.system;

namespace MyWeb.views.Tour
{
    public partial class ucCatTourHome : System.Web.UI.UserControl
    {
        private dataAccessDataContext db = new dataAccessDataContext();
        protected string lang = "vi";
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                LoadList();
            }
        }

        protected void LoadList()
        {
            string strResult = "";
            var listCat =
                    db.tbPages.Where(
                        s => s.pagActive == 1 && s.pagLang == lang && s.pagType == int.Parse(pageType.TourCategory))
                        .ToList().Where(x => x.check1 == "1");
            if (listCat.Any())
            {
                foreach (var item in listCat)
                {
                    strResult += "<div class=\"col-md-6\"><div class=\"clearfix listTour_all\"><div class=\"listTour_label\"><span>" + item.pagName + "</span></div><div class=\"listTour_list\">";
                    int takePage = (item.homeQtity != null) ? int.Parse(item.homeQtity.ToString()) : 2;
                    var listTour =
                    db.Tours.Where(s => s.Active == 1 && s.Lang == lang && s.IdTourCategory == item.pagId).OrderByDescending(s => s.Id).Take(takePage).ToList();
                    if (listTour.Any())
                    {
                        strResult += GetListTourString(listTour);
                    }
                    strResult += "</div></div></div>";
                }
            }

            ltrListItem.Text = strResult;
        }

        protected string GetListTourString(List<MyWeb.Tour> listTour)
        {
            string strResult = "";
            foreach (var item in listTour)
            {
                strResult += "<div class='tourItems'>";
                strResult += "  <div class='tourItems_info1'>";
                strResult += "      <a class='tourItems_img' href='/" + item.Tagname + ".html' title='" + item.Name +
                             "'><img src='" + item.Image1 + "' alt='" + item.Name + "' /></a>";
                strResult += "      <a class='tourItems_link' href='/" + item.Tagname + ".html' title='" + item.Name + "'>" + item.Name + "</a>";
                strResult += "      <span class='tourItems_priceOrifin'><strong>Giá: </strong>" + String.Format("{0:0,0}", item.PriceOrigin) +
                                 " " + item.PriceUnit + "</span>";
                strResult += "      <span class='tourItems_price'><strong>Khuyến mại: </strong>" + String.Format("{0:0,0}", item.Price) + " " +
                             item.PriceUnit + "</span>";
                strResult += "      <span class='tourItems_topic'><strong>Chủ đề: </strong>" + GetName(item.IdTourTopic.Value) + "</span>";
                strResult += "      <span class='tourItems_cat'><strong>Nhóm tour: </strong>" + GetName(item.IdTourCategory.Value) + "</span>";
                strResult += "      <span class='tourItems_des'>" + MyWeb.Common.StringClass.GetContent(item.Description, 150) + "</span>";
                strResult += "  </div>";
                strResult += "  <div class='tourItems_info2'>";
                strResult += "      <div class='tourItems_placeStart'><span>Điểm đi: </span>" + GetName(item.IdPlace.Value) + "</div>";
                strResult += "      <div class='tourItems_placeEnd'><span>Điểm đến: </span>" + GetListPlaceName(item.Id) + "</div>";
                strResult += "  </div>";
                strResult += "  <div class='tourItems_info3'>";
                strResult += "      <span class='tourItems_start'><strong>Thời gian khởi hành: </strong>" + item.Start +
                             "</span>";
                strResult += "      <span class='tourItems_time'><strong>Số ngày: </strong>" + item.NumberOfDay + "</span>";
                strResult += "      <span class='tourItems_tran'><strong>Phương tiện: </strong>" + item.Transportation +
                             "</span>";
                strResult += "  </div>";
                strResult += "</div>";
            }
            return strResult;
        }

        protected string GetName(int plId)
        {
            var item = db.tbPages.FirstOrDefault(s => s.pagId == plId);

            return item != null ? item.pagName : "";
        }

        protected string GetListPlaceName(int tourId)
        {
            var listPl = db.TourPlaces.Where(s => s.IdTour == tourId).Select(s => s.IdPlace).ToList();
            var listPage = db.tbPages.Where(s => listPl.Contains(s.pagId)).ToList();
            string strResult = "";

            if (listPage.Any())
            {

                foreach (var item in listPage)
                {
                    strResult += strResult != "" ? "</br><span class='itemTours_placeItem'>" + item.pagName + "</span>" : "<span class='itemTours_placeItem'>" + item.pagName + "</span>";
                }
            }

            return strResult;
        }
    }
}