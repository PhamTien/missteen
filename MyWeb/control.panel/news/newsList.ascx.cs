﻿using MyWeb.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.news
{
    public partial class newsList : System.Web.UI.UserControl
    {
        string _Admin = "";
        private string lang = "vi";
        int pageSize = 15;
        string userid = "";
        string _strID = "";
        private string arrGrnId = "";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            if (Session["admin"] != null) { _Admin = Session["admin"].ToString(); } if (Session["admin"] != null) { _Admin = Session["admin"].ToString(); }
            if (Session["uid"] != null) userid = Session["uid"].ToString();
            if (Request["pages"] != null && !Request["pages"].Equals(""))
            {
                pageSize = int.Parse(Request["pages"].ToString());
            }

            if (_Admin == "100" || _Admin == "1") { }
            else
            {
                tbUser tbu = db.tbUsers.Where(s => s.useId == int.Parse(userid)).FirstOrDefault();
                string arrAuthor = tbu.useAuthorities.ToString();
                string Author = arrAuthor.Replace("news,3,31,32,", "");
                arrGrnId = Author.Length > 0 ? Author.Substring(0, Author.Length - 1) : "0";
            }
            _strID = PageHelper.GetQueryString("id");

            if (!IsPostBack)
            {
                ViewEvent();
                BindForder();
                BindData();
                getTags();
                if (_strID != "")
                {
                    BindDataNews(_strID);
                }
            }

        }
        void BindDataNews(string strID)
        {
            BindForder();
            tbNew dbNew = db.tbNews.FirstOrDefault(s => s.newid == Int32.Parse(strID));
            hidID.Value = strID;
            txtalt.Text = dbNew.newAlt;
            try
            {
                ddlForder.SelectedValue = dbNew.grnID.ToString();
            }
            catch { }
            txtTieudetin.Text = dbNew.newName;
            txtTomtat.Text = dbNew.newContent;
            FckChitiet.Text = dbNew.newDetail;
            txtImage.Text = dbNew.newImage;

            txtFile.Text = dbNew.newFile;
            txtTieudetitle.Text = dbNew.newTitle;
            txtDesscription.Text = dbNew.newDescription;
            txtKeyword.Text = dbNew.newKeyword;

            chkUutien.Checked = dbNew.newPriority == 1 ? true : false;
            chkIndex.Checked = dbNew.newIndex == 1 ? true : false;
            chkActive.Checked = dbNew.newActive == 0 ? true : false;
            txtNewsDate.Text = string.Format("{0:dd/MM/yyyy HH:mm:ss}", dbNew.newDate);
            if (dbNew.newTag != null)
            {
                int i = 0;
                string strMaTin = null;
                strMaTin = dbNew.newTag;
                string[] MaTin = null;
                MaTin = strMaTin.Split(new Char[] { ';' });
                string k = null;
                lstB.Items.Clear();
                foreach (string k_loopVariable in MaTin)
                {
                    k = k_loopVariable;
                    if (!string.IsNullOrEmpty(k.Trim()))
                    {
                        lstB.Items.Add(new ListItem(k, k));
                    }
                }
                lstNewRelate.Items.Clear();
                for (i = 0; i < lstB.Items.Count; i++)
                {

                    IEnumerable<tbTag> list2 = db.tbTags.Where(s => s.Lang == lang && s.Active == 1 && s.Type == 2);
                    foreach (tbTag n in list2)
                    {
                        if (lstB.Items[i].Text == n.Name.ToString())
                        {
                            lstNewRelate.Items.Add(new ListItem(n.Name.ToString(), n.Id.ToString()));
                        }
                    }
                }
            }
            pnlErr.Visible = false;
            ltrErr.Text = "";

            pnlListForder.Visible = false;
            pnlAddForder.Visible = true;
        }

        void BindData()
        {
            if (_Admin == "100" || _Admin == "1")
            {
                List<tbNewsDATA> _listNews = tbNewsDB.tbNews_GetByAll(lang);

                if (txtSearch.Text != "")
                {
                    _listNews = _listNews.Where(s => s.newName.Contains(txtSearch.Text)).ToList();
                }

                if (ddlType.SelectedValue != "0")
                {
                    if (ddlType.SelectedValue == "1")
                    {
                        _listNews = _listNews.Where(s => s.newType == "1").ToList();
                    }
                    else if (ddlType.SelectedValue == "2")
                    {
                        _listNews = _listNews.Where(s => s.newimg == "1").ToList();
                    }
                    else if (ddlType.SelectedValue == "3")
                    {
                        _listNews = _listNews.Where(s => s.newPriority == "1").ToList();
                    }
                    else if (ddlType.SelectedValue == "4")
                    {
                        _listNews = _listNews.Where(s => s.newIndex == "1").ToList();
                    }
                }

                if (drlForder.SelectedValue != "0")
                {
                    var obj = db.tbPages.Where(u => u.pagId == Convert.ToInt32(drlForder.SelectedValue)).FirstOrDefault();
                    var arr = db.tbPages.Where(s => s.paglevel.StartsWith(obj.paglevel)).Select(s => s.pagId.ToString()).ToArray();
                    _listNews = _listNews.Where(s => arr.Contains(s.grnID)).ToList();
                }


                if (_listNews.Count() > 0)
                {
                    recordCount = _listNews.Count();
                    #region Statistic
                    int fResult = currentPage * pageSize + 1;
                    int tResult = (currentPage + 1) * pageSize;
                    tResult = tResult > recordCount ? recordCount : tResult;
                    ltrStatistic.Text = "Hiển thị kết quả từ " + fResult + " - " + tResult + " trong tổng số " + recordCount + " kết quả";
                    #endregion

                    var result = _listNews.Skip(currentPage * pageSize).Take(pageSize);
                    rptListNews.DataSource = result;
                    rptListNews.DataBind();
                    BindPaging();
                }
            }
            else
            {
                List<tbNew> _listNews = db.tbNews.Where(s => s.userId == arrGrnId && s.newLang == lang).OrderByDescending(u => u.newDate).ToList();
                if (txtSearch.Text != "")
                {
                    _listNews = _listNews.Where(s => s.newName.Contains(txtSearch.Text)).ToList();
                }
                if (drlForder.SelectedValue != "0")
                {
                    var obj = db.tbPages.Where(u => u.pagId == Convert.ToInt32(drlForder.SelectedValue)).FirstOrDefault();
                    var arr = db.tbPages.Where(s => s.paglevel.StartsWith(obj.paglevel)).Select(s => s.pagId).ToArray();
                    _listNews = _listNews.Where(s => arr.Contains(s.grnID.Value)).ToList();
                }

                if (_listNews.Count() > 0)
                {
                    recordCount = _listNews.Count();
                    #region Statistic
                    int fResult = currentPage * pageSize + 1;
                    int tResult = (currentPage + 1) * pageSize;
                    tResult = tResult > recordCount ? recordCount : tResult;
                    ltrStatistic.Text = "Hiển thị kết quả từ " + fResult + " - " + tResult + " trong tổng số " + recordCount + " kết quả";
                    #endregion

                    var result = _listNews.Skip(currentPage * pageSize).Take(pageSize);
                    rptListNews.DataSource = result;
                    rptListNews.DataBind();
                    BindPaging();
                }
            }
        }

        void BindForder()
        {
            string strForder = "";
            if (_Admin == "0" || _Admin == "100" || _Admin == "1")
            {
                List<tbPageDATA> list = tbPageDB.tbPage_GetByAll(lang);
                list = list.Where(s => s.pagType == pageType.GN).ToList();
                drlForder.Items.Clear();
                drlForder.Items.Add(new ListItem("- Nhóm tin -", "0"));

                ddlForder.Items.Clear();
                ddlForder.Items.Add(new ListItem("- Nhóm cha -", ""));
                for (int i = 0; i < list.Count; i++)
                {
                    strForder = "";
                    for (int j = 1; j < list[i].paglevel.Length / 5; j++)
                    {
                        strForder = strForder + "---";
                    }
                    drlForder.Items.Add(new ListItem(strForder.ToString() + list[i].pagName, list[i].pagId));
                    ddlForder.Items.Add(new ListItem(strForder.ToString() + list[i].pagName, list[i].pagId));
                }
                list.Clear();
                list = null;

            }
            else
            {
                List<tbPageDATA> list = tbPageDB.tbPage_GetByArrCatID("");
                drlForder.Items.Clear();
                drlForder.Items.Add(new ListItem("- Nhóm tin -", "0"));
                for (int i = 0; i < list.Count; i++)
                {
                    strForder = "";
                    for (int j = 1; j < list[i].paglevel.Length / 5; j++)
                    {
                        strForder = strForder + "---";
                    }
                    drlForder.Items.Add(new ListItem(strForder.ToString() + list[i].pagName, list[i].pagId));
                    ddlForder.Items.Add(new ListItem(strForder.ToString() + list[i].pagName, list[i].pagId));
                }
                list.Clear();
                list = null;
            }

        }

        #region Page

        private int currentPage { get { return ViewState["currentPage"] != null ? int.Parse(ViewState["currentPage"].ToString()) : 0; } set { ViewState["currentPage"] = value; } }
        private int recordCount { get { return ViewState["recordCount"] != null ? int.Parse(ViewState["recordCount"].ToString()) : 0; } set { ViewState["recordCount"] = value; } }
        private int pageCount { get { double iCount = (double)((decimal)recordCount / (decimal)pageSize); return (int)Math.Ceiling(iCount); } }

        private void BindPaging()
        {
            int icurPage = currentPage + 1;
            int ipCount = pageCount;
            if (ipCount >= 1)
            {
                rptNumberPage.Visible = true;
                int PageShow = ipCount > 5 ? 5 : ipCount;
                int FromPage;
                int ToPage;
                DataTable dt = new DataTable();
                dt.Columns.Add("PageIndex");
                dt.Columns.Add("PageText");
                FromPage = icurPage > PageShow ? icurPage - PageShow : 1;
                ToPage = (ipCount - icurPage > PageShow) ? icurPage + PageShow : ipCount;
                if (icurPage - 10 > 0) dt.Rows.Add(icurPage - 9, icurPage - 10);
                for (int i = FromPage; i <= ToPage; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = i - 1;
                    dr[1] = i;
                    dt.Rows.Add(dr);
                }
                if (icurPage + 10 <= ipCount) dt.Rows.Add(icurPage + 9, icurPage + 10);
                rptNumberPage.DataSource = dt;
                rptNumberPage.DataBind();
            }
        }

        protected void btnPage_Click(object sender, EventArgs e)
        {
            if (((LinkButton)sender).ID == "btnPrevious")
            {
                if (currentPage > 0) currentPage = currentPage - 1;
            }
            else if (((LinkButton)sender).ID == "btnNext")
            {
                if (currentPage < pageCount - 1) currentPage = currentPage + 1;
            }
            BindData();
        }

        protected void rptNumberPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("page"))
            {
                currentPage = Convert.ToInt32(e.CommandArgument.ToString());
                BindData();
            }
        }

        protected void rptNumberPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lnkPage = (LinkButton)e.Item.FindControl("btn");
            Literal _ltrPage = (Literal)e.Item.FindControl("ltrLiPage");
            if (lnkPage.CommandArgument.ToString() == currentPage.ToString())
            {
                lnkPage.Enabled = false;
                _ltrPage.Text = "<li class=\"paginate_button active\">";
            }
            else
            {
                _ltrPage.Text = "<li class=\"paginate_button\">";
            }
        }

        #endregion

        void ViewEvent()
        {
            List<tbSukienDATA> list = tbSukienDB.tbSukien_GetByAll();
            drlSukien.Items.Clear();
            drlSukien.Items.Add(new ListItem(" - Chọn sự kiện - ", "0"));
            for (int i = 0; i < list.Count; i++)
            {
                drlSukien.Items.Add(new ListItem(list[i].skName, list[i].ID));
            }
            list.Clear();
            list = null;
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = false;
            pnlAddForder.Visible = true;
            Resetcontrol();
        }

        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
            RepeaterItem item = default(RepeaterItem);
            for (int i = 0; i < rptListNews.Items.Count; i++)
            {
                item = rptListNews.Items[i];
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    if (((CheckBox)item.FindControl("chkBox")).Checked)
                    {
                        HiddenField hidID = (HiddenField)item.FindControl("hidCatID");
                        string tagName = db.tbNews.Where(s => s.newid == int.Parse(hidID.Value)).FirstOrDefault().newTagName;
                        string pagId = db.tbPages.Where(s => s.pagTagName == tagName).FirstOrDefault().pagId.ToString();
                        tbPageDB.tbPage_Delete(pagId);

                        var data = db.tbComments.Where(u => u.comNewId == Convert.ToInt32(hidID.Value)).ToList();
                        if (data.Count > 0)
                        {
                            for (int j = 0; j < data.Count; j++)
                            {
                                db.tbComments.DeleteOnSubmit(data[j]);
                                db.SubmitChanges();
                            }
                        }

                        tbNewsDB.tbNews_Delete(hidID.Value);
                    }
                }
            }

            pnlErr.Visible = true;
            ltrErr.Text = "Xóa thành công bài viết !";
            BindData();
            chkSelectAll.Checked = false;
        }

        protected void drlForder_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }

        #region[Resetcontrol]
        void Resetcontrol()
        {
            txtTieudetin.Text = "";
            txtTomtat.Text = "";
            FckChitiet.Text = "";
            txtImage.Text = "";
            txtFile.Text = "";
            txtTieudetitle.Text = "";
            txtDesscription.Text = "";
            txtKeyword.Text = "";
            chkUutien.Checked = false;
            hidID.Value = "";
            txtalt.Text = "";
            lstNewRelate.Items.Clear();
            txtNewsDate.Text = string.Format("{0:MM/dd/yyyy}", DateTime.Now);
        }
        #endregion

        bool Validation()
        {
            if (ddlForder.SelectedValue == "0") { ltrErr2.Text = "Chưa chọn thuộc nhóm chuyên mục !!"; pnlErr2.Visible = true; return false; }
            if (txtTieudetin.Text == "") { ltrErr2.Text = "Chưa nhập tiêu đề tin !!"; pnlErr2.Visible = true; return false; }
            if (FckChitiet.Text == "") { ltrErr2.Text = "Chưa nhập nội dung tin !!"; pnlErr2.Visible = true; return false; }
            return true;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (Validation() == true)
            {
                string strID = hidID.Value;

                DateTime datetime = DateTime.Now;
                if (txtNewsDate.Text != "")
                {
                    //datetime = DateTime.ParseExact(txtNewsDate.Text, "MM/dd/yyyy", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat);
                    var arrDate = txtNewsDate.Text.Split(' ');
                    string strNewTime = arrDate[1];
                    if (arrDate[2] == "PM")
                    {
                        var hour = int.Parse(arrDate[1].Split(':')[0].ToString());
                        hour = hour + 12;
                        strNewTime = hour + ":" + arrDate[1].Split(':')[1];
                    }
                    string strNewDate = arrDate[0] + " " + strNewTime + ":00";
                    datetime = DateTime.ParseExact(strNewDate, "MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat);
                }
                if (strID.Length == 0)
                {
                    tbNewsDATA ObjtbNewsDATA = new tbNewsDATA();
                    ObjtbNewsDATA.newName = txtTieudetin.Text;
                    ObjtbNewsDATA.newImage = txtImage.Text;
                    ObjtbNewsDATA.newFile = txtFile.Text;
                    ObjtbNewsDATA.newContent = txtTomtat.Text;
                    ObjtbNewsDATA.newDetail = FckChitiet.Text;
                    ObjtbNewsDATA.newDate = string.Format("{0:MM/dd/yyyy HH:mm:ss}", datetime);
                    ObjtbNewsDATA.newTitle = txtTieudetitle.Text;
                    ObjtbNewsDATA.newDescription = txtDesscription.Text;
                    ObjtbNewsDATA.newKeyword = txtKeyword.Text;
                    ObjtbNewsDATA.newPriority = chkUutien.Checked ? "1" : "0";
                    ObjtbNewsDATA.newIndex = chkIndex.Checked ? "1" : "0";
                    ObjtbNewsDATA.newActive = chkActive.Checked ? "0" : "1";
                    ObjtbNewsDATA.newimg = chkSukien.Checked ? "1" : "0";
                    ObjtbNewsDATA.skId = drlSukien.SelectedValue;
                    ObjtbNewsDATA.grnID = ddlForder.SelectedValue;
                    ObjtbNewsDATA.newType = chkChuyenDe.Checked ? "1" : "0";
                    ObjtbNewsDATA.usrID = userid;
                    ObjtbNewsDATA.newLang = lang;
                    #region Them vao tbPage + tbNews

                    tbPageDATA objNewMenu = new tbPageDATA();
                    objNewMenu.pagName = txtTieudetin.Text;
                    objNewMenu.pagImage = "";
                    objNewMenu.pagType = pageType.ND;
                    var curNewType = db.tbPages.Where(s => s.pagId == int.Parse(ddlForder.SelectedValue)).FirstOrDefault();
                    objNewMenu.paglevel = curNewType.paglevel + "00000";
                    objNewMenu.pagTitle = txtTieudetitle.Text;
                    objNewMenu.pagDescription = txtDesscription.Text;
                    objNewMenu.pagKeyword = txtKeyword.Text;
                    objNewMenu.pagOrd = "0";
                    objNewMenu.pagActive = chkActive.Checked ? "0" : "1";
                    objNewMenu.pagLang = lang;
                    objNewMenu.pagDetail = "";
                    objNewMenu.pagLink = "";
                    objNewMenu.pagTarget = "";
                    objNewMenu.pagPosition = "0";
                    objNewMenu.catId = "";
                    objNewMenu.grnId = "";
                    objNewMenu.pagTagName = "";
                    objNewMenu.check1 = "";
                    objNewMenu.check2 = "";
                    objNewMenu.check3 = "";
                    objNewMenu.check4 = "";
                    objNewMenu.check5 = "";
                    string tagName = MyWeb.Common.StringClass.NameToTag(txtTieudetin.Text);

                    if (tbPageDB.tbPage_Add(objNewMenu))
                    {
                        List<tbPageDATA> curItem = tbPageDB.tbPage_GetByMax();
                        var hasTagName = db.tbPages.Where(s => s.pagTagName == tagName).FirstOrDefault();
                        if (hasTagName != null)
                        {
                            curItem[0].pagTagName = tagName + "-" + curItem[0].pagId;
                        }
                        else
                        {
                            curItem[0].pagTagName = tagName;
                        }
                        curItem[0].pagId = curItem[0].pagId;

                        curItem[0].check1 = "";
                        curItem[0].check2 = "";
                        curItem[0].check3 = "";
                        curItem[0].check4 = "";
                        curItem[0].check5 = "";
                        tbPageDB.tbPage_Update(curItem[0]);

                        ObjtbNewsDATA.newTagName = curItem[0].pagTagName;

                        if (tbNewsDB.tbNews_Add(ObjtbNewsDATA))
                        {
                            string NewRelateID = "", strDauPhay = "";
                            int j = 0;
                            if (lstNewRelate.Items.Count > 0)
                            {
                                for (j = 0; j < lstNewRelate.Items.Count; j++)
                                {
                                    strDauPhay = j == lstNewRelate.Items.Count - 1 ? "" : ";";
                                    NewRelateID += lstNewRelate.Items[j].Text + strDauPhay;
                                }
                            }
                            tbNew dbNew = db.tbNews.OrderByDescending(s => s.newid).FirstOrDefault();
                            if (_Admin == "0" || _Admin == "100" || _Admin == "1")
                            {
                            }
                            else
                            {
                                dbNew.userId = userid;
                            }
                            dbNew.newTag = NewRelateID;
                            dbNew.newAlt = txtalt.Text;
                            db.SubmitChanges();
                            pnlErr.Visible = true;
                            ltrErr.Text = "Thêm mới bài viết thành công !";
                            BindData();
                            Resetcontrol();
                            pnlListForder.Visible = true;
                            pnlAddForder.Visible = false;
                        }
                    }
                    #endregion

                }
                else
                {

                    List<tbNewsDATA> list = tbNewsDB.tbNews_GetByID(strID);
                    if (list.Count > 0)
                    {
                        Session["name"] = list[0].newName;
                        string NewRelateID1 = "", strDauPhay1 = "";
                        int j = 0;
                        if (lstNewRelate.Items.Count > 0)
                        {
                            for (j = 0; j < lstNewRelate.Items.Count; j++)
                            {
                                strDauPhay1 = j == lstNewRelate.Items.Count - 1 ? "" : ";";
                                NewRelateID1 += lstNewRelate.Items[j].Text + strDauPhay1;
                            }
                        }
                        var n = db.tbNews.Where(u => u.newid == Convert.ToInt32(strID)).FirstOrDefault();
                        n.newAlt = txtalt.Text;
                        n.newTag = NewRelateID1;
                        db.SubmitChanges();
                        list[0].newid = strID;
                        list[0].newName = txtTieudetin.Text;
                        list[0].newImage = txtImage.Text;
                        list[0].newFile = txtFile.Text;
                        list[0].newContent = txtTomtat.Text;
                        list[0].newDetail = FckChitiet.Text;
                        list[0].newDate = string.Format("{0:MM/dd/yyyy HH:mm:ss}", datetime);
                        list[0].newimg = chkSukien.Checked ? "1" : "0";
                        list[0].skId = drlSukien.SelectedValue;
                        list[0].newTitle = txtTieudetitle.Text;
                        list[0].newKeyword = txtKeyword.Text;
                        list[0].newDescription = txtDesscription.Text;
                        list[0].grnID = ddlForder.SelectedValue;
                        list[0].newPriority = chkUutien.Checked ? "1" : "0";
                        list[0].newIndex = chkIndex.Checked ? "1" : "0";
                        list[0].newActive = chkActive.Checked ? "0" : "1";
                        list[0].newLang = lang;
                        list[0].newType = chkChuyenDe.Checked ? "1" : "0";
                        #region Page
                        List<tbPageDATA> curPage = tbPageDB.tbPage_GetByTagName(list[0].newTagName).ToList();
                        string bfTagname = "";
                        if (curPage.Count > 0)
                        {
                            bfTagname = curPage[0].pagTagName;
                            curPage[0].pagName = txtTieudetin.Text;
                            curPage[0].pagImage = "";
                            curPage[0].pagType = pageType.ND;
                            var curNewType = db.tbPages.Where(s => s.pagId == int.Parse(ddlForder.SelectedValue)).FirstOrDefault();
                            curPage[0].paglevel = curNewType.paglevel + "00000";
                            curPage[0].pagTitle = txtTieudetitle.Text;
                            curPage[0].pagDescription = txtDesscription.Text;
                            curPage[0].pagKeyword = txtKeyword.Text;
                            curPage[0].pagOrd = "0";
                            curPage[0].pagActive = chkActive.Checked ? "0" : "1";
                            curPage[0].pagLang = lang;
                            curPage[0].pagDetail = "";
                            curPage[0].pagLink = "";
                            curPage[0].pagTarget = "";
                            curPage[0].pagPosition = "0";
                            curPage[0].catId = "0";
                            curPage[0].grnId = "0";
                            curPage[0].pagTagName = curPage[0].pagTagName;
                            if (txtTieudetin.Text != Session["name"].ToString())
                            {
                                string tagName = MyWeb.Common.StringClass.NameToTag(txtTieudetin.Text);
                                var hasTagName = db.tbPages.Where(s => s.pagTagName == MyWeb.Common.StringClass.NameToTag(txtTieudetin.Text)).FirstOrDefault();
                                curPage[0].pagTagName = hasTagName != null ? MyWeb.Common.StringClass.NameToTag(txtTieudetin.Text) + "-" + curPage[0].pagId : MyWeb.Common.StringClass.NameToTag(txtTieudetin.Text);
                                #region Update các menu liên kết đến
                                List<tbPageDATA> lstUpdate = tbPageDB.tbPage_GetByAll(lang);
                                lstUpdate = lstUpdate.Where(s => s.pagLink == bfTagname).ToList();
                                if (lstUpdate.Count > 0)
                                {
                                    for (int i = 0; i < lstUpdate.Count; i++)
                                    {
                                        lstUpdate[i].pagLink = curPage[0].pagTagName;
                                        tbPageDB.tbPage_Update(lstUpdate[i]);
                                    }
                                }
                                #endregion
                            }
                            curPage[0].check1 = "";
                            curPage[0].check2 = "";
                            curPage[0].check3 = "";
                            curPage[0].check4 = "";
                            curPage[0].check5 = "";
                            list[0].newTagName = curPage[0].pagTagName;
                            if (tbPageDB.tbPage_Update(curPage[0]))
                            {
                            }

                            tbNew deltag = db.tbNews.Where(s => s.newid == Int32.Parse(strID)).FirstOrDefault();
                            db.SubmitChanges();
                            if (tbNewsDB.tbNews_Update(list[0]))
                            {
                                string NewRelateID = "", strDauPhay = "";
                                int jg = 0;
                                if (lstNewRelate.Items.Count > 0)
                                {
                                    for (jg = 0; jg < lstNewRelate.Items.Count; jg++)
                                    {
                                        strDauPhay = jg == lstNewRelate.Items.Count - 1 ? "" : ";";
                                        NewRelateID += lstNewRelate.Items[jg].Text + strDauPhay;
                                    }
                                }

                                if (_Admin == "0" || _Admin == "100" || _Admin == "1")
                                {
                                }
                                else
                                {
                                    deltag.userId = userid;
                                }
                                db.SubmitChanges();
                            }
                        }
                        else
                        {
                            tbPageDATA objNewMenu = new tbPageDATA();
                            objNewMenu.pagName = txtTieudetin.Text;
                            objNewMenu.pagImage = "";
                            objNewMenu.pagType = pageType.ND;
                            var curNewType = db.tbPages.Where(s => s.pagId == int.Parse(ddlForder.SelectedValue)).FirstOrDefault();
                            objNewMenu.paglevel = curNewType.paglevel + "00000";
                            objNewMenu.pagTitle = txtTieudetitle.Text;
                            objNewMenu.pagDescription = txtDesscription.Text;
                            objNewMenu.pagKeyword = txtKeyword.Text;
                            objNewMenu.pagOrd = "0";
                            objNewMenu.pagActive = chkActive.Checked ? "0" : "1";
                            objNewMenu.pagLang = lang;
                            objNewMenu.pagDetail = "";
                            objNewMenu.pagLink = "";
                            objNewMenu.pagTarget = "";
                            objNewMenu.pagPosition = "0";
                            objNewMenu.catId = "";
                            objNewMenu.grnId = "";
                            objNewMenu.pagTagName = "";
                            objNewMenu.check1 = "";
                            objNewMenu.check2 = "";
                            objNewMenu.check3 = "";
                            objNewMenu.check4 = "";
                            objNewMenu.check5 = "";
                            string tagName = MyWeb.Common.StringClass.NameToTag(txtTieudetin.Text);

                            if (tbPageDB.tbPage_Add(objNewMenu))
                            {
                                List<tbPageDATA> curItem = tbPageDB.tbPage_GetByMax();
                                var hasTagName = db.tbPages.Where(s => s.pagTagName == tagName).FirstOrDefault();
                                if (hasTagName != null)
                                {
                                    curItem[0].pagTagName = tagName + "-" + curItem[0].pagId;
                                }
                                else
                                {
                                    curItem[0].pagTagName = tagName;
                                }
                                curItem[0].pagId = curItem[0].pagId;

                                curItem[0].check1 = "";
                                curItem[0].check2 = "";
                                curItem[0].check3 = "";
                                curItem[0].check4 = "";
                                curItem[0].check5 = "";
                                list[0].newTagName = curItem[0].pagTagName;
                                tbPageDB.tbPage_Update(curItem[0]);



                                tbNew deltag = db.tbNews.Where(s => s.newid == Int32.Parse(strID)).FirstOrDefault();
                                db.SubmitChanges();

                                if (tbNewsDB.tbNews_Update(list[0]))
                                {
                                    string NewRelateID = "", strDauPhay = "";
                                    int jg = 0;
                                    if (lstNewRelate.Items.Count > 0)
                                    {
                                        for (jg = 0; jg < lstNewRelate.Items.Count; jg++)
                                        {
                                            strDauPhay = jg == lstNewRelate.Items.Count - 1 ? "" : ";";
                                            NewRelateID += lstNewRelate.Items[jg].Text + strDauPhay;
                                        }
                                    }

                                    if (_Admin == "0" || _Admin == "100" || _Admin == "1")
                                    {
                                    }
                                    else
                                    {
                                        deltag.userId = userid;
                                    }
                                    db.SubmitChanges();
                                }
                            }
                        }
                        #endregion
                    }

                    pnlErr.Visible = true;
                    ltrErr.Text = "Cập nhật bài viết thành công !";
                    Resetcontrol();
                    BindData();
                    hidID.Value = "";

                    pnlListForder.Visible = true;
                    pnlAddForder.Visible = false;

                }
            }
        }

        protected void getTags()
        {
            IEnumerable<tbTag> list = db.tbTags.Where(s => s.Lang == lang && s.Active == 1 && s.Type == 2).OrderBy(s => s.Ord).ThenByDescending(s => s.Id);

            foreach (tbTag n in list)
            {
                lstNew.Items.Add(new ListItem(n.Name, n.Id.ToString()));
            }
        }

        protected void btnActive_Click(object sender, EventArgs e)
        {
            int i = 0;
            for (i = 0; i <= lstNewRelate.Items.Count - 1; i++)
            {
                if (lstNewRelate.Items[i].Value == lstNew.SelectedValue)
                {
                    return;
                }
            }
            lstNewRelate.Items.Add(new ListItem(lstNew.SelectedItem.Text, lstNew.SelectedItem.Value));
        }

        protected void btnUnactive_Click(object sender, EventArgs e)
        {
            int i = 0;
            for (i = 0; i <= lstNewRelate.Items.Count - 1; i++)
            {
                if (lstNewRelate.Items[i].Selected == true)
                {
                    lstNewRelate.Items.RemoveAt(lstNewRelate.SelectedIndex);
                    return;
                }
            }
        }

        public string BindCateName(string grnID)
        {
            var curNewType = db.tbPages.Where(s => s.pagId == int.Parse(grnID)).FirstOrDefault();
            return curNewType != null ? curNewType.pagName : "";
        }

        protected string FormatDate(object date)
        {
            if (date.ToString().Trim().Length > 0)
            {
                if (DateTime.Parse(date.ToString()).Year != 1900)
                {
                    DateTime dNgay = Convert.ToDateTime(date.ToString());
                    return ((DateTime)dNgay).ToString("MM/dd/yyyy hh:mm:ss");
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }

        public static string ShowActive(string proActive)
        {
            return proActive == "0" || proActive == "True" ? "<i class=\"fa fa-check\"></i>" : "<i class=\"fa fa-times\"></i>";
        }

        protected void rptListNews_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string strID = e.CommandArgument.ToString();
            switch (e.CommandName.ToString())
            {
                case "Active":
                    tbNew obj = db.tbNews.FirstOrDefault(s => s.newid == Int32.Parse(strID));
                    if (obj.newActive == 0) { obj.newActive = 1; } else { obj.newActive = 0; }
                    db.SubmitChanges();
                    BindData();
                    pnlErr.Visible = true;
                    ltrErr.Text = "Cập nhật trạng thái thành công !";
                    break;
                case "Copy":
                    var itemNewCP = db.tbNews.FirstOrDefault(s => s.newid == int.Parse(strID));
                    tbNew itemCP = new tbNew();
                    itemCP.newName = itemNewCP.newName;
                    itemCP.newImage = itemNewCP.newImage;
                    itemCP.newFile = itemNewCP.newFile;
                    itemCP.newContent = itemNewCP.newContent;
                    itemCP.newDetail = itemNewCP.newDetail;
                    itemCP.newDate = itemNewCP.newDate;
                    itemCP.newTitle = itemNewCP.newTitle;
                    itemCP.newDescription = itemNewCP.newDescription;
                    itemCP.newKeyword = itemNewCP.newKeyword;
                    itemCP.newPriority = itemNewCP.newPriority;
                    itemCP.newIndex = itemNewCP.newIndex;
                    itemCP.newActive = itemNewCP.newActive;
                    itemCP.newimg = itemNewCP.newimg;
                    itemCP.skId = itemNewCP.skId;
                    itemCP.grnID = itemNewCP.grnID;
                    itemCP.newType = itemNewCP.newType;
                    itemCP.usrID = itemNewCP.usrID;
                    itemCP.newLang = itemNewCP.newLang;

                    #region Them vao tbPage + tbNews
                    tbPageDATA objNewMenu = new tbPageDATA();
                    objNewMenu.pagName = itemNewCP.newName;
                    objNewMenu.pagImage = "";
                    objNewMenu.pagType = pageType.ND;
                    var curNewType = db.tbPages.Where(s => s.pagId == itemNewCP.grnID).FirstOrDefault();
                    objNewMenu.paglevel = curNewType.paglevel + "00000";
                    objNewMenu.pagTitle = itemNewCP.newTitle;
                    objNewMenu.pagDescription = itemNewCP.newDescription;
                    objNewMenu.pagKeyword = itemNewCP.newKeyword;
                    objNewMenu.pagOrd = "0";
                    objNewMenu.pagActive = itemNewCP.newActive.ToString();
                    objNewMenu.pagLang = lang;
                    objNewMenu.pagDetail = "";
                    objNewMenu.pagLink = "";
                    objNewMenu.pagTarget = "";
                    objNewMenu.pagPosition = "0";
                    objNewMenu.catId = "";
                    objNewMenu.grnId = "";
                    objNewMenu.pagTagName = "";
                    objNewMenu.check1 = "";
                    objNewMenu.check2 = "";
                    objNewMenu.check3 = "";
                    objNewMenu.check4 = "";
                    objNewMenu.check5 = "";
                    string tagName = itemNewCP.newTagName;

                    if (tbPageDB.tbPage_Add(objNewMenu))
                    {
                        List<tbPageDATA> curItem = tbPageDB.tbPage_GetByMax();
                        var hasTagName = db.tbPages.Where(s => s.pagTagName == tagName).FirstOrDefault();
                        if (hasTagName != null)
                        {
                            curItem[0].pagTagName = tagName + "-" + curItem[0].pagId;
                        }
                        else
                        {
                            curItem[0].pagTagName = tagName;
                        }
                        curItem[0].pagId = curItem[0].pagId;

                        curItem[0].check1 = "";
                        curItem[0].check2 = "";
                        curItem[0].check3 = "";
                        curItem[0].check4 = "";
                        curItem[0].check5 = "";
                        tbPageDB.tbPage_Update(curItem[0]);

                        itemCP.newTagName = curItem[0].pagTagName;

                        db.tbNews.InsertOnSubmit(itemCP);
                        db.SubmitChanges();
                        string NewRelateID = "", strDauPhay = "";
                        int j = 0;
                        if (lstNewRelate.Items.Count > 0)
                        {
                            for (j = 0; j < lstNewRelate.Items.Count; j++)
                            {
                                strDauPhay = j == lstNewRelate.Items.Count - 1 ? "" : ";";
                                NewRelateID += lstNewRelate.Items[j].Text + strDauPhay;
                            }
                        }
                        tbNew dbNew2 = db.tbNews.OrderByDescending(s => s.newid).FirstOrDefault();
                        if (_Admin == "0" || _Admin == "100" || _Admin == "1")
                        {
                        }
                        else
                        {
                            dbNew2.userId = userid;
                        }
                        dbNew2.newTag = NewRelateID;
                        dbNew2.newAlt = txtalt.Text;
                        db.SubmitChanges();
                        pnlErr.Visible = true;
                        ltrErr.Text = "Copy bài viết thành công !";
                        BindData();
                        Resetcontrol();
                        pnlListForder.Visible = true;
                        pnlAddForder.Visible = false;
                    }
                    #endregion

                    break;
                case "Del":
                    string tagName2 = db.tbNews.Where(s => s.newid == int.Parse(strID)).FirstOrDefault().newTagName;

                    try
                    {
                        string pagId = db.tbPages.Where(s => s.pagTagName == tagName2).FirstOrDefault().pagId.ToString();
                        tbPageDB.tbPage_Delete(pagId);
                    }
                    catch { }

                    var data = db.tbComments.Where(u => u.comNewId == Convert.ToInt32(strID)).ToList();
                    if (data.Count > 0)
                    {
                        for (int i = 0; i < data.Count; i++)
                        {
                            db.tbComments.DeleteOnSubmit(data[i]);
                            db.SubmitChanges();
                        }
                    }

                    tbNewsDB.tbNews_Delete(strID);
                    pnlErr.Visible = true;
                    ltrErr.Text = "Xóa thành công nhóm chuyên mục !";
                    BindData();
                    break;
                case "Edit":
                    BindForder();
                    tbNew dbNew = db.tbNews.FirstOrDefault(s => s.newid == Int32.Parse(strID));
                    hidID.Value = strID;
                    txtalt.Text = dbNew.newAlt;
                    try
                    {
                        ddlForder.SelectedValue = dbNew.grnID.ToString();
                    }
                    catch { }
                    txtTieudetin.Text = dbNew.newName;
                    txtTomtat.Text = dbNew.newContent;
                    FckChitiet.Text = dbNew.newDetail;
                    txtImage.Text = dbNew.newImage;

                    txtFile.Text = dbNew.newFile;
                    txtTieudetitle.Text = dbNew.newTitle;
                    txtDesscription.Text = dbNew.newDescription;
                    txtKeyword.Text = dbNew.newKeyword;

                    chkUutien.Checked = dbNew.newPriority == 1 ? true : false;
                    chkIndex.Checked = dbNew.newIndex == 1 ? true : false;
                    chkActive.Checked = dbNew.newActive == 0 ? true : false;
                    txtNewsDate.Text = string.Format("{0:MM/dd/yyyy}", dbNew.newDate);

                    if (dbNew.newTag != null)
                    {
                        int i = 0;
                        string strMaTin = null;
                        strMaTin = dbNew.newTag;
                        string[] MaTin = null;
                        MaTin = strMaTin.Split(new Char[] { ';' });
                        string k = null;
                        lstB.Items.Clear();
                        foreach (string k_loopVariable in MaTin)
                        {
                            k = k_loopVariable;
                            if (!string.IsNullOrEmpty(k.Trim()))
                            {
                                lstB.Items.Add(new ListItem(k, k));
                            }
                        }
                        lstNewRelate.Items.Clear();
                        for (i = 0; i < lstB.Items.Count; i++)
                        {

                            IEnumerable<tbTag> list2 = db.tbTags.Where(s => s.Lang == lang && s.Active == 1 && s.Type == 2);
                            foreach (tbTag n in list2)
                            {
                                if (lstB.Items[i].Text == n.Name.ToString())
                                {
                                    lstNewRelate.Items.Add(new ListItem(n.Name.ToString(), n.Id.ToString()));
                                }
                            }
                        }
                    }
                    pnlErr.Visible = false;
                    ltrErr.Text = "";

                    pnlListForder.Visible = false;
                    pnlAddForder.Visible = true;
                    break;
            }

        }

        protected void txtName_TextChanged(object sender, EventArgs e)
        {
            TextBox txtName = (TextBox)sender;
            var blbID = (Label)txtName.FindControl("lblID");
            var obj = db.tbNews.FirstOrDefault(s => s.newid == int.Parse(blbID.Text));
            if (obj != null)
            {

                obj.newName = txtName.Text;
                db.SubmitChanges();
            }
            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Cập nhật tên nhóm thành công !";
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = true;
            pnlAddForder.Visible = false;
            BindData();
            Session["insert"] = "false";
            hidID.Value = "";
        }

        public static string ShowActiveClass(string proActive)
        {
            return proActive == "0" || proActive == "True" ? "btn btn-primary btn-xs" : "btn btn-danger btn-xs";
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }
    }
}