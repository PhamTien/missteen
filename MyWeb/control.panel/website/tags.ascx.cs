﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.website
{
    public partial class tags : System.Web.UI.UserControl
    {
        private string lang = "vi";
        int pageSize = 15;
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            if (!IsPostBack)
            {
                BindData();
            }
        }

        void BindData()
        {
            List<tbTag> _listForder = db.tbTags.Where(t => t.Lang == lang).OrderBy(t => t.Ord).ThenByDescending(t => t.Id).ToList();

            if (drlFind.SelectedValue != "0")
            {
                _listForder = _listForder.Where(s => s.Type == int.Parse(drlFind.SelectedValue)).ToList();
            }

            if (_listForder.Count() > 0)
            {
                recordCount = _listForder.Count();
                #region Statistic
                int fResult = currentPage * pageSize + 1;
                int tResult = (currentPage + 1) * pageSize;
                tResult = tResult > recordCount ? recordCount : tResult;
                ltrStatistic.Text = "Hiển thị kết quả từ " + fResult + " - " + tResult + " trong tổng số " + recordCount + " kết quả";
                #endregion

                var result = _listForder.Skip(currentPage * pageSize).Take(pageSize);
                rptFolderList.DataSource = result;
                rptFolderList.DataBind();
                BindPaging();
            }
            else
            {
                rptFolderList.DataSource = null;
                rptFolderList.DataBind();
            }
        }

        #region Page

        private int currentPage { get { return ViewState["currentPage"] != null ? int.Parse(ViewState["currentPage"].ToString()) : 0; } set { ViewState["currentPage"] = value; } }
        private int recordCount { get { return ViewState["recordCount"] != null ? int.Parse(ViewState["recordCount"].ToString()) : 0; } set { ViewState["recordCount"] = value; } }
        private int pageCount { get { double iCount = (double)((decimal)recordCount / (decimal)pageSize); return (int)Math.Ceiling(iCount); } }

        private void BindPaging()
        {
            int icurPage = currentPage + 1;
            int ipCount = pageCount;
            if (ipCount >= 1)
            {
                rptNumberPage.Visible = true;
                int PageShow = ipCount > 5 ? 5 : ipCount;
                int FromPage;
                int ToPage;
                DataTable dt = new DataTable();
                dt.Columns.Add("PageIndex");
                dt.Columns.Add("PageText");
                FromPage = icurPage > PageShow ? icurPage - PageShow : 1;
                ToPage = (ipCount - icurPage > PageShow) ? icurPage + PageShow : ipCount;
                if (icurPage - 10 > 0) dt.Rows.Add(icurPage - 9, icurPage - 10);
                for (int i = FromPage; i <= ToPage; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = i - 1;
                    dr[1] = i;
                    dt.Rows.Add(dr);
                }
                if (icurPage + 10 <= ipCount) dt.Rows.Add(icurPage + 9, icurPage + 10);
                rptNumberPage.DataSource = dt;
                rptNumberPage.DataBind();
            }
        }

        protected void btnPage_Click(object sender, EventArgs e)
        {
            if (((LinkButton)sender).ID == "btnPrevious")
            {
                if (currentPage > 0) currentPage = currentPage - 1;
            }
            else if (((LinkButton)sender).ID == "btnNext")
            {
                if (currentPage < pageCount - 1) currentPage = currentPage + 1;
            }
            BindData();
        }

        protected void rptNumberPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("page"))
            {
                currentPage = Convert.ToInt32(e.CommandArgument.ToString());
                BindData();
            }
        }

        protected void rptNumberPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lnkPage = (LinkButton)e.Item.FindControl("btn");
            Literal _ltrPage = (Literal)e.Item.FindControl("ltrLiPage");
            if (lnkPage.CommandArgument.ToString() == currentPage.ToString())
            {
                lnkPage.Enabled = false;
                _ltrPage.Text = "<li class=\"paginate_button active\">";
            }
            else
            {
                _ltrPage.Text = "<li class=\"paginate_button\">";
            }
        }

        #endregion

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = false;
            pnlAddForder.Visible = true;
            Resetcontrol();
        }

        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
            RepeaterItem item = default(RepeaterItem);
            for (int i = 0; i < rptFolderList.Items.Count; i++)
            {
                item = rptFolderList.Items[i];
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    if (((CheckBox)item.FindControl("chkBox")).Checked)
                    {
                        HiddenField hidID = (HiddenField)item.FindControl("hidCatID");

                        tbTag tag = db.tbTags.Where(t => t.Id == int.Parse(hidID.Value)).FirstOrDefault();
                        if (tag != null)
                        {
                            db.tbTags.DeleteOnSubmit(tag);
                            db.SubmitChanges();
                        }
                    }
                }
            }
            pnlErr.Visible = true;
            ltrErr.Text = "Xóa thành công !";
            BindData();
            chkSelectAll.Checked = false;
        }

        protected void txtTit_TextChanged(object sender, EventArgs e)
        {
            TextBox txtTit = (TextBox)sender;
            var b = (Label)txtTit.FindControl("lblID");
            var tag = db.tbTags.Where(t => t.Id == int.Parse(b.Text)).FirstOrDefault();
            if (tag != null)
            {

                tag.Title = txtTit.Text;
                db.SubmitChanges();
            }
            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Cập nhật thành công !";
        }

        protected void txtDes_TextChanged(object sender, EventArgs e)
        {
            TextBox txtDes = (TextBox)sender;
            var b = (Label)txtDes.FindControl("lblID");
            var tag = db.tbTags.Where(t => t.Id == int.Parse(b.Text)).FirstOrDefault();
            if (tag != null)
            {

                tag.Description = txtDes.Text;
                db.SubmitChanges();
            }
            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Cập nhật thành công !";
        }

        protected void rptFolderList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string strID = e.CommandArgument.ToString();
            switch (e.CommandName.ToString())
            {
                case "Active":
                    var tag2 = db.tbTags.Where(t => t.Id == int.Parse(strID)).FirstOrDefault();
                    if (tag2.Active == 0)
                    {
                        tag2.Active = 1;
                    }
                    else
                    {
                        tag2.Active = 0;
                    }
                    db.SubmitChanges();
                    BindData();
                    pnlErr.Visible = true;
                    ltrErr.Text = "Cập nhật trạng thái thành công !";
                    break;
                case "Del":
                    tbTag tag = db.tbTags.Where(t => t.Id == int.Parse(strID)).FirstOrDefault();
                    if (tag != null)
                    {
                        db.tbTags.DeleteOnSubmit(tag);
                        db.SubmitChanges();
                        ltrErr.Text = "Xóa thành công!!";
                    }
                    else
                    {
                        ltrErr.Text = "Xóa không thành công !!";
                    }
                    BindData();
                    pnlErr.Visible = true;
                    break;

                case "Edit":
                    hidID.Value = strID;
                    List<tbTag> list = db.tbTags.Where(t => t.Id == int.Parse(strID)).ToList();
                    txtName.Text = list[0].Name;
                    drlType.SelectedValue = list[0].Type.ToString();
                    txtTitle.Text = list[0].Title;
                    txtDescription.Text = list[0].Description;
                    //txtKeyword.Text = list[0].Keyword;
                    txtThuTu.Text = list[0].Ord.ToString();
                    chkIndex.Checked = list[0].Index == 1 ? true : false;
                    chkActive.Checked = list[0].Active == 1 ? true : false;
                    pnlErr.Visible = false;
                    ltrErr.Text = "";

                    pnlListForder.Visible = false;
                    pnlAddForder.Visible = true;
                    break;
            }
        }

        protected void txtNameForder_TextChanged(object sender, EventArgs e)
        {
            TextBox txtNameForder = (TextBox)sender;
            var b = (Label)txtNameForder.FindControl("lblID");
            var tag = db.tbTags.Where(t => t.Id == int.Parse(b.Text)).FirstOrDefault();
            if (tag != null)
            {
                tag.Name = txtNameForder.Text;
                db.SubmitChanges();
            }
            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Cập nhật thành công !";
        }

        protected void txtNumberOrder_TextChanged(object sender, EventArgs e)
        {
            TextBox txtNumberOrder = (TextBox)sender;
            var b = (Label)txtNumberOrder.FindControl("lblID");
            var tag = db.tbTags.Where(t => t.Id == int.Parse(b.Text)).FirstOrDefault();
            if (tag != null)
            {
                tag.Ord = int.Parse(txtNumberOrder.Text);
                db.SubmitChanges();
            }
            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Cập nhật thành công !";
        }

        public static string ShowActive(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "<i class=\"fa fa-check\"></i>" : "<i class=\"fa fa-times\"></i>";
        }

        void Resetcontrol()
        {
            hidID.Value = "";
            drlType.SelectedValue = "1";
            txtName.Text = "";
            txtTitle.Text = "";
            txtDescription.Text = "";
            txtKeyword.Text = "";
            chkActive.Checked = false;
            chkIndex.Checked = false;
            txtThuTu.Text = (db.tbTags.Max(s => s.Ord) + 1).ToString();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (Validation())
            {
                int sadvActive = 0;
                if (chkActive.Checked == true)
                {
                    sadvActive = 1;
                }
                else
                {
                    sadvActive = 0;
                }
                string strID = hidID.Value;
                if (strID.Length == 0)
                {


                    tbTag ord = new tbTag
                    {
                        Name = txtName.Text,
                        Type = int.Parse(drlType.SelectedValue),
                        Title = txtTitle.Text,
                        Description = txtDescription.Text,
                        Keyword = MyWeb.Common.StringClass.RemoveUnicode(txtName.Text),
                        Ord = int.Parse(txtThuTu.Text),
                        Index = chkIndex.Checked == true ? 1 : 0,
                        Active = sadvActive,
                        Lang = lang
                    };
                    db.tbTags.InsertOnSubmit(ord);
                    try
                    {
                        db.SubmitChanges();
                        pnlErr.Visible = true;
                        ltrErr.Text = "Thêm mới chuyên mục thành công !";
                        BindData();
                        Resetcontrol();
                        pnlListForder.Visible = true;
                        pnlAddForder.Visible = false;
                    }
                    catch { }
                }
                else
                {
                    if (strID.Length > 0)
                    {
                        var list = db.tbTags.Where(t => t.Id == int.Parse(strID)).FirstOrDefault();
                        if (list != null)
                        {
                            list.Name = txtName.Text;
                            list.Type = int.Parse(drlType.SelectedValue);
                            list.Title = txtTitle.Text;
                            list.Description = txtDescription.Text;
                            list.Keyword = MyWeb.Common.StringClass.RemoveUnicode(txtName.Text);
                            list.Ord = int.Parse(txtThuTu.Text);
                            list.Index = chkIndex.Checked == true ? 1 : 0;
                            list.Active = chkActive.Checked == true ? 1 : 0;

                            try
                            {
                                db.SubmitChanges();
                                pnlErr.Visible = true;
                                ltrErr.Text = "Cập nhật chuyên mục thành công !";
                                Resetcontrol();
                                BindData();
                                hidID.Value = "";
                                pnlListForder.Visible = true;
                                pnlAddForder.Visible = false;
                            }
                            catch { }
                        }

                    }
                }
            }
        }

        protected bool Validation()
        {
            if (txtName.Text == "") { ltrErr2.Text = "Chưa nhập tên thẻ tags !"; txtName.Focus(); pnlErr2.Visible = true; return false; }
            return true;
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = true;
            pnlAddForder.Visible = false;
            BindData();
            Session["insert"] = "false";
            hidID.Value = "";
        }

        protected void drlFind_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

    }
}