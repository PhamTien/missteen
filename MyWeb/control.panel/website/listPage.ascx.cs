﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.website
{
    public partial class listPage : System.Web.UI.UserControl
    {
        string _Admin = "";
        private string lang = "vi";
        int pageSize = 15;
        string userid = "";
        dataAccessDataContext db = new dataAccessDataContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            if (Session["admin"] != null) { _Admin = Session["admin"].ToString(); } if (Session["admin"] != null) { _Admin = Session["admin"].ToString(); }
            if (Session["uid"] != null) userid = Session["uid"].ToString();
            if (!IsPostBack)
            {
                BindForder();
                BindForderAdd();
                LoadGroupLink();
                ViewCatePage();
                LoadTourCategory();
                Viewcategory();
                ViewLibrary();
                BindData();
            }
        }

        void BindData()
        {
           
                List<tbPageDATA> _listForder = tbPageDB.tbPage_GetByAll(lang);
                _listForder = _listForder.Where(s => int.Parse(s.pagType) < 99).ToList();

                if (txtSearch.Text != "")
                {
                    _listForder = _listForder.Where(s => s.pagName.ToLower().Contains(txtSearch.Text.ToLower())).ToList();
                }

                if (drlForder.SelectedValue != "0")
                {
                    _listForder = _listForder.Where(s => s.paglevel.StartsWith(drlForder.SelectedValue)).ToList();
                }

                if (drlVitriFind.SelectedValue != "0")
                {
                    _listForder = _listForder.Where(s => s.pagPosition == drlVitriFind.SelectedValue).ToList();
                }
                if (_listForder.Count() > 0)
                {
                    recordCount = _listForder.Count();
                    #region Statistic
                    int fResult = currentPage * pageSize + 1;
                    int tResult = (currentPage + 1) * pageSize;
                    tResult = tResult > recordCount ? recordCount : tResult;
                    ltrStatistic.Text = "Hiển thị kết quả từ " + fResult + " - " + tResult + " trong tổng số " + recordCount + " kết quả";
                    #endregion

                    var result = _listForder.Skip(currentPage * pageSize).Take(pageSize);
                    rptFolderList.DataSource = result;
                    rptFolderList.DataBind();
                    BindPaging();
                }
                else
                {
                    rptFolderList.DataSource = null;
                    rptFolderList.DataBind();
                }            
        }

        void BindForderAdd()
        {
            List<tbPageDATA> list = tbPageDB.tbPage_GetByAll(lang);
            list = list.Where(s => int.Parse(s.pagType) < 99).ToList();

            if (drlVitri.SelectedValue != "0")
            {
                list = list.Where(s => s.pagPosition == drlVitri.SelectedValue).ToList();
            }

            drlChuyenmuc.Items.Clear();
            drlChuyenmuc.Items.Add(new ListItem("- Chọn cấp cha -", ""));
            for (int i = 0; i < list.Count; i++)
            {
                string space = "";
                for (int j = 0; j < list[i].paglevel.Length / 5 - 1; j++) space += "-----";
                drlChuyenmuc.Items.Add(new ListItem(space + list[i].pagName, list[i].paglevel));
            }
            list.Clear();
            list = null;
        }

        void BindForder()
        {
            List<tbPageDATA> list = tbPageDB.tbPage_GetByAll(lang);
            list = list.Where(s => int.Parse(s.pagType) < 99).ToList();
            drlForder.Items.Clear();
            drlForder.Items.Add(new ListItem("- Chọn cấp cha -", ""));
            for (int i = 0; i < list.Count; i++)
            {
                string space = "";
                for (int j = 0; j < list[i].paglevel.Length / 5 - 1; j++) space += "-----";
                drlForder.Items.Add(new ListItem(space + list[i].pagName, list[i].paglevel));
            }
            list.Clear();
            list = null;
        }

        protected string BindView(string name, string treecode)
        {
            string str = "<i class=\"ion-ios-folder fa-2x text-inverse icon-forder\"></i>";
            if (treecode.ToString().Trim().Length > 50)
            {
                str = "<span class='icon-forder' style='margin-left:100px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i></span>";
            }
            else if (treecode.ToString().Trim().Length > 45)
            {
                str = "<span class='icon-forder' style='margin-left:90px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i></span>";
            }
            else if (treecode.ToString().Trim().Length > 40)
            {
                str = "<span class='icon-forder' style='margin-left:80px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i></span>";
            }
            else if (treecode.ToString().Trim().Length > 35)
            {
                str = "<span class='icon-forder' style='margin-left:70px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i></span>";
            }
            else if (treecode.ToString().Trim().Length > 30)
            {
                str = "<span class='icon-forder' style='margin-left:60px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i></span>";
            }
            else if (treecode.ToString().Trim().Length > 25)
            {
                str = "<span class='icon-forder' style='margin-left:50px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i></span>";
            }
            else if (treecode.ToString().Trim().Length > 20)
            {
                str = "<span class='icon-forder' style='margin-left:40px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i><span>";
            }
            else if (treecode.ToString().Trim().Length > 15)
            {
                str = "<span class='icon-forder' style='margin-left:30px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i></span>";
            }
            else if (treecode.ToString().Trim().Length > 10)
            {
                str = "<span class='icon-forder' style='margin-left:20px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i></span>";
            }
            else if (treecode.ToString().Trim().Length > 5)
            {
                str = "<span class='icon-forder' style='margin-left:10px'><i class=\"ion-ios-arrow-thin-right fa-2x text-inverse\"></i></span>";
            }
            else
            {
                str = str;
            }
            return str = str + "&nbsp;" + name;
        }

        public string BindCateName(string strID)
        {
            string strReturn = "";
            var obj = db.tbPages.Where(s => s.paglevel == strID.Substring(0, strID.Length - 5)).ToList();
            if (obj.Count > 0)
            {
                strReturn = obj[0].pagName;
            }
            return strReturn;
        }

        public static string ShowActive(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "<i class=\"fa fa-check\"></i>" : "<i class=\"fa fa-times\"></i>";
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = false;
            pnlAddForder.Visible = true;
            pnlErr2.Visible = false;
            ltrErr2.Text = "";
            BindForderAdd();
            Resetcontrol();
        }

        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
            RepeaterItem item = default(RepeaterItem);
            for (int i = 0; i < rptFolderList.Items.Count; i++)
            {
                item = rptFolderList.Items[i];
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    if (((CheckBox)item.FindControl("chkBox")).Checked)
                    {
                        HiddenField hidID = (HiddenField)item.FindControl("hidCatID");
                        tbPage del = db.tbPages.Where(s => s.pagId == int.Parse(hidID.Value)).FirstOrDefault();
                        List<tbPage> catChild = db.tbPages.Where(s => s.paglevel.Substring(0, del.paglevel.Length) == del.paglevel).ToList();
                        if (catChild.Count() > 0)
                        {
                            for (int k = 0; k < catChild.Count; k++)
                            {
                                db.tbPages.DeleteOnSubmit(catChild[k]);
                                db.SubmitChanges();

                            }
                            //pnlErr.Visible = true;
                            //ltrErr.Text = "Xóa thành công !";
                        }
                        else
                        {
                            try
                            {
                                tbPageDB.tbPage_Delete(hidID.Value);
                            }
                            catch { }
                        }
                    }
                }
            }
            pnlErr.Visible = true;
            ltrErr.Text = "Xóa thành công nhóm chuyên mục !";
            BindData();
            chkSelectAll.Checked = false;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }

        protected void drlForder_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        #region Page

        private int currentPage { get { return ViewState["currentPage"] != null ? int.Parse(ViewState["currentPage"].ToString()) : 0; } set { ViewState["currentPage"] = value; } }
        private int recordCount { get { return ViewState["recordCount"] != null ? int.Parse(ViewState["recordCount"].ToString()) : 0; } set { ViewState["recordCount"] = value; } }
        private int pageCount { get { double iCount = (double)((decimal)recordCount / (decimal)pageSize); return (int)Math.Ceiling(iCount); } }

        private void BindPaging()
        {
            int icurPage = currentPage + 1;
            int ipCount = pageCount;
            if (ipCount >= 1)
            {
                rptNumberPage.Visible = true;
                int PageShow = ipCount > 5 ? 5 : ipCount;
                int FromPage;
                int ToPage;
                DataTable dt = new DataTable();
                dt.Columns.Add("PageIndex");
                dt.Columns.Add("PageText");
                FromPage = icurPage > PageShow ? icurPage - PageShow : 1;
                ToPage = (ipCount - icurPage > PageShow) ? icurPage + PageShow : ipCount;
                if (icurPage - 10 > 0) dt.Rows.Add(icurPage - 9, icurPage - 10);
                for (int i = FromPage; i <= ToPage; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = i - 1;
                    dr[1] = i;
                    dt.Rows.Add(dr);
                }
                if (icurPage + 10 <= ipCount) dt.Rows.Add(icurPage + 9, icurPage + 10);
                rptNumberPage.DataSource = dt;
                rptNumberPage.DataBind();
            }
        }

        protected void btnPage_Click(object sender, EventArgs e)
        {
            if (((LinkButton)sender).ID == "btnPrevious")
            {
                if (currentPage > 0) currentPage = currentPage - 1;
            }
            else if (((LinkButton)sender).ID == "btnNext")
            {
                if (currentPage < pageCount - 1) currentPage = currentPage + 1;
            }
            BindData();
        }

        protected void rptNumberPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("page"))
            {
                currentPage = Convert.ToInt32(e.CommandArgument.ToString());
                BindData();
            }
        }

        protected void rptNumberPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lnkPage = (LinkButton)e.Item.FindControl("btn");
            Literal _ltrPage = (Literal)e.Item.FindControl("ltrLiPage");
            if (lnkPage.CommandArgument.ToString() == currentPage.ToString())
            {
                lnkPage.Enabled = false;
                _ltrPage.Text = "<li class=\"paginate_button active\">";
            }
            else
            {
                _ltrPage.Text = "<li class=\"paginate_button\">";
            }
        }

        #endregion

        protected string Position(string strPosition)
        {
            string str = "";
            if (strPosition == "1")
            {
                str = "Menu chính";
            }
            else if (strPosition == "2")
            {
                str = "Menu dưới";
            }
            else if (strPosition == "3")
            {
                str = "Menu trái";
            }
            else if (strPosition == "4")
            {
                str = "Menu top";
            }
            else
            {
                str = "Hỏi đáp";
            }
            return str;
        }

        protected void txtNameForder_TextChanged(object sender, EventArgs e)
        {
            TextBox txtNameForder = (TextBox)sender;
            var lblID = (Label)txtNameForder.FindControl("lblID");
            List<tbPageDATA> list = tbPageDB.tbPage_GetByID(lblID.Text);
            if (list.Count > 0)
            {
                list[0].pagName = txtNameForder.Text;
                tbPageDB.tbPage_Update(list[0]);
            }
            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Cập nhật tên thành công !";
        }

        protected void txtNumberOrder_TextChanged(object sender, EventArgs e)
        {
            TextBox txtOrder = (TextBox)sender;
            var lblID = (Label)txtOrder.FindControl("lblID");
            List<tbPageDATA> list = tbPageDB.tbPage_GetByID(lblID.Text);
            if (list.Count > 0)
            {

                list[0].pagOrd = txtOrder.Text;
                tbPageDB.tbPage_Update(list[0]);
            }
            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Cập nhật thứ tự thành công !";
        }

        protected void rptFolderList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string strID = e.CommandArgument.ToString();
            switch (e.CommandName.ToString())
            {
                case "Active":
                    tbPageDATA item = tbPageDB.tbPage_GetByID(strID).FirstOrDefault();
                    if (item != null)
                    {
                        item.pagId = strID;
                        item.pagActive = item.pagActive == "1" ? "0" : "1";
                        tbPageDB.tbPage_Update(item);
                    }
                    BindData();
                    pnlErr.Visible = true;
                    ltrErr.Text = "Cập nhật trạng thái thành công !";
                    break;
                case "Del":
                    tbPage del = db.tbPages.Where(s => s.pagId == int.Parse(strID)).FirstOrDefault();
                    List<tbPage> catChild = db.tbPages.Where(s => s.paglevel.Substring(0, del.paglevel.Length) == del.paglevel).ToList();
                    if (catChild.Count() > 0)
                    {
                        for (int i = 0; i < catChild.Count; i++)
                        {
                            db.tbPages.DeleteOnSubmit(catChild[i]);
                            db.SubmitChanges();

                        }
                        pnlErr.Visible = true;
                        ltrErr.Text = "Xóa thành công !";
                    }
                    else
                    {
                        tbPageDB.tbPage_Delete(strID);
                        pnlErr.Visible = true;
                        ltrErr.Text = "Xóa thành công !";
                    }
                    BindData();
                    break;
                case "Edit":

                    var list = db.tbPages.Where(u => u.pagId == Convert.ToInt32(strID)).ToList();
                    hidID.Value = list[0].pagId.ToString();
                    hidLevel.Value = list[0].paglevel;
                    txtTenmenu.Text = list[0].pagName;
                    if (list[0].pagType == 3 || list[0].pagType == 1)
                    {
                        txtUrl.Text = list[0].pagLink;
                    }
                    if (list[0].pagImage.Length > 0)
                    {
                        txtImage.Text = list[0].pagImage.ToString();
                    }
                    else
                    {
                        txtImage.Text = "";
                    }
                    txtTieude.Text = list[0].pagTitle;
                    txtDesscription.Text = list[0].pagDescription;
                    txtKeyword.Text = list[0].pagKeyword;
                    txtThuTu.Text = list[0].pagOrd.ToString();
                    BindForder();
                    BindForderAdd();
                    try
                    {
                        drlChuyenmuc.SelectedValue = list[0].paglevel.Substring(0, list[0].paglevel.Length - 5);
                    }
                    catch { }
                    if (list[0].pagActive == 1)
                    {
                        chkKichhoat.Checked = true;
                    }
                    else
                    {
                        chkKichhoat.Checked = false;
                    }
                    if (list[0].pagType > 3)
                    {
                        drlKieutrang.SelectedValue = pageType.ML;
                        drlNhomlienket.SelectedValue = list[0].pagType.ToString();
                    }
                    else drlKieutrang.SelectedValue = list[0].pagType.ToString();

                    fckNoidung.Text = list[0].pagDetail;
                    if (list[0].pagType == Convert.ToInt32(pageType.ML) || list[0].pagType > 3)
                    {
                        if (list[0].pagLink != "/")
                        {
                            string strLink = list[0].pagLink.Substring(0, (list[0].pagLink.Length - 5));
                            var curLink = db.tbPages.Where(s => s.pagTagName == strLink && s.pagType != int.Parse(pageType.MC)).FirstOrDefault();
                            if (curLink != null)
                            {
                                if (curLink.pagType == int.Parse(pageType.GN))
                                {
                                    pnNhom.Visible = true;
                                    drlgrnId.SelectedValue = curLink.pagId.ToString();
                                    drlgrnId.Visible = true;
                                    drlNhomlienket.SelectedValue = pageType.MN;
                                }
                                else if (curLink.pagType == int.Parse(pageType.GM))
                                {
                                    pnMan.Visible = true;
                                    ddlMan.Visible = true;
                                    ddlMan.SelectedValue = curLink.pagId.ToString();
                                    drlNhomlienket.SelectedValue = pageType.MM;
                                }
                                else if (curLink.pagType == int.Parse(pageType.TourCategory))
                                {
                                    LoadTourCategory();
                                    pnTourCategory.Visible = true;
                                    ddlTourCategory.Visible = true;
                                    tbPage tbp = db.tbPages.FirstOrDefault(s => s.pagTagName == strLink && s.pagType == int.Parse(pageType.TourCategory));
                                    if (tbp != null) { ddlTourCategory.SelectedValue = tbp.pagId.ToString(); } else { ddlTourCategory.SelectedValue = "0"; }                                    
                                    drlNhomlienket.SelectedValue = pageType.MenuTourCategory;
                                }
                                else if (curLink.pagType == int.Parse(pageType.TourTopic))
                                {
                                    LoadTourCategory();
                                    pnTourCategory.Visible = true;
                                    ddlTourCategory.Visible = true;
                                    tbPage tbp = db.tbPages.FirstOrDefault(s => s.pagTagName == strLink && s.pagType == int.Parse(pageType.TourTopic));
                                    if (tbp != null) { ddlTourCategory.SelectedValue = tbp.pagId.ToString(); } else { ddlTourCategory.SelectedValue = "0"; }
                                    drlNhomlienket.SelectedValue = pageType.MTT;
                                }
                                else if (curLink.pagType == int.Parse(pageType.TourPlace))
                                {
                                    LoadTourCategory();
                                    pnTourCategory.Visible = true;
                                    ddlTourCategory.Visible = true;
                                    tbPage tbp = db.tbPages.FirstOrDefault(s => s.pagTagName == strLink && s.pagType == int.Parse(pageType.TourPlace));
                                    if (tbp != null) { ddlTourCategory.SelectedValue = tbp.pagId.ToString(); } else { ddlTourCategory.SelectedValue = "0"; }
                                    drlNhomlienket.SelectedValue = pageType.MTP;
                                }
                                else if (curLink.pagType == int.Parse(pageType.GP))
                                {
                                    pnpro.Visible = true;
                                    drlCategory.SelectedValue = curLink.pagId.ToString();
                                    drlCategory.Visible = true;
                                    drlNhomlienket.SelectedValue = pageType.MP;
                                }
                                else if (curLink.pagType == int.Parse(pageType.MP))
                                {
                                    pnpro.Visible = true;
                                    tbPage tbp = db.tbPages.FirstOrDefault(s => s.pagTagName == strLink && s.pagType == 100);
                                    if (tbp != null) { drlCategory.SelectedValue = tbp.pagId.ToString(); } else { drlCategory.SelectedValue = "0"; }

                                    drlCategory.Visible = true;
                                    drlNhomlienket.SelectedValue = pageType.MP;
                                }
                                else if (curLink.pagType == int.Parse(pageType.GL))
                                {
                                    pnLibrary.Visible = true;
                                    drlLibrary.SelectedValue = curLink.pagId.ToString();
                                    drlLibrary.Visible = true;
                                    drlNhomlienket.SelectedValue = pageType.LP;
                                }

                            }
                            else if (list[0].pagLink == "san-pham.html")
                            {
                                drlNhomlienket.SelectedValue = pageType.MP;
                            }
                            else if (list[0].pagLink == "san-pham-moi.html")
                            {
                                drlNhomlienket.SelectedValue = pageType.MPn;
                            }
                            else if (list[0].pagLink == "tin-tuc.html")
                            {
                                drlNhomlienket.SelectedValue = pageType.MN;
                            }
                            else if (list[0].pagLink == "thu-vien.html")
                            {
                                drlNhomlienket.SelectedValue = pageType.LP;

                            }
                            else
                            {
                                drlNhomlienket.SelectedValue = "0";
                                pnNhom.Visible = false;
                                pnpro.Visible = false;
                                pnLibrary.Visible = false;
                            }
                        }
                        else
                        {
                            pnNhom.Visible = false;
                            drlgrnId.Visible = false;
                            drlNhomlienket.SelectedValue = pageType.HP;
                        }
                        pnKieulienket.Visible = true;
                    }
                    else if (list[0].pagType == Convert.ToInt32(pageType.MU))
                    {
                        txtUrl.Text = list[0].pagLink;
                        pnUrl.Visible = true;
                    }
                    else
                    {
                        pnNoidung.Visible = true;
                    }

                    drlVitri.SelectedValue = list[0].pagPosition.ToString();

                    pnlErr.Visible = false;
                    ltrErr.Text = "";
                    pnlListForder.Visible = false;
                    pnlAddForder.Visible = true;
                    break;
                case "Add":
                    List<tbPageDATA> lists = tbPageDB.tbPage_GetByID(strID);
                    pnlListForder.Visible = false;
                    pnlAddForder.Visible = true;
                    Resetcontrol();
                    Resetcontrol();
                    hidLevel.Value = lists[0].paglevel;
                    BindForder();
                    BindForderAdd();
					drlVitri.SelectedValue = lists[0].pagPosition;
                    drlChuyenmuc.SelectedValue = lists[0].paglevel;
                    break;
            }
        }

        protected void drlKieutrang_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (drlKieutrang.SelectedValue == pageType.MC)
            {
                pnNoidung.Visible = true;
                pnKieulienket.Visible = false;
                drlNhomlienket.SelectedValue = "0";
                drlCategory.SelectedValue = "0";
                pnUrl.Visible = false;
                pnpro.Visible = false;
                pnMan.Visible = false;
            }
            else if (drlKieutrang.SelectedValue == pageType.ML)
            {
                pnNoidung.Visible = false;
                pnKieulienket.Visible = true;
                pnUrl.Visible = false;
                pnpro.Visible = false;
                pnMan.Visible = false;
            }
            else if (drlKieutrang.SelectedValue == pageType.MU)
            {
                pnNoidung.Visible = false;
                pnKieulienket.Visible = false;
                pnUrl.Visible = true;
                pnpro.Visible = false;
                drlNhomlienket.SelectedValue = "0";
                drlCategory.SelectedValue = "0";
                pnMan.Visible = false;
            }
            else
            {
                pnNoidung.Visible = false;
                pnKieulienket.Visible = false;
                pnUrl.Visible = false;
                pnpro.Visible = false;
                drlNhomlienket.SelectedValue = "0";
                drlCategory.SelectedValue = "0";
                pnMan.Visible = false;
            }
        }

        protected void drlNhomlienket_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (drlNhomlienket.SelectedValue == pageType.MN)
            {
                pnNhom.Visible = true;
                drlgrnId.Visible = true;
                ViewCatePage();
                pnpro.Visible = false;
                pnLibrary.Visible = false;
                drlCategory.Visible = false;
                pnMan.Visible = false;
                pnTourCategory.Visible = false;
            }
            else if (drlNhomlienket.SelectedValue == pageType.MP)
            {
                pnpro.Visible = true;
                drlCategory.Visible = true;
                Viewcategory();
                pnNhom.Visible = false;
                pnLibrary.Visible = false;
                drlgrnId.Visible = false;
                pnMan.Visible = false;
                pnTourCategory.Visible = false;
            }
            else if (drlNhomlienket.SelectedValue == pageType.MM)
            {
                pnpro.Visible = false;                                
                pnNhom.Visible = false;
                pnLibrary.Visible = false;
                drlgrnId.Visible = false;
                pnMan.Visible = true;
                ddlMan.Visible = true;
                ViewListMan();
                pnTourCategory.Visible = false;
            }
            else if (drlNhomlienket.SelectedValue == pageType.LP)
            {
                drlLibrary.Visible = true;
                ViewLibrary();
                pnNhom.Visible = false;
                pnpro.Visible = false;
                pnLibrary.Visible = true;
                drlgrnId.Visible = false;
                pnMan.Visible = false;
                pnTourCategory.Visible = false;
            }
            else if (drlNhomlienket.SelectedValue == pageType.MenuTourCategory)
            {
                pnLibrary.Visible = false;
                pnNhom.Visible = false;
                pnpro.Visible = false;
                drlgrnId.Visible = false;
                drlCategory.Visible = false;
                pnMan.Visible = false;
                pnTourCategory.Visible = true;
                ddlTourCategory.Visible = true;
                LoadTourCategory();
            }
            else if (drlNhomlienket.SelectedValue == pageType.MTT)
            {
                pnLibrary.Visible = false;
                pnNhom.Visible = false;
                pnpro.Visible = false;
                drlgrnId.Visible = false;
                drlCategory.Visible = false;
                pnMan.Visible = false;
                pnTourCategory.Visible = true;
                ddlTourCategory.Visible = true;
                LoadTourCategory();
            }
            else if (drlNhomlienket.SelectedValue == pageType.MTP)
            {
                pnLibrary.Visible = false;
                pnNhom.Visible = false;
                pnpro.Visible = false;
                drlgrnId.Visible = false;
                drlCategory.Visible = false;
                pnMan.Visible = false;
                pnTourCategory.Visible = true;
                ddlTourCategory.Visible = true;
                LoadTourCategory();
            }
            else
            {
                pnLibrary.Visible = false;
                pnNhom.Visible = false;
                pnpro.Visible = false;
                drlgrnId.Visible = false;
                drlCategory.Visible = false;
                pnMan.Visible = false;
                pnTourCategory.Visible = false;
            }
        }        

        private void LoadGroupLink()
        {
            drlNhomlienket.Items.Add(new ListItem("Chọn nhóm liên kết", "0"));
            drlNhomlienket.Items.Add(new ListItem("Trang chủ", pageType.HP));
            drlNhomlienket.Items.Add(new ListItem("Tin tức", pageType.MN));
            drlNhomlienket.Items.Add(new ListItem("Nhóm tour", pageType.MenuTourCategory));
            drlNhomlienket.Items.Add(new ListItem("Chủ đề tour", pageType.MTT));
            drlNhomlienket.Items.Add(new ListItem("Địa điểm tour", pageType.MTP));
            if (GlobalClass.pageProducts)
            {
                drlNhomlienket.Items.Add(new ListItem("Hãng sản xuất", pageType.MM));
                drlNhomlienket.Items.Add(new ListItem("Sản phẩm", pageType.MP));
                drlNhomlienket.Items.Add(new ListItem("Sản phẩm mới", pageType.MPn));

                drlNhomlienket.Items.Add(new ListItem("Sản phẩm nổi bật", pageType.MPnb));
                drlNhomlienket.Items.Add(new ListItem("Sản phẩm bán chạy", pageType.MPbc));
                drlNhomlienket.Items.Add(new ListItem("Sản phẩm khuyến mại", pageType.MPkm));

                drlNhomlienket.Items.Add(new ListItem("Giỏ hàng", pageType.PC));
            }
            if (GlobalClass.showLibrary)
            {
                drlNhomlienket.Items.Add(new ListItem("Thư viện", pageType.LP));
            }
            drlNhomlienket.Items.Add(new ListItem("Liên hệ", pageType.CP));

            drlNhomlienket.Items.Add(new ListItem("Đăng ký thành viên", pageType.RG));
            drlNhomlienket.Items.Add(new ListItem("Quên mật khẩu", pageType.FP));
            drlNhomlienket.Items.Add(new ListItem("Đổi mật khẩu", pageType.CPw));
            drlNhomlienket.Items.Add(new ListItem("Thông tin tài khoản", pageType.UI));

            drlNhomlienket.Items.Add(new ListItem("Đơn hàng", pageType.LO));
            drlNhomlienket.Items.Add(new ListItem("Thanh toán", pageType.PM));

        }

        void ViewCatePage()
        {
            string str = "";
            List<tbPageDATA> list = tbPageDB.tbPage_GetByAll(lang);
            list = list.Where(s => s.pagType == pageType.GN).ToList();
            drlgrnId.Items.Clear();
            drlgrnId.Items.Add(new ListItem("- Chọn nhóm chuyên mục -", "0"));
            for (int i = 0; i < list.Count; i++)
            {
                str = "";
                for (int j = 1; j < list[i].paglevel.Length / 5; j++)
                {
                    str = str + "---";
                }
                drlgrnId.Items.Add(new ListItem(str + " " + list[i].pagName, list[i].pagId));
            }
            list.Clear();
            list = null;
        }

        void LoadTourCategory()
        {            
            List<tbPageDATA> list = tbPageDB.tbPage_GetByAll(lang);
            if (drlNhomlienket.SelectedValue == pageType.MTT)
            {
                list = list.Where(s => s.pagType == pageType.TourTopic).ToList();
            }
            else if (drlNhomlienket.SelectedValue == pageType.MTP)
            {
                list = list.Where(s => s.pagType == pageType.TourPlace).ToList();
            }
            else
            {
                list = list.Where(s => s.pagType == pageType.TourCategory).ToList();
            }
            ddlTourCategory.Items.Clear();
            ddlTourCategory.Items.Add(new ListItem("- Chọn nhóm -", "0"));

            for (int i = 0; i < list.Count; i++)
            {
                string str = "";
                for (int j = 1; j < list[i].paglevel.Length / 5; j++)
                {
                    str = str + "---";
                }
                ddlTourCategory.Items.Add(new ListItem(str + " " + list[i].pagName, list[i].pagId));
            }
            list.Clear();
            list = null;
        }

        void Viewcategory()
        {
            List<tbPageDATA> list = tbPageDB.tbPage_GetByAll(lang);
            list = list.Where(s => s.pagType == pageType.GP).ToList();
            drlCategory.Items.Clear();
            drlCategory.Items.Add(new ListItem("- Chọn chuyên mục -", "0"));
            for (int i = 0; i < list.Count; i++)
            {
                string str = "";
                for (int j = 1; j < list[i].paglevel.Length / 5; j++)
                {
                    str = str + "---";
                }
                drlCategory.Items.Add(new ListItem(str + " " + list[i].pagName, list[i].pagId));
            }
            list.Clear();
            list = null;
        }

        protected void ViewListMan()
        {
            List<tbPageDATA> list = tbPageDB.tbPage_GetByAll(lang);
            list = list.Where(s => s.pagType == pageType.GM).ToList();
            ddlMan.Items.Clear();
            ddlMan.Items.Add(new ListItem("- Chọn hãng sản xuất -", "0"));
            for (int i = 0; i < list.Count; i++)
            {
                string str = "";
                for (int j = 1; j < list[i].paglevel.Length / 5; j++)
                {
                    str = str + "---";
                }
                ddlMan.Items.Add(new ListItem(str + " " + list[i].pagName, list[i].pagId));
            }
            list.Clear();
            list = null;
        }

        void ViewLibrary()
        {
            List<tbPageDATA> librarys = tbPageDB.tbPage_GetByAll(lang);
            librarys = librarys.Where(s => s.pagType == pageType.GL).OrderBy(s => s.paglevel).ToList();
            drlLibrary.Items.Clear();
            drlLibrary.Items.Add(new ListItem("- Chọn chuyên mục -", "0"));
            foreach (var lib in librarys)
            {
                string str = "";
                for (int j = 1; j < lib.paglevel.Length / 5; j++)
                {
                    str = str + "---";
                }
                drlLibrary.Items.Add(new ListItem(str + " " + lib.pagName, lib.pagId.ToString()));
            }
            librarys.Clear();
            librarys = null;
        }

        void Resetcontrol()
        {
            hidID.Value = "";
            hidLevel.Value = "";
            drlgrnId.SelectedValue = "0";
            txtTenmenu.Text = "";
            drlKieutrang.SelectedValue = "0";
            fckNoidung.Text = "";
            txtUrl.Text = "";
            txtImage.Text = "";
            txtTieude.Text = "";
            txtDesscription.Text = "";
            txtKeyword.Text = "";
            drlVitri.SelectedValue = "0";
            txtThuTu.Text = "1";
            pnKieulienket.Visible = false;
            drlNhomlienket.SelectedValue = "0";
            pnNoidung.Visible = false;
            pnNhom.Visible = false;
            drlgrnId.Visible = false;
            pnKieulienket.Visible = false;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (Validation() == true)
            {
                string spagActive = "";
                if (chkKichhoat.Checked == true) { spagActive = "1"; } else { spagActive = "0"; }
                string img_path = "";
                if (txtImage.Text != "")
                {
                    img_path = txtImage.Text;
                }
                string spagLink = "";
                string strID = hidID.Value;
                string sgrnlevel = hidLevel.Value;

                if (strID.Length == 0)
                {
                    tbPageDATA ObjtbPageDATA = new tbPageDATA();
                    ObjtbPageDATA.pagName = txtTenmenu.Text.Trim();
                    ObjtbPageDATA.pagImage = img_path;
                    ObjtbPageDATA.pagDetail = fckNoidung.Text;
                    ObjtbPageDATA.paglevel = drlChuyenmuc.SelectedValue + "00000";
                    ObjtbPageDATA.pagTitle = txtTieude.Text;
                    ObjtbPageDATA.pagDescription = txtDesscription.Text;
                    ObjtbPageDATA.pagKeyword = txtKeyword.Text;

                    ObjtbPageDATA.pagType = (drlNhomlienket.SelectedValue != "0") ? drlNhomlienket.SelectedValue : drlKieutrang.SelectedValue;

                    ObjtbPageDATA.pagTarget = drlKieuxuathien.SelectedValue;
                    ObjtbPageDATA.pagPosition = drlVitri.SelectedValue;
                    ObjtbPageDATA.pagOrd = txtThuTu.Text;
                    ObjtbPageDATA.pagLink = txtUrl.Text;
                    ObjtbPageDATA.pagActive = spagActive;
                    ObjtbPageDATA.catId = "0";
                    ObjtbPageDATA.grnId = "0";
                    ObjtbPageDATA.pagLang = lang;
                    ObjtbPageDATA.pagLink = "";
                    ObjtbPageDATA.pagTagName = "";

                    ObjtbPageDATA.pagLink = "";
                    ObjtbPageDATA.check1 = "";
                    ObjtbPageDATA.check2 = "";
                    ObjtbPageDATA.check3 = "";
                    ObjtbPageDATA.check4 = "";
                    ObjtbPageDATA.check5 = "";
                    string tagName = MyWeb.Common.StringClass.NameToTag(txtTenmenu.Text);
                    if (tbPageDB.tbPage_Add(ObjtbPageDATA))
                    {
                        #region Check
                        List<tbPageDATA> curItem = tbPageDB.tbPage_GetByMax();
                        var hasTagName = db.tbPages.Where(s => s.pagTagName == tagName).FirstOrDefault();
                        curItem[0].pagId = curItem[0].pagId;
                        curItem[0].pagTagName = hasTagName != null ? tagName + "_" + curItem[0].pagId : tagName;

                        if (drlKieutrang.SelectedValue == pageType.ML)
                        {
                            if (drlNhomlienket.SelectedValue == pageType.MN)
                            {
                                if (drlgrnId.SelectedValue != "0")
                                {
                                    var grn = db.tbPages.Where(s => s.pagId == int.Parse(drlgrnId.SelectedValue)).First();
                                    spagLink = grn.pagTagName + ".html";
                                }
                                else
                                {
                                    spagLink = "tin-tuc.html";
                                }
                            }
                            else if (drlNhomlienket.SelectedValue == pageType.MP)
                            {

                                if (drlCategory.SelectedValue != "0")
                                {
                                    var grn = db.tbPages.Where(s => s.pagId == int.Parse(drlCategory.SelectedValue)).First();
                                    spagLink = grn.pagTagName + ".html";
                                }
                                else
                                {
                                    spagLink = "san-pham.html";
                                }

                            }
                            else if (drlNhomlienket.SelectedValue == pageType.MM)
                            {
                                if (ddlMan.SelectedValue != "0")
                                {
                                    var man = db.tbPages.FirstOrDefault(s => s.pagId == int.Parse(ddlMan.SelectedValue));
                                    spagLink = man.pagTagName + ".html";
                                }
                            }
                            else if (drlNhomlienket.SelectedValue == pageType.MenuTourCategory 
                                || drlNhomlienket.SelectedValue == pageType.MTT
                                || drlNhomlienket.SelectedValue == pageType.MTP)
                            {
                                if (ddlTourCategory.SelectedValue != "0")
                                {
                                    var tourCat =
                                        db.tbPages.FirstOrDefault(
                                            s => s.pagId == int.Parse(ddlTourCategory.SelectedValue));
                                    spagLink = tourCat.pagTagName + ".html";
                                }
                                else
                                {
                                    spagLink = "danh-sach-nhom-tour.html";
                                }
                            }
                            else if (drlNhomlienket.SelectedValue == pageType.LP)
                            {
                                if (drlLibrary.SelectedValue != "0")
                                {
                                    var grn = db.tbPages.Where(s => s.pagId == int.Parse(drlLibrary.SelectedValue)).First();
                                    spagLink = grn.pagTagName + ".html";
                                }
                                else
                                {
                                    spagLink = "thu-vien.html";
                                }
                            }
                            else if (drlNhomlienket.SelectedValue == pageType.CP 
                                || drlNhomlienket.SelectedValue == pageType.PC)
                            {
                                spagLink = curItem[0].pagTagName + ".html";
                            }
                            else if (drlNhomlienket.SelectedValue == pageType.MPn)
                            {
                                spagLink = "san-pham-moi.html";
                            }
                            else if (drlNhomlienket.SelectedValue == pageType.MPnb)
                            {
                                spagLink = "san-pham-noi-bat.html";
                            }
                            else if (drlNhomlienket.SelectedValue == pageType.MPbc)
                            {
                                spagLink = "san-pham-ban-chay.html";
                            }
                            else if (drlNhomlienket.SelectedValue == pageType.MPkm)
                            {
                                spagLink = "san-pham-khuyen-mai.html";
                            }
                            else { spagLink = "/"; }
                        }
                        else if (drlKieutrang.SelectedValue == pageType.MU) { spagLink = txtUrl.Text; }
                        else { spagLink = spagLink = curItem[0].pagTagName + ".html"; }

                        curItem[0].pagLink = spagLink;
                        tbPageDB.tbPage_Update(curItem[0]);
                        #endregion

                        pnlErr.Visible = true;
                        ltrErr.Text = "Thêm mới trình đơn thành công !";
                        BindData();
                        Resetcontrol();
                        hidLevel.Value = "";
                        hidParId.Value = "";
                        pnlListForder.Visible = true;
                        pnlAddForder.Visible = false;
                    }
                }
                else
                {

                    List<tbPageDATA> lst = tbPageDB.tbPage_GetByID(strID);
                    if (lst.Count > 0)
                    {
                        lst[0].pagId = strID;
                        lst[0].pagName = txtTenmenu.Text.Trim();
                        lst[0].pagImage = img_path;
                        lst[0].pagDetail = fckNoidung.Text;
                        lst[0].paglevel = drlChuyenmuc.SelectedValue + "00000";
                        lst[0].pagTitle = txtTieude.Text;
                        lst[0].pagDescription = txtDesscription.Text;
                        lst[0].pagKeyword = txtKeyword.Text;
                        lst[0].pagType = (drlNhomlienket.SelectedValue != "0") ? drlNhomlienket.SelectedValue : drlKieutrang.SelectedValue;
                        lst[0].pagTarget = drlKieuxuathien.SelectedValue;
                        lst[0].pagPosition = drlVitri.SelectedValue;
                        lst[0].pagOrd = txtThuTu.Text;
                        lst[0].pagActive = spagActive;
                        lst[0].catId = "0";
                        lst[0].grnId = "0";
                        lst[0].pagLang = lang;
                        lst[0].check1 = "";
                        lst[0].check2 = "";
                        lst[0].check3 = "";
                        lst[0].check4 = "";
                        lst[0].check5 = "";

                        var hasTagName = db.tbPages.Where(s => s.pagTagName == MyWeb.Common.StringClass.NameToTag(txtTenmenu.Text)).FirstOrDefault();
                        //lst[0].pagTagName = hasTagName != null ? MyWeb.Common.StringClass.NameToTag(txtTenmenu.Text) + "-" + lst[0].pagId : MyWeb.Common.StringClass.NameToTag(txtTenmenu.Text);
                        if (hasTagName != null && hasTagName.pagId != int.Parse(strID))
                        {
                            lst[0].pagTagName = MyWeb.Common.StringClass.NameToTag(txtTenmenu.Text) + "-" + lst[0].pagId;
                        }
                        else
                        {
                            lst[0].pagTagName = MyWeb.Common.StringClass.NameToTag(txtTenmenu.Text);
                        }
                        
                        if (drlKieutrang.SelectedValue == pageType.ML)
                        {
                            if (drlNhomlienket.SelectedValue == pageType.MN)
                            {
                                if (drlgrnId.SelectedValue != "0")
                                {
                                    var grn = db.tbPages.Where(s => s.pagId == int.Parse(drlgrnId.SelectedValue)).First();
                                    spagLink = grn.pagTagName + ".html";
                                }
                                else
                                {
                                    spagLink = "tin-tuc.html";
                                }

                            }
                            else if (drlNhomlienket.SelectedValue == pageType.MP)
                            {
                                if (drlCategory.SelectedValue != "0")
                                {
                                    var grn = db.tbPages.Where(s => s.pagId == int.Parse(drlCategory.SelectedValue)).First();
                                    spagLink = grn.pagTagName + ".html";

                                }
                                else
                                {
                                    spagLink = "san-pham.html";
                                }
                            }
                            else if (drlNhomlienket.SelectedValue == pageType.MM)
                            {
                                if (ddlMan.SelectedValue != "0")
                                {
                                    var man = db.tbPages.FirstOrDefault(s => s.pagId == int.Parse(ddlMan.SelectedValue));
                                    spagLink = man.pagTagName + ".html";
                                }
                            }
                            else if (drlNhomlienket.SelectedValue == pageType.MPn)
                            {

                                spagLink = "san-pham-moi.html";

                            }
                            else if (drlNhomlienket.SelectedValue == pageType.MPnb)
                            {
                                spagLink = "san-pham-noi-bat.html";
                            }
                            else if (drlNhomlienket.SelectedValue == pageType.MPbc)
                            {
                                spagLink = "san-pham-ban-chay.html";
                            }
                            else if (drlNhomlienket.SelectedValue == pageType.MPkm)
                            {
                                spagLink = "san-pham-khuyen-mai.html";
                            }
                            else if (drlNhomlienket.SelectedValue == pageType.LP)
                            {
                                if (drlLibrary.SelectedValue != "0")
                                {
                                    var grn = db.tbPages.Where(s => s.pagId == int.Parse(drlLibrary.SelectedValue)).First();
                                    spagLink = grn.pagTagName + ".html";
                                }
                                else
                                {
                                    spagLink = "thu-vien.html";
                                }
                            }
                            else if (drlNhomlienket.SelectedValue == pageType.CP 
                                || drlNhomlienket.SelectedValue == pageType.PC)
                            {
                                spagLink = lst[0].pagTagName + ".html";
                            }
                            else if (drlNhomlienket.SelectedValue == pageType.MenuTourCategory
                               || drlNhomlienket.SelectedValue == pageType.MTT
                               || drlNhomlienket.SelectedValue == pageType.MTP)
                            {
                                var catLink = db.tbPages.FirstOrDefault(x => x.pagId == int.Parse(ddlTourCategory.SelectedValue));
                                if (catLink != null)
                                    spagLink = catLink.pagTagName + ".html";
                            }
                            else { spagLink = "/"; }
                        }
                        else if (drlKieutrang.SelectedValue == pageType.MU) { spagLink = txtUrl.Text; }
                        else { spagLink = lst[0].pagTagName + ".html"; }
                        lst[0].pagLink = spagLink;

                        if (tbPageDB.tbPage_Update(lst[0]))
                        {
                            pnlErr.Visible = true;
                            ltrErr.Text = "Cập nhật trình đơn thành công !";
                            Resetcontrol();
                            BindData();
                            hidID.Value = "";
                            hidLevel.Value = "";
                            hidParId.Value = "";
                            pnlListForder.Visible = true;
                            pnlAddForder.Visible = false;
                        }


                    }
                }
            }
        }

        protected bool Validation()
        {
            if (txtTenmenu.Text == "") { ltrErr2.Text = "Chưa nhập tên menu !"; pnlErr2.Visible = true; return false; }
            if (drlKieutrang.SelectedValue == "0") { ltrErr2.Text = "Chưa chọn kiểu trang !"; pnlErr2.Visible = true; return false; }
            if (drlKieutrang.SelectedValue == "2" && fckNoidung.Text == "") { ltrErr2.Text = "Chưa nhập nội dung trang !"; pnlErr2.Visible = true; return false; }
            if (drlKieutrang.SelectedValue == "3" && txtUrl.Text == "") { ltrErr2.Text = "Chưa nhập đường dẫn Url liên kết !"; pnlErr2.Visible = true; return false; }
            if (drlKieuxuathien.SelectedValue == "0") { ltrErr2.Text = "Chưa chọn kiểu xuất hiện trang !"; pnlErr2.Visible = true; return false; }
            if (drlVitri.SelectedValue == "0") { ltrErr2.Text = "Chưa chọn vị trí menu !"; pnlErr2.Visible = true; return false; }
            return true;
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = true;
            pnlAddForder.Visible = false;
            BindData();
            Session["insert"] = "false";
            hidLevel.Value = "";
            hidParId.Value = "";
            hidID.Value = "";
        }

        public static string ShowActiveClass(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "btn btn-primary btn-xs" : "btn btn-danger btn-xs";
        }

        protected void drlVitriFind_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected void drlVitri_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindForderAdd();
        }
    }
}