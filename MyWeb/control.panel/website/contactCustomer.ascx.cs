﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.news
{
    public partial class contactCustomer : System.Web.UI.UserControl
    {
        string _Admin = "";
        private string lang = "vi";
        int pageSize = 15;
        string userid = "";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            if (Session["admin"] != null) { _Admin = Session["admin"].ToString(); } if (Session["admin"] != null) { _Admin = Session["admin"].ToString(); }

            if (_Admin == "0" || _Admin == "100" || _Admin == "1")
            {

            }

            if (Session["uid"] != null) userid = Session["uid"].ToString();

            if (Request["pages"] != null && !Request["pages"].Equals(""))
            {
                pageSize = int.Parse(Request["pages"].ToString());
            }
            if (!IsPostBack)
            {
                BindData();
            }
        }

        void BindData()
        {
            List<tbReply> _listForder = db.tbReplies.Where(x => x.HoTen != "").ToList();
            if (txtSearch.Text != "")
            {
                _listForder = _listForder.Where(s => s.HoTen.ToLower().Contains(txtSearch.Text.ToLower())).ToList();
            }

            if (_listForder.Count() > 0)
            {
                recordCount = _listForder.Count();
                #region Statistic
                int fResult = currentPage * pageSize + 1;
                int tResult = (currentPage + 1) * pageSize;
                tResult = tResult > recordCount ? recordCount : tResult;
                ltrStatistic.Text = "Hiển thị kết quả từ " + fResult + " - " + tResult + " trong tổng số " + recordCount + " kết quả";
                #endregion

                var result = _listForder.Skip(currentPage * pageSize).Take(pageSize);
                rptFolderList.DataSource = result;
                rptFolderList.DataBind();
                BindPaging();
            }
            else
            {
                rptFolderList.DataSource = null;
                rptFolderList.DataBind();
            }
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = false;
            pnlAddForder.Visible = true;
            Resetcontrol();
        }

        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
            try
            {

                RepeaterItem item = default(RepeaterItem);
                for (int i = 0; i < rptFolderList.Items.Count; i++)
                {
                    item = rptFolderList.Items[i];
                    if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                    {
                        if (((CheckBox)item.FindControl("chkBox")).Checked)
                        {
                            HiddenField hidID = (HiddenField)item.FindControl("hidCatID");
                            tbReply itemDel = db.tbReplies.FirstOrDefault(x => x.cusId == int.Parse(hidID.Value));
                            db.tbReplies.DeleteOnSubmit(itemDel);
                        }
                    }
                }
                db.SubmitChanges();
                pnlErr.Visible = true;
                ltrErr.Text = "Xóa thành công nhóm chuyên mục !";
                BindData();
                chkSelectAll.Checked = false;
            }
            catch
            {
                pnlErr.Visible = true;
                ltrErr.Text = "Xóa không thành công !";
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }

        protected void drlForder_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected void txtOrder_TextChanged(object sender, EventArgs e)
        {
            TextBox txtOrder = (TextBox)sender;
            var blbID = (Label)txtOrder.FindControl("lblID");
            List<tbPageDATA> list = tbPageDB.tbPage_GetByID(blbID.Text);
            if (list.Count > 0)
            {
                list[0].pagOrd = txtOrder.Text;
                tbPageDB.tbPage_Update(list[0]);
            }
            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Cập nhật thứ tự thành công !";
        }

        public static string ShowActive(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "<i class=\"fa fa-check\"></i>" : "<i class=\"fa fa-times\"></i>";
        }

        #region Page

        private int currentPage { get { return ViewState["currentPage"] != null ? int.Parse(ViewState["currentPage"].ToString()) : 0; } set { ViewState["currentPage"] = value; } }
        private int recordCount { get { return ViewState["recordCount"] != null ? int.Parse(ViewState["recordCount"].ToString()) : 0; } set { ViewState["recordCount"] = value; } }
        private int pageCount { get { double iCount = (double)((decimal)recordCount / (decimal)pageSize); return (int)Math.Ceiling(iCount); } }

        private void BindPaging()
        {
            int icurPage = currentPage + 1;
            int ipCount = pageCount;
            if (ipCount >= 1)
            {
                rptNumberPage.Visible = true;
                int PageShow = ipCount > 5 ? 5 : ipCount;
                int FromPage;
                int ToPage;
                DataTable dt = new DataTable();
                dt.Columns.Add("PageIndex");
                dt.Columns.Add("PageText");
                FromPage = icurPage > PageShow ? icurPage - PageShow : 1;
                ToPage = (ipCount - icurPage > PageShow) ? icurPage + PageShow : ipCount;
                if (icurPage - 10 > 0) dt.Rows.Add(icurPage - 9, icurPage - 10);
                for (int i = FromPage; i <= ToPage; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = i - 1;
                    dr[1] = i;
                    dt.Rows.Add(dr);
                }
                if (icurPage + 10 <= ipCount) dt.Rows.Add(icurPage + 9, icurPage + 10);
                rptNumberPage.DataSource = dt;
                rptNumberPage.DataBind();
            }
        }

        protected void btnPage_Click(object sender, EventArgs e)
        {
            if (((LinkButton)sender).ID == "btnPrevious")
            {
                if (currentPage > 0) currentPage = currentPage - 1;
            }
            else if (((LinkButton)sender).ID == "btnNext")
            {
                if (currentPage < pageCount - 1) currentPage = currentPage + 1;
            }
            BindData();
        }

        protected void rptNumberPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("page"))
            {
                currentPage = Convert.ToInt32(e.CommandArgument.ToString());
                BindData();
            }
        }

        protected void rptNumberPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lnkPage = (LinkButton)e.Item.FindControl("btn");
            Literal _ltrPage = (Literal)e.Item.FindControl("ltrLiPage");
            if (lnkPage.CommandArgument.ToString() == currentPage.ToString())
            {
                lnkPage.Enabled = false;
                _ltrPage.Text = "<li class=\"paginate_button active\">";
            }
            else
            {
                _ltrPage.Text = "<li class=\"paginate_button\">";
            }
        }

        #endregion

        #region[Resetcontrol]
        void Resetcontrol()
        {
            txtHoTen.Text = "";
            txtImage.Text = "";
            txtChucDanh.Text = "";
            txtNoiDung.Text = "";
            chkKichhoat.Checked = false;
            txtImage.Text = "";
            hidID.Value = "";
        }
        #endregion

        protected void rptFolderList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string strID = e.CommandArgument.ToString();
            switch (e.CommandName.ToString())
            {
                case "Active":
                    var item = db.tbReplies.FirstOrDefault(x => x.cusId == int.Parse(strID));
                    if (item != null)
                    {
                        item.cusId = int.Parse(strID);
                        item.cusActive = item.cusActive == 1 ? 0 : 1;
                        db.SubmitChanges();
                    }
                    BindData();
                    pnlErr.Visible = true;
                    ltrErr.Text = "Cập nhật trạng thái thành công !";
                    break;
                case "Del":
                    var itemDel = db.tbReplies.FirstOrDefault(x => x.cusId == int.Parse(strID));
                    if (itemDel != null)
                    {
                        db.tbReplies.DeleteOnSubmit(itemDel);
                        db.SubmitChanges();
                    }
                    pnlErr.Visible = true;
                    ltrErr.Text = "Xóa thành công !";
                    BindData();
                    break;
                case "Edit":
                    var itemEdit = db.tbReplies.FirstOrDefault(x => x.cusId == int.Parse(strID));
                    hidID.Value = strID;
                    txtHoTen.Text = itemEdit.HoTen;
                    txtChucDanh.Text = itemEdit.ChucDanh;
                    txtNoiDung.Text = itemEdit.NoiDung;
                    txtImage.Text = itemEdit.cusImage;
                    if (itemEdit.cusImage.Length > 0)
                    {
                        txtImage.Text = itemEdit.cusImage.ToString();
                    }
                    txtThuTu.Text = itemEdit.cusOrd.ToString();
                    chkKichhoat.Checked = itemEdit.cusActive == 1 ? true : false;

                    pnlErr.Visible = false;
                    ltrErr.Text = "";

                    pnlListForder.Visible = false;
                    pnlAddForder.Visible = true;
                    break;
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (Validation() == true)
                {
                    string strID = hidID.Value;
                    if (strID.Length == 0)
                    {
                        tbReply item = new tbReply();
                        item.HoTen = txtHoTen.Text.Trim();
                        item.ChucDanh = txtChucDanh.Text;
                        item.NoiDung = txtNoiDung.Text;
                        item.cusImage = txtImage.Text;
                        item.cusOrd = int.Parse(txtThuTu.Text);
                        item.cusActive = (chkKichhoat.Checked == true) ? 1 : 0;
                        db.tbReplies.InsertOnSubmit(item);
                        db.SubmitChanges();
                        pnlErr.Visible = true;
                        ltrErr.Text = "Thêm mới thành công !";
                        BindData();
                        Resetcontrol();
                        pnlListForder.Visible = true;
                        pnlAddForder.Visible = false;
                    }
                    else
                    {
                        tbReply itemUpdate = db.tbReplies.FirstOrDefault(x => x.cusId == int.Parse(strID));
                        if (itemUpdate != null)
                        {
                            itemUpdate.HoTen = txtHoTen.Text.Trim();
                            itemUpdate.ChucDanh = txtChucDanh.Text;
                            itemUpdate.NoiDung = txtNoiDung.Text;
                            itemUpdate.cusImage = txtImage.Text;
                            itemUpdate.cusOrd = int.Parse(txtThuTu.Text);
                            itemUpdate.cusActive = (chkKichhoat.Checked == true) ? 1 : 0;
                            db.SubmitChanges();

                            pnlErr.Visible = true;
                            ltrErr.Text = "Cập nhật thành công !";
                            Resetcontrol();
                            BindData();
                            hidID.Value = "";
                            pnlListForder.Visible = true;
                            pnlAddForder.Visible = false;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                pnlErr.Visible = true;
                ltrErr.Text = "Lỗi: " + ex.Message;
            }
        }

        protected bool Validation()
        {
            if (txtHoTen.Text == "") { ltrErr2.Text = "Chưa nhập họ tên !!"; txtHoTen.Focus(); pnlErr2.Visible = true; return false; }
            return true;
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = true;
            pnlAddForder.Visible = false;
            BindData();
            Session["insert"] = "false";
            hidID.Value = "";
        }

        public static string ShowActiveClass(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "btn btn-primary btn-xs" : "btn btn-danger btn-xs";
        }

        public static string BindImages(string strPart)
        {
            string str = "";
            try
            {
                str = "<img style=\"width:50px;\" src=\"" + strPart.Split(',')[0].Replace("uploads", "uploads/_thumbs") + "\" />";
            }
            catch (Exception)
            { }
            return str;
        }
    }
}