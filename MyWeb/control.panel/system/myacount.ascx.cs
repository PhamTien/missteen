﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.system
{
    public partial class myacount : System.Web.UI.UserControl
    {
        string lang = "vi";
        string userid = "";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            if (Session["uid"] != null) userid = Session["uid"].ToString();
            if (!IsPostBack)
            {
                showAcount();
            }
        }

        void showAcount()
        {
            tbUser _user = db.tbUsers.FirstOrDefault(s => s.useId == int.Parse(userid));
            if (_user != null)
            {
                txtName.Text = _user.useName;
                txtUserName.Text = _user.useUid;
                txtUserName.ReadOnly = true;
                txtEmail.Text = _user.useMail;
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            tbUser _user = db.tbUsers.FirstOrDefault(s => s.useId == int.Parse(userid));
            if (_user != null)
            {
                _user.useName = txtName.Text;
                _user.useMail= txtEmail.Text;
                db.SubmitChanges();
                pnlErr.Visible = true;
                ltrErr.Text = "Cập nhật cấu hình thành công !";
                showAcount();
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            showAcount();
        }
    }
}