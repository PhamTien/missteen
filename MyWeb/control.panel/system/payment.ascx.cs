﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.system
{
    public partial class payment : System.Web.UI.UserControl
    {
        string lang = "vi";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            if (!IsPostBack)
            {
                showConfig();
            }
        }

        void showConfig()
        {
            var list = new List<tbConfig>();
            list = db.tbConfigs.Where(s => s.conLang == lang).ToList();
            if (list.Count > 0)
            {
                string[] viewPayment = list[0].viewPayment.Split(Convert.ToChar(","));
                txtId.Value = list[0].conid + "";
                chkbaokim.Checked = viewPayment[0].Contains("1") ? true : false;
                txtemaibaokim.Text = viewPayment[1];
                txtmerchantsitebk.Text = viewPayment[2];
                txtmkbaokim.Text = viewPayment[3];
                chknganluong.Checked = viewPayment[4].Contains("1") ? true : false;
                txtemailnganluong.Text = viewPayment[5];
                txtmerchantsitenl.Text = viewPayment[6];
                txtmknl.Text = viewPayment[7];

            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string[] viewPayment = new string[8];

            viewPayment[0] = chkbaokim.Checked ? "1" : "0";
            viewPayment[1] = txtemaibaokim.Text.Trim();
            viewPayment[2] = txtmerchantsitebk.Text.Trim();
            viewPayment[3] = txtmkbaokim.Text.Trim();
            viewPayment[4] = chknganluong.Checked ? "1" : "0";
            viewPayment[5] = txtemailnganluong.Text.Trim();
            viewPayment[6] = txtmerchantsitenl.Text.Trim();
            viewPayment[7] = txtmknl.Text.Trim();
            if (txtId.Value == "")
            {
                tbConfigDATA ObjtbConfigDATA = new tbConfigDATA();
                ObjtbConfigDATA.viewPayment = string.Join(",", viewPayment);
                if (tbConfigDB.tbConfig_Add(ObjtbConfigDATA))
                {
                    pnlErr.Visible = true;
                    ltrErr.Text = "Thêm mới cấu hình thành công !";
                }
            }
            else
            {
                if (txtId.Value.Length > 0)
                {
                    List<tbConfigDATA> list = tbConfigDB.tbConfig_GetID(txtId.Value);
                    if (list.Count > 0)
                    {
                        list[0].conid = txtId.Value;
                        list[0].viewPayment = string.Join(",", viewPayment);
                        if (tbConfigDB.tbConfig_Update(list[0]))
                        {
                            pnlErr.Visible = true;
                            ltrErr.Text = "Cập nhật cấu hình thành công !";
                        }
                    }
                }
            }
            MyWeb.Global.LoadConfig(lang);
            showConfig();
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            showConfig();
        }
    }
}