﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucMenu.ascx.cs" Inherits="MyWeb.control.panel.control.ucMenu" %>

<div id='<%if (strFaceValue == "MENU_TOP"){%>top-menu<%}else{ %>sidebar<%} %>' class='<%if (strFaceValue == "MENU_TOP"){%>top-menu<%}else{ %>sidebar<%} %>'>
    <%if (strFaceValue == "MENU_LEFT"){%><div data-scrollbar="true" data-height="100%"><%} %>
        <ul class="nav">
            <%if (strFaceValue == "MENU_LEFT")
              {%>
            <li class="nav-header">DANH MỤC HỆ THỐNG <span class="vntsc-admin-lang">
                <asp:Literal ID="ltrLang" runat="server"></asp:Literal></span></li>
            <%} %>

            <asp:Literal ID="ltrMenu" runat="server"></asp:Literal>
            <li class="has-sub" style="<%if (strFaceValue == "MENU_LEFT"){%>border-top: 1px solid #232a2f;<%}else{ %>border-top: 0px solid #232a2f;<%} %>">
                <a href="javascript:;">
                    <b class="caret pull-right"></b>
                    <i class="fa fa-language"></i>
                    <span>Ngôn ngữ quản trị</span>
                </a>
                <ul class="sub-menu">
                      <li>
                          <asp:LinkButton ID="btnLinkVi" runat="server" OnClick="btnLinkVi_Click">
                            <img src="../theme/admin_cms/img/vi.png" />Tiếng việt
                          </asp:LinkButton>
                      </li>
                      <li>
                          <asp:LinkButton ID="btnLinkEn" runat="server" OnClick="btnLinkEn_Click">
                            <img src="../theme/admin_cms/img/en.png" />English
                          </asp:LinkButton>
                      </li>
                  </ul>
            </li>
            <%if (strFaceValue == "MENU_LEFT")
              {%>
            <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
            <%}
              else
              {%>
            <li class="menu-control menu-control-left">
                <a href="#" data-click="prev-menu"><i class="fa fa-angle-left"></i></a>
            </li>
            <li class="menu-control menu-control-right">
                <a href="#" data-click="next-menu"><i class="fa fa-angle-right"></i></a>
            </li>
            <%} %>
        </ul>

    <%if (strFaceValue == "MENU_LEFT"){%></div><%} %>
</div>
