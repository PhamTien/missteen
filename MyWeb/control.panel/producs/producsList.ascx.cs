﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.producs
{
    public partial class producsList : System.Web.UI.UserControl
    {
        string _Admin = "";
        private string lang = "vi";
        string userid = "";
        int pageSize = 15;
        string strWhere = "";
        string strOrderBy = "";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["pages"] != null && !Request["pages"].Equals(""))
            {
                pageSize = int.Parse(Request["pages"].ToString());
            }

            lang = MyWeb.Global.GetLangAdm();
            if (!IsPostBack)
            {
                if (Session["admin"] != null) { _Admin = Session["admin"].ToString(); }
                if (Session["uid"] != null) userid = Session["uid"].ToString();
                BindForder();
                BindData();
            }
        }

        void BindData()
        {
            List<tbProductDATA> _result = new List<tbProductDATA>();

            strWhere += " AND LOWER(proLang) = '" + lang + "' ";

            strOrderBy = "proDate DESC, proId desc";
            if (Common.StringClass.Check(txtSearch.Text))
            {
                strWhere += " AND (LOWER(proId) LIKE N'%" + txtSearch.Text.ToLower() + "%' OR LOWER(proCode) LIKE N'%" + txtSearch.Text.ToLower() + "%' OR LOWER(proName) LIKE N'%" + txtSearch.Text.ToLower() + "%' OR LOWER(proPrice) LIKE N'%" + txtSearch.Text.ToLower() + "%' )";
            }

            if (drlForder.SelectedValue != "0")
            {
                string _strLevel = db.tbPages.FirstOrDefault(s => s.pagId == int.Parse(drlForder.SelectedValue)).paglevel;
                IQueryable<string> arrSrv = db.tbPages.Where(c => c.paglevel.StartsWith(_strLevel)).Select(c => c.pagId.ToString());
                string strFilter = string.Join(",", arrSrv.ToArray());
                strWhere += " AND catId IN(" + strFilter + ")";
            }

            if (ddlType.SelectedValue != "0")
            {
                if (ddlType.SelectedValue == "1") strWhere += " AND proNoibat= 1";
                else if (ddlType.SelectedValue == "2") strWhere += " AND proIndex= 1";
                else if (ddlType.SelectedValue == "3") strWhere += " AND proBanchay= 1";
                else if (ddlType.SelectedValue == "4") strWhere += " AND proKM= 1";
            }
            if (_Admin == "0")
            {
                strWhere += " AND userId ='" + userid + "'";
            }

            if (drlSortBy.SelectedValue != "0")
            {
                if (drlSortBy.SelectedValue == "name")
                {
                    strOrderBy = "proName";
                }
                else if (drlSortBy.SelectedValue == "namedesc")
                {
                    strOrderBy = "proName DESC";
                }
                else if (drlSortBy.SelectedValue == "price")
                {
                    strOrderBy = "proPrice";
                }
                else if (drlSortBy.SelectedValue == "pricedesc")
                {
                    strOrderBy = "proPrice DESC";
                }
                else if (drlSortBy.SelectedValue == "ord")
                {
                    strOrderBy = "proOrd";
                }
                else
                {
                    strOrderBy = "proDate DESC";
                }
            }
            int intCount = 0;
            _result = tbProductDB.getProductListByAdmin(currentPage + 1, ref intCount, pageSize, 0, strWhere, strOrderBy);
            if (_result.Count() > 0)
            {
                recordCount = (int)intCount;
                rptProductList.DataSource = _result.ToList();
                rptProductList.DataBind();
                BindPaging();

                #region Statistic
                int fResult = currentPage * pageSize + 1;
                int tResult = (currentPage + 1) * pageSize;
                tResult = tResult > recordCount ? recordCount : tResult;
                ltrStatistic.Text = "Hiển thị kết quả từ " + fResult + " - " + tResult + " trong tổng số " + recordCount + " kết quả";
                #endregion
            }
            else
            {
                rptProductList.DataSource = null;
                rptProductList.DataBind();
            }

        }

        private int currentPage { get { return ViewState["currentPage"] != null ? int.Parse(ViewState["currentPage"].ToString()) : 0; } set { ViewState["currentPage"] = value; } }
        private int recordCount { get { return ViewState["recordCount"] != null ? int.Parse(ViewState["recordCount"].ToString()) : 0; } set { ViewState["recordCount"] = value; } }
        private int pageCount { get { double iCount = (double)((decimal)recordCount / (decimal)pageSize); return (int)Math.Ceiling(iCount); } }

        protected string BindImages(string strPart)
        {
            string str = "";
            try
            {
                str = "<img style=\"width:50px;\" src=\"" + strPart.Split(',')[0].Replace("uploads", "uploads/_thumbs") + "\" />";
            }
            catch (Exception)
            { }
            return str;
        }

        public string BindCateName(string strID)
        {
            string strReturn = "";
            var obj = db.tbPages.Where(u => u.pagId == Convert.ToInt32(strID)).ToList();
            if (obj.Count > 0)
            {
                strReturn = obj[0].pagName;
            }
            return strReturn;
        }

        public static string BindNumber(string strNumber)
        {
            Double giatri = 0;
            if (strNumber.ToString().Trim().Length > 0)
            {
                giatri = Double.Parse(strNumber.ToString());
                strNumber = giatri.ToString();
                string dp = display_so(strNumber);
                string p1 = "", p2 = "";
                if (dp.IndexOf(".") > -1)
                {
                    string[] s = dp.Split(Convert.ToChar("."));
                    p1 = s[0];
                    p2 = s[1];
                }
                else
                {
                    p1 = dp;
                }
                if (p1.Length > 3)
                {
                    dp = "";
                    int count = 0;
                    for (int i = p1.Length - 1; i > -1; i--)
                    {
                        count++;
                        dp = p1[i].ToString() + dp;
                        if (i < p1.Length - 1 && count % 3 == 0 && i > 0)
                        {
                            dp = "," + dp;
                        }


                    }
                    if (p2.Length > 0)
                        dp = dp + "." + p2;
                }

                return dp;
            }
            else
            {
                return "0";
            }
        }

        static string display_so(string so)
        {
            string pr = "";
            if (so.Length > 5)
            {
                string re = "";
                if (so.IndexOf(".") > -1)
                {
                    string[] s = so.Split(Convert.ToChar("."));
                    re = s[0];
                    if (s[1].Length > 2)
                    {
                        re += "." + s[1].Substring(0, 2);
                    }
                    else
                    {
                        re += "." + s[1];
                    }
                }
                else
                {
                    return so;
                }

                return re;
            }
            else
            {
                return so;
            }
            return so;
        }

        void BindForder()
        {
            string strForder = "";
            if (_Admin == "0" || _Admin == "100" || _Admin == "1")
            {
                List<tbPageDATA> list = tbPageDB.tbPage_GetByAll(lang);
                list = list.Where(s => s.pagType == pageType.GP).ToList();
                drlForder.Items.Clear();
                drlForder.Items.Add(new ListItem("- Nhóm sản phẩm -", "0"));
                for (int i = 0; i < list.Count; i++)
                {
                    strForder = "";
                    for (int j = 1; j < list[i].paglevel.Length / 5; j++)
                    {
                        strForder = strForder + "---";
                    }
                    drlForder.Items.Add(new ListItem(strForder.ToString() + list[i].pagName, list[i].pagId));
                }
                list.Clear();
                list = null;

            }
            else
            {
                List<tbPageDATA> list = tbPageDB.tbPage_GetByTop("", "pagType = '100' and pagId in (select roleidmodule from tbUserRole where roleuserid=" + userid + ")", "paglevel");
                drlForder.Items.Clear();
                drlForder.Items.Add(new ListItem("- Nhóm sản phẩm -", "0"));
                for (int i = 0; i < list.Count; i++)
                {
                    strForder = "";
                    for (int j = 1; j < list[i].paglevel.Length / 5; j++)
                    {
                        strForder = strForder + "---";
                    }
                    drlForder.Items.Add(new ListItem(strForder.ToString() + list[i].pagName, list[i].pagId));
                }
                list.Clear();
                list = null;
            }

        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            BindData();
            Response.Redirect("/admin-product/list.aspx");
        }

        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
            RepeaterItem item = default(RepeaterItem);

            for (int i = 0; i < rptProductList.Items.Count; i++)
            {
                item = rptProductList.Items[i];
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    if (((CheckBox)item.FindControl("chkBox")).Checked)
                    {
                        HiddenField hidID = (HiddenField)item.FindControl("hidProID");
                        string tagName = db.tbProducts.Where(s => s.proId == int.Parse(hidID.Value)).FirstOrDefault().proTagName;
                        tbPage pg = db.tbPages.Where(s => s.pagTagName == tagName).FirstOrDefault();
                        if (pg != null)
                        {
                            tbPageDB.tbPage_Delete(pg.pagId.ToString());
                        }
                        var comm = db.tbComments.Where(u => u.comProId == Convert.ToInt32(hidID.Value)).ToList();
                        if (comm.Count > 0)
                        {
                            for (int j = 0; j < comm.Count; j++)
                            {
                                db.tbComments.DeleteOnSubmit(comm[j]);
                                db.SubmitChanges();
                            }
                        }

                        var objTabs = db.tbProTags.Where(s => s.proId == int.Parse(hidID.Value)).ToList();
                        if (objTabs.Count > 0)
                        {
                            for (int t = 0; t < objTabs.Count; t++)
                            {
                                db.tbProTags.DeleteOnSubmit(objTabs[t]);
                            }
                            db.SubmitChanges();
                        }


                        tbProductDB.tbProduct_Delete(hidID.Value);
                    }
                }
            }
            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Xóa thành công sản phẩm !!";
            chkSelectAll.Checked = false;
        }

        protected void drlForder_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["ssForder"] = drlForder.SelectedValue;
            BindData();
        }

        protected void drlSortBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }

        private void BindPaging()
        {
            int icurPage = currentPage + 1;
            int ipCount = pageCount;
            if (ipCount >= 1)
            {
                rptNumberPage.Visible = true;
                int PageShow = ipCount > 5 ? 5 : ipCount;
                int FromPage;
                int ToPage;
                DataTable dt = new DataTable();
                dt.Columns.Add("PageIndex");
                dt.Columns.Add("PageText");
                FromPage = icurPage > PageShow ? icurPage - PageShow : 1;
                ToPage = (ipCount - icurPage > PageShow) ? icurPage + PageShow : ipCount;
                if (icurPage - 10 > 0) dt.Rows.Add(icurPage - 9, icurPage - 10);
                for (int i = FromPage; i <= ToPage; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = i - 1;
                    dr[1] = i;
                    dt.Rows.Add(dr);
                }
                if (icurPage + 10 <= ipCount) dt.Rows.Add(icurPage + 9, icurPage + 10);
                rptNumberPage.DataSource = dt;
                rptNumberPage.DataBind();
            }
        }

        protected void btnPage_Click(object sender, EventArgs e)
        {
            if (((LinkButton)sender).ID == "btnPrevious")
            {
                if (currentPage > 0) currentPage = currentPage - 1;
            }
            else if (((LinkButton)sender).ID == "btnNext")
            {
                if (currentPage < pageCount - 1) currentPage = currentPage + 1;
            }
            BindData();
        }

        protected void rptNumberPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("page"))
            {
                currentPage = Convert.ToInt32(e.CommandArgument.ToString());
                BindData();
            }
        }

        protected void rptNumberPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lnkPage = (LinkButton)e.Item.FindControl("btn");
            Literal _ltrPage = (Literal)e.Item.FindControl("ltrLiPage");
            if (lnkPage.CommandArgument.ToString() == currentPage.ToString())
            {
                lnkPage.Enabled = false;
                _ltrPage.Text = "<li class=\"paginate_button active\">";
            }
            else
            {
                _ltrPage.Text = "<li class=\"paginate_button\">";
            }
        }

        protected void rptProductList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string strID = e.CommandArgument.ToString();
            switch (e.CommandName.ToString())
            {
                case "Coppy":
                    #region Nhân bản sản phẩm
                    tbProduct pro = db.tbProducts.FirstOrDefault(s => s.proId == Int32.Parse(strID));
                    DateTime? datetime = null;
                    datetime = DateTime.Now;
                    var ObjtbProductDATA = new tbProduct();
                    ObjtbProductDATA.proCode = pro.proCode;
                    ObjtbProductDATA.proName = pro.proName;
                    ObjtbProductDATA.proImage = pro.proImage;
                    ObjtbProductDATA.proFiles = pro.proFiles;
                    ObjtbProductDATA.proContent = pro.proContent;
                    ObjtbProductDATA.proDetail = pro.proDetail;
                    ObjtbProductDATA.proDate = datetime;
                    ObjtbProductDATA.proTitle = pro.proTitle;
                    ObjtbProductDATA.proDescription = pro.proDescription;
                    ObjtbProductDATA.proKeyword = pro.proKeyword;
                    ObjtbProductDATA.proPriority = pro.proPriority;
                    ObjtbProductDATA.proIndex = pro.proIndex;
                    ObjtbProductDATA.proType = pro.proType;
                    ObjtbProductDATA.proActive = pro.proActive;
                    ObjtbProductDATA.proOriginalPrice = pro.proOriginalPrice;
                    ObjtbProductDATA.proPrice = pro.proPrice;
                    ObjtbProductDATA.proUnitprice = pro.proUnitprice;
                    ObjtbProductDATA.proPromotions = pro.proPromotions;
                    ObjtbProductDATA.proWeight = pro.proWeight;
                    ObjtbProductDATA.proUnits = pro.proUnits;
                    ObjtbProductDATA.proOrd = pro.proOrd;
                    ObjtbProductDATA.proLang = pro.proLang;
                    ObjtbProductDATA.proWarranty = pro.proWarranty;
                    ObjtbProductDATA.proTagName = "";
                    ObjtbProductDATA.proVideo = pro.proVideo != null ? pro.proVideo : "";
                    ObjtbProductDATA.proCount = pro.proCount;
                    ObjtbProductDATA.proCare = pro.proCare;
                    ObjtbProductDATA.proSpLienQuan = pro.proSpLienQuan;
                    ObjtbProductDATA.proSpLienQuan = pro.proSpLienQuan;
                    ObjtbProductDATA.proUrl = pro.proUrl;
                    ObjtbProductDATA.proKieuspdaban = pro.proKieuspdaban;
                    ObjtbProductDATA.proNoibat = pro.proNoibat;
                    ObjtbProductDATA.proBanchay = pro.proBanchay;
                    ObjtbProductDATA.proKM = pro.proKM;
                    ObjtbProductDATA.proTungay = pro.proTungay;
                    ObjtbProductDATA.proDenngay = pro.proDenngay;
                    ObjtbProductDATA.proNoidungKM = pro.proNoidungKM;
                    ObjtbProductDATA.proStatus = pro.proStatus;
                    ObjtbProductDATA.catId = pro.catId;
                    ObjtbProductDATA.manufacturerId = pro.manufacturerId;
                    if (tbProductDB.tbProduct_Add(ObjtbProductDATA))
                    {
                        tbProduct attpro = db.tbProducts.OrderByDescending(c => c.proId).FirstOrDefault();
                        attpro.proTagName = MyWeb.Common.StringClass.NameToTag(attpro.proName) + "-" + attpro.proId;
                        db.SubmitChanges();

                        #region Them vao tbPage + tbProduct
                        tbPageDATA objNewMenu = new tbPageDATA();
                        objNewMenu.pagName = attpro.proName;
                        objNewMenu.pagImage = attpro.proImage;
                        objNewMenu.pagType = pageType.PD;
                        var curNewType = db.tbPages.Where(s => s.pagId == int.Parse(attpro.catId)).FirstOrDefault();
                        objNewMenu.paglevel = curNewType.paglevel + "00000";
                        objNewMenu.pagTitle = attpro.proTitle;
                        objNewMenu.pagDescription = attpro.proDescription; ;
                        objNewMenu.pagKeyword = attpro.proKeyword;
                        objNewMenu.pagOrd = "0";
                        objNewMenu.pagActive = attpro.proActive.ToString();
                        objNewMenu.pagLang = lang;
                        objNewMenu.pagDetail = "";
                        objNewMenu.pagLink = "";
                        objNewMenu.pagTarget = "";
                        objNewMenu.pagPosition = "0";
                        objNewMenu.catId = "";
                        objNewMenu.grnId = "";
                        objNewMenu.pagTagName = attpro.proTagName;
                        objNewMenu.check1 = "";
                        objNewMenu.check2 = "";
                        objNewMenu.check3 = "";
                        objNewMenu.check4 = "";
                        objNewMenu.check5 = "";
                        string strTagName = MyWeb.Common.StringClass.NameToTag(attpro.proName) + "-" + attpro.proId;
                        if (tbPageDB.tbPage_Add(objNewMenu))
                        {
                            List<tbPageDATA> curItem = tbPageDB.tbPage_GetByMax();
                            curItem = curItem.OrderByDescending(s => s.pagId).ToList();
                            var hasTagName = db.tbPages.Where(s => s.pagTagName == strTagName).FirstOrDefault();
                            curItem[0].pagId = curItem[0].pagId;
                            curItem[0].pagTagName = hasTagName != null ? strTagName : "";
                            tbPageDB.tbPage_Update(curItem[0]);
                            ObjtbProductDATA.proTagName = curItem[0].pagTagName;

                        }
                        try
                        {
                            string proid = attpro.proId.ToString();
                            tbProductDB.tbProductTag("update tbProTag set proId=" + attpro.proId + " where proId is null");
                            List<tbProTag> protag = db.tbProTags.Where(s => s.proId == int.Parse(strID)).ToList();
                            if (protag.Count > 0)
                            {
                                for (int t = 0; t < protag.Count; t++)
                                {
                                    tbProTag ptag = new tbProTag();
                                    ptag.proId = attpro.proId;
                                    ptag.TagValue = protag[t].TagValue;
                                    ptag.TagName = protag[t].TagName;
                                    ptag.Ord = protag[t].Ord;
                                    ptag.Active = protag[t].Active;
                                    db.tbProTags.InsertOnSubmit(ptag);
                                    db.SubmitChanges();
                                }
                            }
                        }
                        catch
                        {
                            pnlErr.Visible = true;
                            ltrErr.Text = "Copy sản phẩm bị lỗi";
                        }
                        #endregion
                    }
                    pnlErr.Visible = true;
                    ltrErr.Text = "Coppy sản phẩm thành công !!";
                    BindData();
                    break;
                    #endregion

                case "DEL":
                    string tagName = db.tbProducts.Where(s => s.proId == int.Parse(strID)).FirstOrDefault().proTagName;
                    tbPage pg = db.tbPages.Where(s => s.pagTagName == tagName).FirstOrDefault();
                    if (pg != null)
                    {
                        tbPageDB.tbPage_Delete(pg.pagId.ToString());
                    }
                    var comm = db.tbComments.Where(u => u.comProId == Convert.ToInt32(strID)).ToList();
                    if (comm.Count > 0)
                    {
                        for (int i = 0; i < comm.Count; i++)
                        {
                            db.tbComments.DeleteOnSubmit(comm[i]);
                            db.SubmitChanges();
                        }
                    }
                    //Xoa Tabs
                    var objTabs = db.tbProTags.Where(s => s.proId == int.Parse(strID)).ToList();
                    if (objTabs.Count > 0)
                    {
                        for (int i = 0; i < objTabs.Count; i++)
                        {
                            db.tbProTags.DeleteOnSubmit(objTabs[i]);
                        }
                        db.SubmitChanges();
                    }


                    tbProductDB.tbProduct_Delete(strID);
                    DeleteCenter(strID);
                    pnlErr.Visible = true;
                    ltrErr.Text = "Xóa thành công sản phẩm !!";
                    BindData();
                    break;
                case "Edit":
                    Response.Redirect("/admin-product/add.aspx?idpro=" + strID);
                    break;
                case "Active":
                    tbProduct product = db.tbProducts.FirstOrDefault(s => s.proId == Int32.Parse(strID));
                    if (product.proActive == 0)
                    {
                        product.proActive = 1;
                    }
                    else { product.proActive = 0; }
                    db.SubmitChanges();
                    BindData();
                    pnlErr.Visible = true;
                    ltrErr.Text = "Cập nhật thành công!!";
                    break;
            }
        }

        private void DeleteCenter(string _strCatID)
        {
            IEnumerable<tbCatAtt> listcatatt = db.tbCatAtts.Where(c => c.catId == Int32.Parse(_strCatID));
            if (listcatatt.Count() > 0)
            {
                db.tbCatAtts.DeleteAllOnSubmit(listcatatt);
                db.SubmitChanges();
            }
        }

        public static string ShowActive(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "<i class=\"fa fa-check\"></i>" : "<i class=\"fa fa-times\"></i>";
        }

        public static string ShowActiveClass(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "btn btn-primary btn-xs" : "btn btn-danger btn-xs";
        }
        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }
    }
}